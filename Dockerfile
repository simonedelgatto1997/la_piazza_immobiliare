FROM node:latest

WORKDIR .

COPY . .

RUN npm install
# RUN rm -rf public/build
# WORKDIR ./app
# RUN npm install 
# RUN rm -rf build
# RUN npm run build
# RUN cp -R ./build ../public

WORKDIR ../

EXPOSE 3001
CMD [ "npm", "start" ]
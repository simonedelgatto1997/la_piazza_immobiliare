var authRouter = require('./routes/auth');
var testRouter = require('./routes/test');
var usersRouter = require('./routes/users');
var indexRouter = require('./routes/index');
var housesRouter = require('./routes/houses');
var dealRouter = require('./routes/deal');
const cors = require('cors')
var express = require('express');
var app = express();
var cookieParser = require('cookie-parser');
app.use(cookieParser());

var createError = require('http-errors');
var path = require('path');
var logger = require('morgan');

/**ORDINE CORRETTO NON SPOSTARE LE COSE
 * 1- REQUIRE
 * 2- AGGIUNTA DEI MIDDLEWARE
 * 3- AGGIUNTA DELLE ROUTES
 * 4- aggiunta degli ultimi handler
 */

// view engine setup
app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public','build')));
app.use(cors())

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/auth', authRouter);
app.use('/test', testRouter);
app.use('/houses', housesRouter);
app.use('/deal', dealRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
app.set('views', path.join(__dirname, 'views'));

module.exports = app;

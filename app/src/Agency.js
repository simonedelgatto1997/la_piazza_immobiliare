import React, { Component } from 'react';
import Header from './main_elements/Header';
import Footer from './main_elements/Footer';

export default class Agency extends Component {
  
  constructor(props){
    super(props);

    this.state = {
    }
  }

  render() {
    return (
      <><Header/>
      <div className="container">
        <div className='row justify-content-center'>
          <div className="col-8 text-center">
            <h1 className="TitoloPagina">L'Agenzia</h1>

              <a className="navbar-brand" title="Logo">
                <img src="/esterno2.jpeg" className="logoImg" />
              </a> 
              <p>L'Agenzia LA PIAZZA è da oltre 26 anni un punto di riferimento per il mercato immobiliare di Castel Bolognese. <br/><br/> 
              L'obiettivo principale è quello di assistere la clientela nella scelta dell'immobile giusto, dando la certezza di un acquisto sicuro. Con professionalità valutiamo il Vostro immobile, garantendone il giusto valore di realizzo. <br/><br/> 
              L'Agenzia LA PIAZZA è in grado di dare assistenza a chi desidera acquistare, vendere o affittare sia soluzioni residenziali che commerciali, industriali o artigianali. <br/><br/>Siamo associati FIAIP per dare una ulteriore qualificazione al nostro servizio.
              </p>
          </div>
        </div>
        <div className='row'>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2854.587087088543!2d11.796796915519481!3d44.3184384791041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132b48361b680f27%3A0x12cd8d8cffa0cb4d!2sL&#39;Agenzia%20LA%20PIAZZA!5e0!3m2!1sit!2sit!4v1645971567734!5m2!1sit!2sit"></iframe>
        </div>
	    </div>
      <Footer></Footer>
     </>
    );
  }
}


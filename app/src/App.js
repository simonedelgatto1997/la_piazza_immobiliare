import React from "react";
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import Agency from "./Agency";
import { SignIn } from "./auth/Sign_in"
import Home from "./Home";
import Loan from "./Loan";

import { AgentHome } from "./agent/AgentHome";
import { SignUp } from './auth/Sign_up'
import { useUser, LogoutButtonPopup, AuthStore, AuthifyNoLogout } from './auth'
import { CustomerWebSocket } from "./websocket/customerWebSocket";
import { AgentWebSocket } from "./websocket/agentWebSocket";
import { CustomerProfile } from "./customer/CustomerProfile";
import { CustomerProfileEdit } from "./customer/CustomerProfileEdit";
import { PopulateDb } from "./populate_db/populate_house";
import { PopulateCustomerDb } from "./populate_db/populate_customer";
import { SearchView } from './search/SearchView'
import { Search } from "./search/search";
import { FiltersView } from "./search/FiltersView"
import { SavedSearches } from "./search/SavedSearches";
import { CustomerLoad } from "./agent/client/CustomerLoad";
import { Calendar } from "./agent/Calendar";
import { AddHouse } from "./agent/house/AddHouse";
import { Favourites } from "./house/favourites";
import { NoLogin } from "./elements/NoElements";
import { ClientProfile } from "./agent/client/ClientProfile";
import { ClientProfileEdit } from "./agent/client/ClientProfileEdit";
import { HouseDetail } from "./agent/house/HouseDetail";
import { HouseDetailEdit } from "./agent/house/HouseDetailEdit";
import { FeedBack } from "./elements/FeedBack";
import { ClientRequestLoad } from "./agent/client/ClientRequestLoad";
import { AddDeal } from "./agent/deal/AddDeal";
import { LoadInterest } from "./agent/LoadInterest";
import { DealLoad } from "./agent/deal/DealLoad";
import { DealDetail } from "./agent/deal/DealDetail";
import { DealDetailEdit } from "./agent/deal/DealDetailEdit";
import { HousesLoad } from "./agent/house/HousesLoad"

function App() {
  let socket = undefined
  const useUserF = useUser(['agent', 'customer'], '/auth/', '/auth/decode/')
  const [user, setUser] = React.useState(null)

  React.useEffect(() => {
    AuthStore.on(() => {
      console.log('cambia utente')
      setUser(AuthStore.getValue())
    })
    setUser(AuthStore.getValue())
  }, [])

  React.useEffect(() => {
    console.log('user per app', user)
    if (user != null) {
      if (user.role == 'customer') {
        socket = CustomerWebSocket.connect()
      } else {
        console.log('im agent')
        socket = AgentWebSocket.connect()
      }
    }
  }, [user])

  return (

    <React.Fragment>
      <BrowserRouter>
        <Routes>
          {/* <Route exact path="/" element={<Home user={user}></Home>} /> */}
          <Route exact path="/" element={<Home user={user} />}></Route>
          <Route exact path="/agenzia" element={<Agency />} />
          <Route exact path="/auth" element={<SignIn />} />
          <Route exact path="/registrati" element={<SignUp user={user}/>} />
          <Route exact path="/mutui" element={<Loan/>} />
          <Route exact path="/agentHome/showHouses" element={<HousesLoad user={user}/>}></Route>
          <Route exact path="/agentHome/showClients" element={<CustomerLoad user={user}/>}></Route>
          <Route exact path="/agentHome/calendar" element={<Calendar user={user}/>}></Route>
          <Route exact path="/agentHome/insertHouse" element={<AddHouse user={user}/>}></Route>
          <Route exact path="/agentHome/client/*" element={<ClientProfile user={user}/>}></Route>
          <Route exact path="/agentHome/client-edit/*" element={<ClientProfileEdit user={user}/>}></Route>
          <Route exact path="/agentHome/house/*" element={<HouseDetail user={user}/>}></Route>
          <Route exact path="/agentHome/house-edit/*" element={<HouseDetailEdit user={user}/>}></Route>
          <Route exact path="/agentHome/client-favourites/*" element={<ClientRequestLoad user={user}/>} />
          <Route exact path="/agentHome/showInterest" element={ <LoadInterest user={user}></LoadInterest>}/>
          <Route exact path="/agentHome/showDeal" element={ <DealLoad user={user}></DealLoad>}/>
          <Route exact path="/agentHome/insertDeal" element={ <AddDeal user={user}></AddDeal>}/>
          <Route exact path="/agentHome/deal/*" element={<DealDetail user={user}/>}></Route>
          <Route exact path="/agentHome/deal-edit/*" element={<DealDetailEdit user={user}/>}></Route>
          <Route exact path="/customer" element={<CustomerProfile user={user}></CustomerProfile>} />
          <Route exact path="/registrati" element={<SignUp />} />
          <Route exact path="/mutui" element={<Loan></Loan>} />
          <Route path="/agentHome" element={<AgentHome />}></Route>
          <Route exact path="/customer" element={
            <NoLogin user={user}>
              <CustomerProfile user={user} />
              <SignIn></SignIn>
            </NoLogin>
          } />
          <Route exact path="/customer-edit" element={<CustomerProfileEdit user={user}></CustomerProfileEdit>} />
          <Route exact path="/search" element={
            <FeedBack>
              <SearchView user={user}></SearchView>
            </FeedBack>
          }></Route>
          <Route exact path="/filters" element={<FiltersView user={user}></FiltersView>}></Route>
          <Route exact path="/savedSearches" element={
            <NoLogin user={user}>
              <SavedSearches user={user}></SavedSearches>
              <SignIn></SignIn>
            </NoLogin>
          }></Route>
          <Route exact path="/favourites" element={
            <NoLogin user={user}>
              <Favourites user={user}></Favourites>
              <SignIn></SignIn>
            </NoLogin>
          }></Route>

          <Route path="*"
            element={
              <main style={{ padding: "1rem" }}>
                <p>There's nothing here!</p>
              </main>
            }
          />
        </Routes>
      </BrowserRouter>
      {/* {useUser(['agent', 'customer'], '/auth/', '/auth/decode/').user !== null && <LogoutButtonPopup></LogoutButtonPopup>} */}
      {/* {user !== null && <FileUploader user={user}></FileUploader>} */}
    </React.Fragment>
  );


}


export default App;

/*<Route exact path="/auth" element={<SignIn logoutButton={logoutButton}></SignIn>} />*/
import './App.css';
import * as React from 'react'
import Header from './main_elements/Header';
import Footer from './main_elements/Footer';
//import { Subject } from "rxjs";
import { LocationSearchingBar } from './search/LocationSearchingBar';
import { Navigate } from 'react-router-dom';
import { PriorityHouses } from './house/PriorityHouses';
import { PopulateCustomerDb } from './populate_db/populate_customer';
import { PopulateDb } from './populate_db/populate_house';


function Home({ user }) {
  if (user != null && user.role == 'agent') {
    return <Navigate to='agentHome/showHouses'></Navigate>
  } else {
    return (
      <div>
        <Header user={user}></Header>
        <LocationSearchingBar user={user}></LocationSearchingBar>
        <PriorityHouses></PriorityHouses>
      <Footer></Footer>
    </div>
    );
  }
}

export default Home;

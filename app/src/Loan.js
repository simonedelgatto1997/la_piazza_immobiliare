import React, { Component, useState} from 'react';
import Header from './main_elements/Header';
import Footer from './main_elements/Footer';
import {Bar} from 'react-chartjs-2';
import Chart from 'chart.js/auto'


export default class Loan extends Component {
  constructor(props){
    super(props);
    this.state = {
      chartData: {
        labels: ['5', '10', '15', '20', '25', '30'],
        datasets: [
          {
            label: 'Rata mutuo (euro)',
            data: [0,0,0,0,0,0],
            fill: true,
            backgroundColor: [
              'rgba(255,99,132,0.6)',
              'rgba(54,162,235,0.6)',
              'rgba(255,206, 86,0.6)',
              'rgba(75,192, 192,0.6)',
              'rgba(153,102,255,0.6)',
              'rgba(255,159,64,0.6)',
            ],
            pointBorderColor:"#8884d8",
            hoverBorderWidth: 3,
            hoverBorderColor: '#000'
          },
        ],
      }
    }
    this.computeRates = this.computeRates.bind(this);
  }

  computeRates(){
    const year = [5, 10, 15, 20, 25, 30];
    const r = year.map((y) => {
      let tasso = document.getElementById("tax").value/100;
      let cap = document.getElementById("capital").value;

      /* Capitale * (1 + Tasso/12) ^ (12 * Anni) */
      /* x */
      /* (Tasso/12 )  / ( (1+Tasso/12)^(12*Anni) - 1) */
      let p = Math.pow((1 + tasso/12), 12*y);
      let exp = (cap * p);
      let n = (tasso/12) / (p-1);
      return Math.round(exp*n*100)/100;
    });
    
    this.setState({
      chartData: {
        labels: ['5', '10', '15', '20', '25', '30'],
        datasets: [
          {
            label: 'Rata mutuo',
            data: r,
            fill: true,
            backgroundColor: [
              'rgba(255,99,132,0.6)',
              'rgba(54,162,235,0.6)',
              'rgba(255,206, 86,0.6)',
              'rgba(75,192, 192,0.6)',
              'rgba(153,102,255,0.6)',
              'rgba(255,159,64,0.6)',
            ],
            pointBorderColor:"#8884d8",
            hoverBorderWidth: 3,
            hoverBorderColor: '#000'
          },
        ],
      }
    })
    console.log(r);
  }

  render() {
    return (
      <><Header/>
      <div className="container">
        <div className='row justify-content-center text-center'>
        <h1>Simulazione mutuo</h1>
        <div className="row g-3">
          <div className="col">
            <h6>Capitale</h6>
            <input id='capital' type="text" defaultValue="100000" className="form-control" aria-label="Capitale"/>
          </div>
          <div className="col">
            <h6>Tasso di interesse</h6>
            <input id='tax' type="number" min="0.20" max="10.00" step="0.1" className="form-control" defaultValue="1.5" aria-label="Tasso di interesse"/>
          </div>
        </div>
        
        <div className="col-12"><br/></div>
        
        <div className="col-12">
          <button onClick={this.computeRates}>Calcola mutuo</button>
        </div>
        
        <div className="col-12"><br/></div>
        <div className="col-12"><br/></div>

        <div className="col-12">
          <Bar 
            data={this.state.chartData}
            options={{ 
              title:{
                display: true,
                fontSize: 25
              },
              legend: {
                display: true,
                position: 'right'
              }
            }}
          />
        </div>
        </div>
      </div>
      
      <Footer></Footer>
     </>
    );
  }
}



import React from 'react';
import { CustomerWebSocket } from './websocket/customerWebSocket';


export function PreferenceButton({user}) {
    return <button onClick={() => { 
        CustomerWebSocket.expressInterest(user.id, 'CASA DEL GATTO')
     }} className="btn bg-secondary">
        <i className="bi bi-heart-fill"></i>
    </button>

}

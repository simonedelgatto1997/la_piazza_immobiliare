import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useUser } from "../auth";
import { AgentWebSocket } from '../websocket/agentWebSocket';
import './AgentHome.css';

export function AgentHome ({user}){
  const [showNotification, setShowNotification] = React.useState( Number.parseInt(localStorage.getItem('onCustomerExpressInterest')))
  const { Methods } = useUser(['agent', 'customer'], '/auth/', '/auth/decode/')
  const navigate = useNavigate()

  React.useEffect(()=>{
    AgentWebSocket.onCustomerExpressInterest((data) =>{
      console.log('new notification ', showNotification)
      let prev = Number.parseInt(localStorage.getItem('onCustomerExpressInterest'))
      prev = prev == null ? 0 : prev
      localStorage.setItem('onCustomerExpressInterest', prev+1)
      setShowNotification(prev+1)
    })
  }, [showNotification])


  function setResponsive() {
    var x = document.getElementById("myLeftBar");
    if (x.className === "nav_list") {
      x.className += " responsive";
    } else {
      x.className = "nav_list";
    }
  }

  function checkNotification(){
      console.log('Click')
      localStorage.setItem('onCustomerExpressInterest', 0)
      setShowNotification(0)
      console.log('Notifica vista' + showNotification)
  }

  return (
      <> 
        <header id="headTog">
          <a className="header_toggle mt-2" onClick={setResponsive}>
            <i className="fa fa-bars text-white">{showNotification > 0 ? <span className='button_badge'>{parseInt(localStorage.getItem('onCustomerExpressInterest'))}</span> : ''}</i>
          </a>
        </header>
        <div className="l-navbar" id="nav-bar">
            <div className="nav_list" id="myLeftBar" onClick={setResponsive}> 
              <a className='nav_link'></a>
              <div className="nav_link" onClick={() => navigate('/agentHome/calendar')}> 
                <i className="fa fa-calendar" aria-hidden="true"></i> 
                <span className="nav_name">Agenda</span> 
              </div> 
              <div className="nav_link" onClick={() => navigate('/agentHome/showHouses')}> 
                <i className="fa fa-home" aria-hidden="true"></i> 
                <span className="nav_name">Immobili</span> 
              </div> 
              <div className="nav_link" onClick={() => navigate('/agentHome/insertHouse')}>
                <i className="fa fa-plus" aria-hidden="true"> </i> 
                <span className="nav_name">Nuovo Immobile</span> 
              </div> 
              { showNotification > 0 ?
                <div className="nav_link" onClick={() => {
                  checkNotification()
                  navigate('/agentHome/showInterest')
                }}>
                  <i className="fa fa-bookmark" aria-hidden="true"></i> 
                  <span className="nav_name">Richieste <span className='button_badge'>{parseInt(localStorage.getItem('onCustomerExpressInterest'))}</span> </span>
                </div> :
                <div className="nav_link"  onClick={() => navigate('/agentHome/showInterest')}>
                  <i className="fa fa-bookmark" aria-hidden="true"></i> 
                  <span className="nav_name">Richieste</span> 
                </div> }
              <div className="nav_link" onClick={() => navigate('/agentHome/showClients')}> 
                <i className="fa fa-id-card" aria-hidden="true"></i> 
                <span className="nav_name">Clienti</span> 
              </div>
              <div className="nav_link" onClick={() => navigate('/registrati')}>
                <i className="fa fa-user-plus" aria-hidden="true"></i> 
                <span className="nav_name">Nuovo Cliente</span> 
              </div>
              <div className="nav_link" onClick={() => navigate('/agentHome/showDeal')}> 
                <i className="fa fa-book" aria-hidden="true"></i> 
                <span className="nav_name">Trattative</span> 
              </div>
              <button className='btn rounded-circle' onClick={()=>{
                    Methods.logout()
                    navigate('/auth')
                }}>
                    <i className="fa fa-sign-out" aria-hidden="false" title='Disconnetti'> Esci </i>
                </button>
            </div> 
        </div>
     </>
    );

}
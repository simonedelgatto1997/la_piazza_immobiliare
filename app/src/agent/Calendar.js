import React, { Component, useEffect, useState } from "react";
import { AgentHome } from "./AgentHome";

export function Calendar ({ user }) {
    var [date, setDate] = React.useState([])

    React.useEffect(async () => {
       return showDate()
    }, [date])

    var s = {
            event: {
                summary: 'title',
                description: 'note description',
                start: 
                    {
                        dateTime: 'yyyy-mm-ddThh:mm',
                        timeZone: 'Europe/Rome'
                    }
                ,
                end:
                    {
                        dateTime: 'yyyy-mm-ddThh:mm',
                        timeZone: 'Europe/Rome'
                    }
                ,
                reminders: {
                    useDefault: true
                }
            },

            typeEvent: {
                altro: 'lj021j86l0hf12avbl76vrcv6g@group.calendar.google.com',
                compra: '3qke5t0o6ba2sfo2u3p5vh78f0@group.calendar.google.com',
                locazione: 'alibjfeuvmbmpna2a379ktb4v0@group.calendar.google.com',
                perito_banca: 'a7gv3ij3ka3m2qhms5gse5vb40@group.calendar.google.com',
                stipula_compromesso: '3i6br8ebgr262adnotdq7vj1tk@group.calendar.google.com',
                vende: 'dkf43kcr3oianvbqa0617d9j48@group.calendar.google.com',
                visite_immobili : '3bef7ikgteik752j4tvjh4rroo@group.calendar.google.com'
            },
            eventList: [],   
    }
    
    function handleClick (n, c, w, t, v, d) {
        console.log("click in handle ");

        /* 
        Update with your own Client Id and Api key 
        TODO ARE PROPS??
        */
        const CLIENT_ID = "851871066564-k7sam8odi13kfhj44phaafmc90cuiecc.apps.googleusercontent.com"
        const API_KEY = "AIzaSyDO765kHXKgoNBQHo5lj1uNO7qqVwfxsqg"
        const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"]
        const SCOPES = "https://www.googleapis.com/auth/calendar.events"

        s.event.summary = n + " " + c;
        s.event.start.dateTime = w+'T'+t+':00+01:00';
        s.event.end.dateTime = w+'T'+t+':00-00:00';
        s.event.description = d;

        var gapi = window.gapi;
        gapi.load('client:auth2', () => {
            console.log('loaded client')
  
            gapi.client.init({
                apiKey: API_KEY,
                clientId: CLIENT_ID,
                discoveryDocs: DISCOVERY_DOCS,
                scope: SCOPES,
            })
  
            gapi.client.load('calendar', 'v3', () => console.log('Calendar loaded'))
            /*https://learn.azuqua.com/connector-reference/googlecalendar/#:~:text=Start%20Time%20(date)%3A%20The,00%20is%20the%20timezone%20offset. */
            gapi.auth2.getAuthInstance().signIn().then(() => {
                var event = s.event;
    
                var request = gapi.client.calendar.events.insert({
                    'calendarId': s.typeEvent[v],
                    'resource': event,
                })
  
                request.execute(event => {
                    window.open(event.htmlLink)
                })
            })
        })
    }

    function showEvent (type) {
        const CLIENT_ID = "851871066564-k7sam8odi13kfhj44phaafmc90cuiecc.apps.googleusercontent.com"
        const API_KEY = "AIzaSyDO765kHXKgoNBQHo5lj1uNO7qqVwfxsqg"
        const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"]
        const SCOPES = "https://www.googleapis.com/auth/calendar.events"
          
        s.eventList=[];
        // get events
        var gapi = window.gapi;
        gapi.load('client:auth2', () => {
            console.log('loaded client')
    
            gapi.client.init({
                apiKey: API_KEY,
                clientId: CLIENT_ID,
                discoveryDocs: DISCOVERY_DOCS,
                scope: SCOPES,
            }).then(
                () => {
                    gapi.client.load('calendar', 'v3', () => console.log('Calendar loaded'))       
                    gapi.client.calendar.events.list({
                        'calendarId': s.typeEvent[type],
                        'timeMin': (new Date()).toISOString(),
                        'showDeleted': false,
                        'singleEvents': true,
                        'maxResults': 10,
                        'orderBy': 'startTime'
                    }).then(response => {
                        response.result.items.forEach(e => {
                            s.eventList.push({
                                data: e['start']['dateTime'].substring(0,10), 
                                time: e['start']['dateTime'].substring(11, 16), 
                                who: e['summary'], 
                                descr: e['description'] 
                            })
                        });
                        console.log(s.eventList);
                        setDate(date = JSON.parse(JSON.stringify(s.eventList)))
                    })
                }
            )           
        });
    }

    function showDate(){
        return <table className='table'>
                <thead>
                    <tr>
                    <th>Data</th>
                    <th>Orario</th>
                    <th>Chi</th>
                    <th>Descrizione</th>
                    </tr>
                </thead>
                <tbody>
                {date.map((val, key) => {
                    return (
                    <tr key={key}>
                        <td><div>{val.data}</div></td>
                        <td><div>{val.time}</div></td>
                        <td><div>{val.who}</div></td>
                        <td><div>{val.descr}</div></td>    
                    </tr>)
            })}
            </tbody>
        </table>
    }
  
    return (
        <> 
        <AgentHome/>
        <div className="container">
            <div className="row">
                <h1 className="text-center">Aggiungi un nuovo appuntamento</h1>
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon3">Chi? </span>
                    <input type="text" className="form-control" id="nameClient" aria-describedby="basic-addon3"/>
                    <span className="input-group-text" id="basic-addon3">Cellulare </span>
                    <input type="text" className="form-control" id="phoneClient" aria-describedby="basic-addon3"/>
                </div>
                <div className="input-group mb-3">
                    <span className="input-group-text" id="basic-addon3">Quando? </span>
                    <input type="date" className="form-control" id="whenDate" aria-describedby="basic-addon3"/>
                    <span className="input-group-text" id="basic-addon3">Orario </span>
                    <input type="time" className="form-control" id="timeDate" aria-describedby="basic-addon3"/>
                </div>

                <div className="input-group mb-3">
                <select className="form-select" id="type" aria-label="Example select with button addon">
                    <option value="altro">Altro </option>
                    <option value="compra">Compra</option>
                    <option value="locazione">Locazione</option>
                    <option value="perito_banca">Perito banca</option>
                    <option value="stipula_compromesso">Stipula/Compromesso</option>
                    <option value="vende">Vende</option>
                    <option value="visite_immobili">Visite immobili</option>
                </select>
                </div>

                <div className="input-group mb-3">
                    <div className='col-12'>
                        <textarea className="form-control" id="desc" data-component="note" name="adNotes" placeholder="Inserisci descrizione" rows="3"></textarea>
                    </div>
                </div>
                
                <button onClick={ () =>
                    handleClick(
                        document.getElementById('nameClient').value,
                        document.getElementById('phoneClient').value,
                        document.getElementById('whenDate').value,
                        document.getElementById('timeDate').value,
                        document.getElementById('type').value,
                        document.getElementById('desc').value
                    )
                    }>Aggiungi appuntamento
                </button>

                <div className="col-xs-12"><hr/></div>

                <div className="input-group mb-3">
                    <select className="form-select" id="type2" aria-label="Example select with button addon">
                        <option value="altro">Altro </option>
                        <option value="compra">Compra</option>
                        <option value="locazione">Locazione</option>
                        <option value="perito_banca">Perito banca</option>
                        <option value="stipula_compromesso">Stipula/Compromesso</option>
                        <option value="vende">Vende</option>
                        <option value="visite_immobili">Visite immobili</option>
                    </select>
                    <button onClick={ () =>  {
                        showEvent(document.getElementById('type2').value,)
                        }}>Mostra i prossimi appuntamenti</button>
                </div>    
            </div>
            {showDate()}
        </div>
        </>
    );
}
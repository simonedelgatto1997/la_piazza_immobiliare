import React from "react";
import { AgentHome } from "./AgentHome";
import { Loading } from "./Loading";

export function Interest({ fav, loading }) {

    function show(){
        return fav.map((val, key) => {
            return (
            <tr key={key}>
                <td><div>{val.name}</div></td>
                <td><div>{val.surname}</div></td>
                <td><div>{val.ref}</div></td>
                <td><div>{val.address}</div></td>
                <td><div>{val.price}</div></td>
            </tr>)
        })
    }

    function getFavouritesTable(){
        return loading ? <Loading></Loading> : <>
            <table>
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Cognome</th>
                        <th>Rif</th>
                        <th>Indirizzo</th>
                        <th>Prezzo</th>
                    </tr>
                </thead>
                <tbody>
                    {show()}
                </tbody>
            </table>
        </>
    }

    return (
        <><AgentHome/>
        <React.Fragment>
            <div className="container">
                <div className="row justify-content-center g-2 mt-3">
                    <h2>Richieste utenti</h2>
                </div>
                {getFavouritesTable()}
            </div>
        </React.Fragment></> 
    )
}
import React from "react";
import { AgentHome } from "./AgentHome";
import { Interest } from "./Interest";

export function LoadInterest({ user }) {
    const [fav, setFav] = React.useState([])
    const [loading, setLoading] = React.useState(true)

    React.useEffect(async () => {
         if(user != null){
            let res = await fetch(`/users/interests?jwt=${user.jwt}`)
            let i = await res.json()

            if(i != null){
                i.map(function(v){                
                    JSON.parse(v).interests.map( async function (e) {
                        fetch(`/houses/${e._id}?jwt=${user.jwt}`).then(res2 => {
                            res2.json().then( i2 => {
                                if (i2 != null) {
                                    setFav((prev) => {
                                        return [...prev, {
                                            name: JSON.parse(v).name,
                                            surname: JSON.parse(v).surname,
                                            ref: i2.ref,
                                            address: i2.address,
                                            price: i2.price
                                        }]
                                    })
                                }
                            })
                        })
                    }
                    )
                })
            }
            setLoading(false)
        }
    }, [user])


    return (
        <><AgentHome/>
        <React.Fragment>
            <Interest fav={fav} loading={loading}/>
        </React.Fragment></> 
    )
}
import React from "react";
import { useLocation } from 'react-router-dom'
import { useNavigate } from "react-router-dom"
import { AgentHome } from "../AgentHome";
import { Loading } from "../Loading";

let fD = undefined
export function ClientProfile({ user }) {

    const navigate = useNavigate()
    const nameRef = React.useRef(null)
    const surnameRef = React.useRef(null)
    const cfRef = React.useRef(null)
    const emaiRef = React.useRef(null)
    const phoneRef = React.useRef(null)
    const noteRef = React.useRef(null)
    let location = useLocation()

    React.useEffect(async () => {
        if (user != null) {
            let id = getClientID()
            let res = await fetch(`/users/${id}?jwt=${user.jwt}`)
            let h = await res.json()
            console.log(h)
            
            if (h.length != 0) {
                cfRef.current.value = h.CF
                emaiRef.current.value = h.email
                nameRef.current.value = h.name
                surnameRef.current.value = h.surname
                phoneRef.current.value = h.phone
                noteRef.current.value = h.note
                fD = new FormData()
            }
        }
    }, [user])

    function getClientID(){
        var n = location.pathname.lastIndexOf('/')
        return location.pathname.slice(n+1)
    }

    return (
        user != null ? <><AgentHome/>
        <React.Fragment>
            <div className="container">
            <div className="row justify-content-center g-2 mt-3">
                <div className="form-floating col-10 col-md-4 m-2">
                    <input type="text" ref={nameRef} className="form-control" disabled={true}/>
                    <label>Nome</label>
                </div>
                <div className="form-floating col-10 col-md-4 m-2">
                    <input type="text" ref={surnameRef} className="form-control" disabled={true}/>
                    <label>Cognome</label>
                </div>
                <div className="form-floating col-10 col-md-3 m-2">
                    <input type="text" ref={cfRef} className="form-control" disabled={true}/>
                    <label>Codice Fiscale</label>
                </div>

                <div className="form-floating col-10 col-md-5 m-2">
                    <input type="email" ref={emaiRef} className="form-control" aria-describedby="emailHelp" disabled={true}/>
                    <label>Email</label>
                </div>
                <div className="form-floating col-10 col-md-5 m-2">
                    <input type="phone" ref={phoneRef} className="form-control" aria-describedby="phonHelp" disabled={true}/>
                    <label>Telefono</label>
                </div>
                <div className="form-control col-10 m-2">
                    <label>Note</label>
                    <textarea placeholder="Aggiungi note" ref={noteRef} className="form-control" disabled={true}/>
                </div>
            </div>

            <div className='d-flex justify-content-end m-2'>
                <button className='btn rounded-circle customerButton' onClick={()=>{
                    navigate('/agentHome/client-edit/'+getClientID())
                }}>
                    <i className="fa fa-pencil" aria-hidden="false" title='Modifica utente'></i>
                </button>
            </div>
            </div>
        </React.Fragment></> : 
            <Loading></Loading>
    )
}
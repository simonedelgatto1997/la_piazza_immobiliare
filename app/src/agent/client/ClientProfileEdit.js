import React from "react";
import { useNavigate, useLocation } from "react-router-dom"
import { AgentHome } from "../AgentHome";
import { Loading } from "../Loading";

let fD = undefined
export function ClientProfileEdit({ user }) {

    const navigate = useNavigate()
    const nameRef = React.useRef(null)
    const surnameRef = React.useRef(null)
    const cfRef = React.useRef(null)
    const emaiRef = React.useRef(null)
    const phoneRef = React.useRef(String)
    const noteRef = React.useRef(String)
    let location = useLocation()

    React.useEffect(async () => {
        if (user != null) {
            let id = getClientID()
            let res = await fetch(`/users/${id}?jwt=${user.jwt}`)
            let h = await res.json()
            
            if (h.length != 0) {
                console.log('Find it')
                cfRef.current.value = h.CF
                nameRef.current.value = h.name
                surnameRef.current.value = h.surname
                emaiRef.current.value = h.email
                phoneRef.current.value = h.phone
                noteRef.current.value = h.note
                id = h._id
                fD = new FormData()
            }
        }
    }, [user])


    function getClientID(){
        var n = location.pathname.lastIndexOf('/')
        return location.pathname.slice(n+1)
    }

    return (
        user != null ? <><AgentHome/><React.Fragment>
            <div className="container">
            <div className="row justify-content-center g-2 mt-3">
                <div className="form-floating col-10 col-md-4 m-2">
                    <input type="text" ref={nameRef} className="form-control"/>
                    <label>Nome</label>
                </div>
                <div className="form-floating col-10 col-md-4 m-2">
                    <input type="text" ref={surnameRef} className="form-control"/>
                    <label>Cognome</label>
                </div>
                <div className="form-floating col-10 col-md-3 m-2">
                    <input type="text" ref={cfRef} className="form-control" placeholder = "CF"/>
                    <label>Codice Fiscale</label>
                </div>
                <div className="form-floating col-10 col-md-5 m-2 ">
                    <input type="email" ref={emaiRef} className="form-control" aria-describedby="emailHelp"/>
                    <label>Email</label>
                </div>
                <div className="form-floating col-10 col-md-5 m-2 ">
                    <input type="email" ref={phoneRef} className="form-control" aria-describedby="phoneHelp"/>
                    <label>Telefono</label>
                </div>
                
                <div className="col-10 col-md-5 m-2">
                    <label htmlFor="CI" className="form-label">Carta d'identità</label>
                    <input className="form-control" type="file" id="CI"
                        onChange={(e) => {
                            for (let i = 0; i < e.target.files.length; i++) {
                                let f = e.target.files[i]
                                console.log('append file')
                                fD.append('CI', f)
                            }
                        }} multiple={true}></input>
                </div>
                <div className="col-10 col-md-5 m-2">
                    <label htmlFor="SC" className="form-label">Stato civile</label>
                    <input className="form-control " type="file" id="SC"
                        onChange={(e) => {
                            for (let i = 0; i < e.target.files.length; i++) {
                                let f = e.target.files[i]
                                fD.append('SC', f)
                            }
                        }} multiple={true}></input>
                </div>
                <div className="form-control col-10 m-2">
                    <label>Note</label>
                    <textarea placeholder="Aggiungi note" className="form-control"/>
                </div>
                <div className='d-flex justify-content-end m-2'>
                    <button className='btn rounded-circle customerButton' onClick={async (e) => {
                        e.preventDefault()
                        fD.append('email', emaiRef.current.value)
                        fD.append('name', nameRef.current.value)
                        fD.append('surname', surnameRef.current.value)
                        fD.append('phone', phoneRef.current.value)
                        fD.append('_id', getClientID())
                        fD.append('CF', cfRef.current.value)
                        try {
                            const res = await fetch(`/users/${getClientID()}?jwt=${user.jwt}`, {
                                body: fD,
                                method: 'PUT',
                                "Content-type": "multipart/form-data"
                                })
                            if (res.ok) 
                                navigate(`/agentHome/client/${getClientID()}`)
                        } catch (err) {
                            console.log(err)
                        }
                        }}>
                        <i className="fa fa-floppy-o" aria-hidden="false" title='Salva utente'></i>
                    </button>
                </div>
            </div>
            </div>
        </React.Fragment></> : <Loading/>
    )
}
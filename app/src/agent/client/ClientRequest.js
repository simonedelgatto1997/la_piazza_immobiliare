import React from "react";
import { AgentHome } from "../AgentHome";
import { Loading } from "../Loading";

export function ClientRequest({ req, l }) {

    function showFavourites(){
        return req.map((val, key) => {
            console.log(val.ref)
            return (
            <tr key={key}>
            <td><div>{val.ref}</div></td>
            <td><div>{val.address}</div></td>
            <td><div>{val.price}</div></td>
            </tr>)
        })
    }

    function getFavouritesTable(){
        return req == null ? <div className='text-center'>Non sono presenti immobili da visualizzare</div> : <> 
            <table>
                <thead>
                    <tr>
                    <th>Rif</th>
                    <th>Indirizzo</th>
                    <th>Prezzo</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    {showFavourites()}
                </tbody>
            </table>
        </>
    }

    return (
        !l ? <><AgentHome/>
        <React.Fragment>
            <div className="container">
                {getFavouritesTable()}
            </div>
        </React.Fragment></> : 
            <Loading></Loading>
    )
}
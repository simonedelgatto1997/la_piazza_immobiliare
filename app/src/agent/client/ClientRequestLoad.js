import React from "react";
import { useLocation } from 'react-router-dom'
import { AgentHome } from "../AgentHome";
import { Loading } from "../Loading";
import { ClientRequest } from "./ClientRequest";

export function ClientRequestLoad({ user }) {
    const [request, setRequest] = React.useState([])
    const [loading, setLoading] = React.useState(true)
    let location = useLocation()

    React.useEffect(async () => {
        const id = getClientID()
        if(user != null){
            let res = await fetch(`/users/interests?jwt=${user.jwt}`)
            let i = await res.json()

            if(i != null){
                i.map(function(v){
                    if(JSON.parse(v).id == id){
                        JSON.parse(v).interests.map(async function (e) {
                            fetch(`/houses/${e._id}?jwt=${user.jwt}`).then(res2 => {
                                res2.json().then( i2 => {
                                    if (i2 != null) {
                                        setRequest((prev) => {
                                            return [...prev, {
                                                name: JSON.parse(v).name,
                                                surname: JSON.parse(v).surname,
                                                ref: i2.ref,
                                                address: i2.address,
                                                price: i2.price
                                            }]
                                        })
                                    }
                                })
                            })
                        })
                    }
                })
            }
            setLoading(false)
        }
    }, [user])

   
    function getClientID(){
        var n = location.pathname.lastIndexOf('/')
        return location.pathname.slice(n+1)
    }

   
    return (
        user != null ? <><AgentHome/>
        <React.Fragment>
           <ClientRequest req={request} l={loading}></ClientRequest>
        </React.Fragment></> : 
            <Loading></Loading>
    )
}
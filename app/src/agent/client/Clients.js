import React from "react";
import { AgentHome } from "../AgentHome";
import { Loading } from "../Loading";
import { useNavigate } from "react-router-dom"

export function Clients ({clients, loading}) {

        const navigate = useNavigate()

        function showClient(){
            return clients.map((val, key) => {
                return (
                <tr key={key}>
                <td><div>{val.name}</div></td>
                <td><div>{val.surname}</div></td>
                <td><div>{val.phone}</div></td>
                <td><div>{val.email}</div></td>
                <td>
                    <div className='subnav'>
                        <a className="header_toggle">
                            <i id="header-toggle" className="fa fa-bars text-black"></i>
                        </a>
                        <div className='subnav-content'>
                            <a onClick={() => navigate('/agentHome/client-edit/'+val.id)}>Modifica</a>
                            <br></br>
                            <a onClick={() => navigate('/agentHome/client/'+val.id)}>Visualizza</a>
                            <br></br>
                            <a onClick={() => navigate('/agentHome/client-favourites/'+val.id)}>Mostra preferiti</a>
                        </div>
                    </div>
                </td>     
                </tr>)
            })
        }

        function getClientTable(){
            console.log('in client map elem ')
            return loading ? 
            <Loading/> : clients.lenght == 0 ? <div className='text-center'>Non sono presenti clienti da visualizzare</div> : <> 
                <table>
                    <thead>
                        <tr>
                        <th>Nome</th>
                        <th>Cognome</th>
                        <th>Cellulare</th>
                        <th>Email</th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {showClient()}
                    </tbody>
                </table>
            </>
        }

        return (
            <>
                <AgentHome/>
                <div className='container'>    
                    {getClientTable()}
                </div>
            </>
        );
}


import React from 'react'
import {Clients} from './Clients'

export function CustomerLoad({ user }) {
    const [clients, setClients] = React.useState([])
    const [loading, setLoading] = React.useState(true)

    React.useEffect(async () => {
        if (user != null){
            let res = await fetch(`/users/customers?jwt=${user.jwt}`, {
                method:'GET',
                "Content-type": "multipart/form-data"
            })
            let h = await res.json()
            let cl = []
            console.log('loading')
            if (h.length > 0) {
                h.map(function(v) { 
                    if (v.role == 'customer')
                        cl.push({
                            name: v.name,
                            surname: v.surname,
                            phone: v.phone,
                            email: v.email,
                            id: v._id
                        })
                })
                setClients(cl)
            }
            setLoading(false)
            console.log('. . . loaded')
        }
    }, [user])
    

    return (
        <React.Fragment>
            <Clients clients={clients} loading={loading}></Clients>
        </React.Fragment>
    )
}
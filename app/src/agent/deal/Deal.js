import React from "react"
import { AgentHome } from '../AgentHome'
import { useNavigate } from "react-router-dom"
import { Loading } from '../Loading';

export function Deal ({ deals, l, user }) {

    const navigate = useNavigate()

    function showDeal(){
        return deals.map((val, key) => {
            console.log(val.proper)
            return (
                <tr key={key}>
                    <td><div>{val.proper}</div></td>
                    <td><div>{val.customer}</div></td>
                    <td><div>{val.house}</div></td>
                    <td>
                        <div className='subnav'>
                            <a className="header_toggle">
                                <i id="header-toggle" className="fa fa-bars text-black"></i>
                            </a>
                            <div className='subnav-content'>
                                <a onClick={() => navigate('/agentHome/deal-edit/' + val.id)}>Modifica</a>                                
                                <br></br>
                                <a onClick={() => navigate('/agentHome/deal/' + val.id)}>Visualizza</a>
                                <br></br>
                                {<a onClick={async () => {
                                    let res = await fetch(`/deal/${val.id}?jwt=${user.jwt}`, {
                                        method: 'DELETE',
                                        "Content-type": "application/json"
                                    })
                                    navigate('/agentHome/shoWDeal/')
                                }}> Elimina</a>}
                            </div>
                        </div>
                    </td>     
                </tr>)
        })
    }

    function showTable(){
        return l ? <Loading/> : 
        deals.lenght == 0 ? 
            <div className='text-center'>Non sono presenti immobili</div> : 
            
            <> <div className='d-flex justify-content-end m-2'>
            <button className='btn rounded-circle customerButton' onClick={()=>{
                navigate('/agentHome/insertDeal')
            }}>
                <i className="fa fa-plus" aria-hidden="false" title='Aggiungi trattativa'></i>
            </button>
            </div>
            <table>
                <thead>
                    <tr>
                    <th>Venditore</th>
                    <th>Acquirente</th>
                    <th>Rif immobile</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    {showDeal()}
                </tbody>
            </table>
            </>
    }
   
    return (
        <>
            <AgentHome/>
            <div className='container'>
                {showTable()}
            </div>
                
        </>
    );
    
}


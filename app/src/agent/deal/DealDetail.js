import React from "react"
import { AgentHome } from '../AgentHome'
import { useNavigate, useLocation } from "react-router-dom"


let fD = new FormData()
export function DealDetail ({ user }) {

    const navigate = useNavigate()
    let location = useLocation()

    const houseRef = React.useRef(null)
    const customerRef = React.useRef(null)
    const stateRef = React.useRef(null)
    const notaryRef = React.useRef(null)
    const dateRef = React.useRef(null)
    const noteRef = React.useRef(null)

    const gasORef = React.useRef(String)
    const gasCRef = React.useRef(String)
    const gasPRef = React.useRef(String)
    const gasBRef = React.useRef(String)
    const gasSRef = React.useRef(String)

    const acqORef = React.useRef(String)
    const acqCRef = React.useRef(String)
    const acqPRef = React.useRef(String)
    const acqBRef = React.useRef(String)
    const acqSRef = React.useRef(String)
    
    const enORef = React.useRef(String)
    const enCRef = React.useRef(String)
    const enPRef = React.useRef(String)
    const enBRef = React.useRef(String)
    const enSRef = React.useRef(String)

    function getDealID(){
        var n = location.pathname.lastIndexOf('/')
        return location.pathname.slice(n+1)
    }

    React.useEffect(async () => {
        if (user != null) {
            let id = getDealID()

            let res = await fetch(`/deal/${id}?jwt=${user.jwt}`)
            let d = await res.json()
                
            if (d.length != 0){
                houseRef.current.value = d.house
                customerRef.current.value = d.customers
                stateRef.current.value = d.state
                notaryRef.current.value = d.notary
                dateRef.current.value = d.first_agreement
                noteRef.current.value = d.stipulation

                if(d.household_utilities.length >= 3 && d.household_utilities[2].type == 'gas'){
                    gasORef.current.value = d.household_utilities[2].operator
                    gasCRef.current.value = d.household_utilities[2].codCust
                    gasPRef.current.value = d.household_utilities[2].pay
                    gasBRef.current.value = d.household_utilities[2].boll
                    gasSRef.current.value = d.household_utilities[2].state
                }

                if(d.household_utilities.length >= 1 && d.household_utilities[0].type == 'water'){
                    acqORef.current.value = d.household_utilities[0].operator
                    acqCRef.current.value = d.household_utilities[0].codCust
                    acqPRef.current.value = d.household_utilities[0].pay
                    acqBRef.current.value = d.household_utilities[0].boll
                    acqSRef.current.value = d.household_utilities[0].state
                }
                
                if(d.household_utilities.length >= 2 && d.household_utilities[1].type == 'energy'){
                    enORef.current.value = d.household_utilities[1].operator
                    enCRef.current.value = d.household_utilities[1].codCust
                    enPRef.current.value = d.household_utilities[1].pay
                    enBRef.current.value = d.household_utilities[1].boll
                    enSRef.current.value = d.household_utilities[1].state
                }
            }
        }
    }, [user])

    return (
        <>
            <AgentHome/>
            <div className='container'>
                <div className="row">
                    <div className="col-6 ">
                        <label className='insertDB' htmlFor="h">Immobile:</label>
                        <input list="houses" id="h" name="hous" ref={houseRef} disabled={true}/>
                    </div>
                    <div className="col-6 ">
                        <label className='insertDB' htmlFor="cust">Acquirente:</label>
                        <input list="customers" id="cust" name="customs" ref={customerRef} disabled={true}/>
                    </div>
                </div>
                <div className="row">
                    <div className='col-3'></div>
                    <div className="col-6 ">
                        <label className='insertDB' htmlFor="state">Stato:</label>
                        <input id='state' ref={stateRef} disabled={true}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 ">
                        <label className='insertDB' htmlFor="notaio">Notaio:</label>
                        <input id='notaio' ref={notaryRef} disabled={true}/>  
                    </div>

                    <div className="col-6 ">
                        <label className='insertDB' htmlFor="date">Data di stipula:</label>
                        <input id='date' type='datetime-local' ref={dateRef} disabled={true}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6">
                        <label className='insertDB' htmlFor="propost">Proposta:</label>
                        <input className="" type="file" id='propost' multiple={true} disabled={true}/>
                    </div>
                
                    <div className="col-6 ">
                        <label className='insertDB' htmlFor="note">Note</label>
                        <input id='note' ref={noteRef} disabled={true}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-10">
                        <label className='insertDB' htmlFor="ut">Utenze:</label>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col"></th>
                                <th scope="col">Operatore</th>
                                <th scope="col">Codice cliente</th>
                                <th scope="col">Pagamento</th>
                                <th scope="col">Bollette</th>
                                <th scope="col">Stato</th>
                                <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">Acqua</th>
                                    <td><input id='ut' ref={acqORef} disabled={true}/></td>
                                    <td><input id='ut' ref={acqCRef} disabled={true}/></td>
                                    <td><input id='ut' ref={acqPRef} disabled={true}/></td>
                                    <td><input id='ut' ref={acqBRef} disabled={true}/></td>
                                    <td><input id='ut' ref={acqSRef} disabled={true}/></td>
                                    <td><button className='btn rounded-circle customerButton' disabled={true}>
                                            <i className="fa fa-check" aria-hidden="false" title='Salva'></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Luce</th>
                                    <td><input id='ut' ref={enORef} disabled={true}/></td>
                                    <td><input id='ut' ref={enCRef} disabled={true}/></td>
                                    <td><input id='ut' ref={enPRef} disabled={true}/></td>
                                    <td><input id='ut' ref={enBRef} disabled={true}/></td>
                                    <td><input id='ut' ref={enSRef} disabled={true}/></td>
                                    <td><button className='btn rounded-circle customerButton' disabled={true} >
                                            <i className="fa fa-check" aria-hidden="false" title='Salva'></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Gas</th>
                                    <td><input id='ut' ref={gasORef} disabled={true}/></td>
                                    <td><input id='ut' ref={gasCRef} disabled={true}/></td>
                                    <td><input id='ut' ref={gasPRef} disabled={true}/></td>
                                    <td><input id='ut' ref={gasBRef} disabled={true}/></td>
                                    <td><input id='ut' ref={gasSRef} disabled={true}/></td>
                                    <td><button className='btn rounded-circle customerButton' disabled={true} >
                                            <i className="fa fa-check" aria-hidden="false" title='Salva'></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                            </table>

                    </div>
                </div>

                <div className='d-flex justify-content-end m-2'>
                    <button className='btn rounded-circle customerButton' onClick={(e)=>{
                        e.preventDefault()
                        navigate('/agentHome/deal-edit/'+getDealID())
                    }}>
                        <i className="fa fa-pencil" aria-hidden="false" title='Modifica trattativa'></i>
                    </button>
                </div>
            </div>
        </>
    );
    
}


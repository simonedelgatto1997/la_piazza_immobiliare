import React from "react"
import { AgentHome } from '../AgentHome'
import { useNavigate, useLocation } from "react-router-dom"

let fD = new FormData()
export function DealDetailEdit ({ user }) {

    const navigate = useNavigate()
    let location = useLocation()

    const houseRef = React.useRef(null)
    const customerRef = React.useRef(null)
    const stateRef = React.useRef(null)
    const notaryRef = React.useRef(null)
    const dateRef = React.useRef(null)
    const noteRef = React.useRef(null)

    const gasORef = React.useRef(String)
    const gasCRef = React.useRef(String)
    const gasPRef = React.useRef(String)
    const gasBRef = React.useRef(String)
    const gasSRef = React.useRef(String)

    const acqORef = React.useRef(String)
    const acqCRef = React.useRef(String)
    const acqPRef = React.useRef(String)
    const acqBRef = React.useRef(String)
    const acqSRef = React.useRef(String)
    
    const enORef = React.useRef(String)
    const enCRef = React.useRef(String)
    const enPRef = React.useRef(String)
    const enBRef = React.useRef(String)
    const enSRef = React.useRef(String)

    function getDealID(){
        var n = location.pathname.lastIndexOf('/')
        return location.pathname.slice(n+1)
    }

    var [myCustomer, setMyCustomer] = React.useState([])
    const getCustomerAPI = async () => {
        myCustomer = [];
        let r = await fetch(`/users/customers?jwt=${user.jwt}`, {                                
                            method: 'GET',
                            "Content-type": "application/json"
                        })
                                    
        let h = await r.json()
        if (h.length > 0) {
            h.map(function(v) { 
                myCustomer.push(v)
            })
        }

        setMyCustomer(myCustomer)
    }

    var [myHouse, setMyHouse] = React.useState([])
    const getHouseAPI = async () => {
        myHouse = [];
        let r = await fetch(`/houses?filters={}`, {                                
                            method: 'GET',
                            "Content-type": "application/json"
                        })
                                    
        let h = await r.json()
        if (h.length > 0) {
            h.map(function(v) { 
                myHouse.push(v)
            })
        }

        setMyHouse(myHouse)
    }

    React.useEffect(async () => {
        if (user != null) {
            let id = getDealID()

            let res = await fetch(`/deal/${id}?jwt=${user.jwt}`)
            let d = await res.json()
                
            if (d.length != 0){
                houseRef.current.value = d.house
                customerRef.current.value = d.customers
                stateRef.current.value = d.state
                notaryRef.current.value = d.notary
                dateRef.current.value = d.first_agreement
                noteRef.current.value = d.stipulation

                if(d.household_utilities.length >= 3 && d.household_utilities[2].type == 'gas'){
                    gasORef.current.value = d.household_utilities[2].operator
                    gasCRef.current.value = d.household_utilities[2].codCust
                    gasPRef.current.value = d.household_utilities[2].pay
                    gasBRef.current.value = d.household_utilities[2].boll
                    gasSRef.current.value = d.household_utilities[2].state
                }

                if(d.household_utilities.length >= 1 && d.household_utilities[0].type == 'water'){
                    acqORef.current.value = d.household_utilities[0].operator
                    acqCRef.current.value = d.household_utilities[0].codCust
                    acqPRef.current.value = d.household_utilities[0].pay
                    acqBRef.current.value = d.household_utilities[0].boll
                    acqSRef.current.value = d.household_utilities[0].state
                }
                
                if(d.household_utilities.length >= 2 && d.household_utilities[1].type == 'energy'){
                    enORef.current.value = d.household_utilities[1].operator
                    enCRef.current.value = d.household_utilities[1].codCust
                    enPRef.current.value = d.household_utilities[1].pay
                    enBRef.current.value = d.household_utilities[1].boll
                    enSRef.current.value = d.household_utilities[1].state
                }
            }

            getCustomerAPI()
            getHouseAPI()
        }
    }, [user])

    function showCustomer(){
        return <div className="styled-select">
            <select data-component="acquirenti">
                {myCustomer.map((e) => 
                    { return <option key={e._id} value={e.name + " " + e.surname}>{e.phone}</option>})
                }
            </select>
        </div>
    }

    function showHouses(){
        return <div className="styled-select">
            <select data-component="houses">
                {myHouse.map((e) => 
                    { return <option key={e._id} value={e.ref}>{e.price}</option>})
}
            </select>
        </div>
    }

    return (
        <>
            <AgentHome/>
            <div className='container'>
                <div className="row">
                    <div className="col-6 ">
                        <label className='insertDB' htmlFor="h">Immobile:</label>
                        <input list="houses" id="h" name="hous" ref={houseRef}/>
                        <datalist id="houses">
                            {showHouses()}
                        </datalist>
                    </div>
                    <div className="col-6 ">
                        <label className='insertDB' htmlFor="cust">Acquirente:</label>
                        <input list="customers" id="cust" name="customs" ref={customerRef}/>
                        <datalist id="customers">
                            {showCustomer()}
                        </datalist>
                    </div>
                </div>
                <div className="row">
                    <div className='col-3'></div>
                    <div className="col-6 ">
                        <label className='insertDB' htmlFor="state">Stato:</label>
                        <input id='state' ref={stateRef}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6 ">
                        <label className='insertDB' htmlFor="notaio">Notaio:</label>
                        <input id='notaio' ref={notaryRef}/>  
                    </div>

                    <div className="col-6 ">
                        <label className='insertDB' htmlFor="date">Data di stipula:</label>
                        <input id='date' type='datetime-local' ref={dateRef}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6">
                        <label className='insertDB' htmlFor="propost">Proposta:</label>
                        <input className="" type="file" id='propost' 
                            onChange={(e) => {
                                for (let i = 0; i < e.target.files.length; i++) {
                                let f = e.target.files[i]
                                console.log('append file')
                                fD.append('first_agreement', f)
                                }
                        }} multiple={true}/>
                    </div>
                
                    <div className="col-6 ">
                        <label className='insertDB' htmlFor="note">Note</label>
                        <input id='note' ref={noteRef}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-10">
                        <label className='insertDB' htmlFor="ut">Utenze:</label>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col"></th>
                                <th scope="col">Operatore</th>
                                <th scope="col">Codice cliente</th>
                                <th scope="col">Pagamento</th>
                                <th scope="col">Bollette</th>
                                <th scope="col">Stato</th>
                                <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">Acqua</th>
                                    <td><input id='ut' ref={acqORef}/></td>
                                    <td><input id='ut' ref={acqCRef}/></td>
                                    <td><input id='ut' ref={acqPRef}/></td>
                                    <td><input id='ut' ref={acqBRef}/></td>
                                    <td><input id='ut' ref={acqSRef}/></td>
                                    <td><button className='btn rounded-circle customerButton' onClick={()=>{
                                         fD.append('household_utilities', JSON.stringify({
                                            type: 'water',
                                            operator: acqORef.current.value,
                                            codCust: acqCRef.current.value,
                                            pay: acqPRef.current.value,
                                            boll: acqBRef.current.value,
                                            state: acqSRef.current.value,
                                        }))
                                    }}>
                                            <i className="fa fa-check" aria-hidden="false" title='Salva'></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Luce</th>
                                    <td><input id='ut' ref={enORef}/></td>
                                    <td><input id='ut' ref={enCRef}/></td>
                                    <td><input id='ut' ref={enPRef}/></td>
                                    <td><input id='ut' ref={enBRef}/></td>
                                    <td><input id='ut' ref={enSRef}/></td>
                                    <td><button className='btn rounded-circle customerButton' onClick={()=>{
                                        fD.append('household_utilities', JSON.stringify({
                                            type: 'energy',
                                            operator: enORef.current.value,
                                            codCust: enCRef.current.value,
                                            pay: enPRef.current.value,
                                            boll: enBRef.current.value,
                                            state: enSRef.current.value,
                                        }))
                                    }}>
                                            <i className="fa fa-check" aria-hidden="false" title='Salva'></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Gas</th>
                                    <td><input id='ut' ref={gasORef}/></td>
                                    <td><input id='ut' ref={gasCRef}/></td>
                                    <td><input id='ut' ref={gasPRef}/></td>
                                    <td><input id='ut' ref={gasBRef}/></td>
                                    <td><input id='ut' ref={gasSRef}/></td>
                                    <td><button className='btn rounded-circle customerButton' onClick={()=>{
                                        fD.append('household_utilities', JSON.stringify({
                                            type: 'gas',
                                            operator: gasORef.current.value,
                                            codCust: gasCRef.current.value,
                                            pay: gasPRef.current.value,
                                            boll: gasBRef.current.value,
                                            state: gasSRef.current.value,
                                        }))
                                    }}>
                                            <i className="fa fa-check" aria-hidden="false" title='Salva'></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                            </table>

                    </div>
                </div>

                <div className='d-flex justify-content-end m-2'>
                    <button className='btn rounded-circle customerButton' onClick={async (e)=>{
                        e.preventDefault()
                        fD.append('house', houseRef.current.value)
                        fD.append('customers', customerRef.current.value)
                        fD.append('state', stateRef.current.value)
                        fD.append('notary', notaryRef.current.value)
                        fD.append('first_agreement', dateRef.current.value)
                        fD.append('stipulation', noteRef.current.value)

                        for (var v of fD.values()){
                            console.log(v)
                        }

                        fetch(`/deal/${getDealID()}?jwt=${user.jwt}`, {
                            method: 'PUT',
                            body: fD,
                            "Content-type": "multipart/form-data"
                        }).then( res => {
                            console.log('fetch done in modify deal')
                            navigate('/agentHome/deal/'+getDealID())
                        }).catch(err => {
                            console.log("ERRORE: " + err)
                        })

                    }}>
                        <i className="fa fa-save" aria-hidden="false" title='Salva trattativa'></i>
                    </button>
                </div>
            </div>
        </>
    );
    
}


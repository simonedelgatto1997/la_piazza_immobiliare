import React from "react"
import { Deal } from './Deal'

export function DealLoad ({ user }) {

    const [deals, setDeals] = React.useState([])
    const [loading, setLoading] = React.useState(true)

    React.useEffect(async () => {
        if (user != null){
            let res = await fetch(`/deal?jwt=${user.jwt}`)
            let d = await res.json()
            let filter
            if (d != null) {
                d.map(async function (v) { 
                    filter = {ref: v.house}
                    fetch(`/houses?filters=${JSON.stringify(filter)}`).then( (res2) => 
                        res2.json().then( res => {   
                            setDeals((prev) => {
                                console.log(res)
                                return [...prev, {
                                    proper: res[0].owners != null ? res[0].owners[0][0] : "NON DEFINITO",
                                    customer: v.customers,
                                    house: v.house,
                                    id: v._id //della trattativa
                                }]
                            })}
                        )
                    )
                })
            }
            setLoading(false)
        }
    }, [user])

    return (
        <>
            <React.Fragment>
                <Deal deals={deals} l={loading} user={user}></Deal>
            </React.Fragment>
        </>
    );
    
}


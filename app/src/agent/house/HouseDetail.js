import React from "react";
import { useNavigate, useLocation } from "react-router-dom"
import { AgentHome } from "../AgentHome";
import { Loading } from "../Loading"
import { getCadrastalCategory } from '../../house/cadrastalCategory';
import { getRenderingCategories } from '../../house/category';
import { getRenderingAgreementTypes } from '../../house/agreementTypes';
import { getRenderingBuildingStates } from '../../house/buildingState';
import { getRenderingFloor } from '../../house/floor';
import { getRenderingEnergeticClass } from '../../house/energeticClass';
import { getRenderingMandatory } from '../../house/mandatoryType';
import { getRenderingContractTypes } from '../../house/contractType'

let fD = new FormData()
export function HouseDetail({ user }){
    const navigate = useNavigate()

    let location = useLocation()
    const [hd, setHd] = React.useState()

    React.useEffect( async () => {
        if(user != null){
            let id = getHouseID()
            let res = await fetch(`/houses/${id}?jwt=${user.jwt}`)
            let h = await res.json()
            setHd(h)
        }
    }, [user])

    function getHouseID(){
        var n = location.pathname.lastIndexOf('/')
        return location.pathname.slice(n+1)
    }


    return (
        (hd != null) ? <>
            <AgentHome/>
            <React.Fragment>
            <div className='container text-center'>    
            <form role="form" className="gtx-form" method="post" id="gtx-form-ins-annuncio">
            
            <div data-error-target="annuncio" className="step2-form-error  gtx-standard-margin" hidden={true}>
                <div className="alert alert-danger"></div>
            </div>

            <div className="panel" data-component="tipologies">
                <div className="panel-heading"><h4>Tipologia</h4></div>
                <div className="panel-body">                  
                    <div className="row">
                        <div className="col-4">Categoria
                        <div className="styled-select" >
                                <select data-component="gruppo" value={hd.category} disabled={true}>
                                {getRenderingCategories()}
                                </select>
                            </div>
                        </div>
                        <div className="col-4"> Contratto
                            <div className="styled-select" >
                                <select data-component="contratto" value={hd.agreement} disabled={true}>
                                {getRenderingAgreementTypes()}
                                </select>
                            </div>
                        </div>
                        <div className="col-4">Tipo proprietà
                            <div className="styled-select">
                                <select data-component="tipoProprieta" value={hd.property_type} disabled={true}>
                                { getRenderingContractTypes() }
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="panel" data-component="contract-price" data-props-category="1">
                <div className="panel-heading"><h4>Prezzo e costi</h4></div>
                <div className="panel-body">
                    <div className="row">
                        <div className="col-6">Richiesta
                            <div className="input-group mb-3">
                                <span className="input-group-text">€</span>
                                <input type="number" value={hd.price} className="form-control" id="prezzo" disabled={true}/>
                            </div>
                        </div>

                        <div className="col-6">Spese condominiali
                            <div className="input-group mb-3">
                                <span className="input-group-text" id="basic-addon3">€/anno</span>
                                <input type="text" value={hd.condominium_fees} className="form-control" id="speseCond" disabled={true}/>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-4 ">Libero                
                            <div data-component="libero">
                                <input name="libero" value={hd.free_from} id="libero" type="text" disabled={true}/>
                            </div>
                        </div>                       
                        <div className="col-4">Affitto con riscatto
                            <div value={hd.rent_to_buy}>
                                <input type="radio" className="btn-check" name="options-outlined rentToBuy" id="rentToBuy-true" checked={hd.rent_to_buy == null ? false : hd.rent_to_buy} disabled={true}/>
                                    <label className="btn btn-outline-primary" htmlFor="rentToBuy-true" disabled={true}>Sì</label>
                                <input type="radio" className="btn-check" name="options-outlined rentToBuy" id="rentToBuy-false" autoComplete="off" checked={hd.rent_to_buy == null ? false : !hd.rent_to_buy} disabled={true}/>
                                    <label className="btn btn-outline-primary" htmlFor="rentToBuy-false" disabled={true}>No</label>   
                            </div>
                        </div>
                        <div className="col-4">Immobile a reddito
                        <div value={hd.income_property}>
                                <input type="radio" className="btn-check" name="options-outlined incomeProp" id="incomeProp-true" autoComplete="off" checked={hd.income_property == null ? false : hd.income_property} disabled={true}/>
                                    <label className="btn btn-outline-primary" htmlFor="incomeProp-true" disabled={true}>Sì</label>
                                <input type="radio" className="btn-check" name="options-outlined incomeProp" id="incomeProp-false" autoComplete="off" checked={!hd.income_property} disabled={true}/>
                                    <label className="btn btn-outline-primary" htmlFor="incomeProp-false" disabled={true}>No</label>   
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="panel" data-component="surface-details">
                <div className="panel-heading" id="superficie">
                    <h4>Superficie</h4>
                </div>
                <div className="panel-body">
                    <div className='row'>
                        <div className='col-12'>
                            <input type="number" value={hd.square_meter} step="0.1" disabled={true}/>
                            <span className="input-group-addon">   m<sup>2</sup></span>
                        </div>
                    </div>
                </div>
            </div>

            <div className="panel" data-component="composition-residential">
                <div className="panel-heading" id="composizione">
                    <h4>Composizione</h4>
                </div>
                
                <div className="panel-body">
                    <div data-error-target="composizione" className="step2-form-error alert alert-danger" hidden={true}></div>
                        <div className="col-12">
                            <div className="row mb-3">
                                <div className='col-2'></div>
                                <div className='col-4'>Numero stanze
                                    <div className="input-group">
                                        <input type="number" value={hd.rooms} className="form-control" disabled={true}/>
                                    </div>
                                </div>
                                <div className="col-4">Bagni
                                    <div className="input-group">
                                        <input type="number" value={hd.bathroom} className="form-control" disabled={true}/>
                                    </div>
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-4 ">Cucina
                                    <div className="styled-select">
                                        <select data-component="cucina" value={hd.kitchen} disabled={true}>
                                            <option value="">Scegli</option>
                                            <option value="Abitabile">Abitabile</option>
                                            <option value="Angolo cottura">Angolo cottura</option>
                                            <option value="Cucinotto">Cucinotto</option>
                                        </select>
                                    </div>
                                </div>                  
                                <div className="col-4 ">Giardino
                                    <div className="styled-select">
                                        <select data-component="idGiardino" value={hd.garden} disabled={true}>
                                            <option value="false">Scegli</option>
                                            <option value="true">Privato</option>
                                            <option value="false">Comune</option>
                                            <option value="false">Nessuno</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-4 "> Garage
                                    <div className="styled-select">
                                        <select data-component="boxauto" value={hd.garage} disabled={true}>
                                            <option value="false">Scegli</option>
                                            <option value="true">Singolo</option>
                                            <option value="true">Doppio</option>
                                            <option value="false">Nessuno</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="row mb-1">
                                <div className="col-4 "> Balcone / terrazzo
                                    <div value={hd.balcony}>
                                        <input type="radio" className="btn-check" name="options-outlined balcony" id="balcony-true" autoComplete="off" checked={hd.balcony} disabled={true}/>
                                            <label className="btn btn-outline-primary" htmlFor="balcony-true" disabled={true}>Sì</label>
                                        <input type="radio" className="btn-check" name="options-outlined balcony" id="balcony-false" autoComplete="off" checked={!hd.balcony} disabled={true}/>
                                            <label className="btn btn-outline-primary" htmlFor="balcony-false" disabled={true}>No</label> 
                                    </div>
                                </div>
                                <div className="col-4"> Cantina
                                    <div value={hd.cellar}>
                                        <input type="radio" className="btn-check" name="options-outlined cellar" id="cellar-true" autoComplete="off" checked={hd.cellar} disabled={true}/>
                                            <label className="btn btn-outline-primary" htmlFor="cellar-true" disabled={true}>Sì</label>
                                        <input type="radio" className="btn-check" name="options-outlined cellar" id="cellar-false" autoComplete="off" checked={!hd.cellar} disabled={true}/>
                                            <label className="btn btn-outline-primary" htmlFor="cellar-false" disabled={true}>No</label>   
                                    </div>
                                </div>
                                <div className="col-4 ">Ingresso indipendente
                                    <div value={hd.indipendent_entrace}>
                                        <input type="radio" className="btn-check" name="options-outlined ingrIndip" id="ingrIndip-true" autoComplete="off" checked={hd.indipendent_entrace == null ? false : hd.indipendent_entrace} disabled={true}/>
                                            <label className="btn btn-outline-primary" htmlFor="ingrIndip-true" disabled={true}>Sì</label>
                                        <input type="radio" className="btn-check" name="options-outlined ingrIndip" id="ingrIndip-false" autoComplete="off" checked={hd.indipendent_entrace == null ? false : !hd.indipendent_entrace} disabled={true}/>
                                            <label className="btn btn-outline-primary" htmlFor="ingrIndip-false" disabled={true}>No</label>   
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

            <div className="panel" data-component="feautures-residential" data-props-ad-id="">
                <div className="panel-heading">
                    <h4>Caratteristiche</h4>
                </div>
            <div className="panel-body">
                <div className="col-12">
                    <div className="row mb-2">
                        <div className='col-2'></div>
                        <div className="col-4 ">Anno costruzione
                            <input type="text" value={hd.building_year} className="form-control" data-parsley-min="1000" data-parsley-max="2100" data-parsley-min-message="Questo valore deve essere maggiore di 999." disabled={true}/>
                        </div>  
                        <div className="col-4">Stato
                            <div className="styled-select">
                                <select data-component="stato" value={hd.building_state} disabled={true}>
                                    {getRenderingBuildingStates()}
                                </select>
                            </div>
                        </div>                     
                    </div>
                    <div className="row mb-2">
                        <div className="col-4 ">Piano
                            <div className="styled-select">
                                <select data-component="piano" value={hd.floor} disabled={true}>
                                    {getRenderingFloor()}
                                </select>
                            </div>
                        </div>
                        <div className="col-4 ">Su _ piani
                            <input type="number" value={hd.floors_number} className="form-control" disabled={true}/>
                        </div> 
                        <div className="col-4">Riscaldamento
                            <div className="styled-select">
                                <select data-component="riscaldamento" value={hd.heating} disabled={true}>
                                    <option value="">Scegli</option>
                                    <option value="Autonomo a pavimento">Autonomo a pavimento</option>
                                    <option value="Autonomo a radiatori">Autonomo a radiatori</option>
                                    <option value="Centralizzato a radiatori">Centralizzato a radiatori</option>
                                    <option value="Centralizzato ad aria">Centralizzato ad aria</option>
                                    <option value="Assente">Assente</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-4">Ascensore
                            <div value={hd.lift}>
                                <input type="radio" className="btn-check" name="options-outlined lift" id="lift-true" autoComplete="off" checked={hd.lift == null ? false : hd.lift} disabled={true}/>
                                    <label className="btn btn-outline-primary" htmlFor="lift-true" disabled={true}>Sì</label>
                                <input type="radio" className="btn-check" name="options-outlined lift" id="lift-false" autoComplete="off" checked={hd.lift == null ? false : !hd.lift} disabled={true}/>
                                    <label className="btn btn-outline-primary" htmlFor="lift-false" disabled={true}>No</label>   
                            </div>
                        </div>
                    
                        <div className="col-4">Accesso per disabili
                            <div value={hd.disabled_entrace}>
                                <input type="radio" className="btn-check" name="options-outlined disabEnt" id="disabEnt-true" autoComplete="off" checked={hd.disabled_entrace == null ? false : hd.disabled_entrace} disabled={true}/>
                                    <label className="btn btn-outline-primary" htmlFor="disabEnt-true" disabled={true}>Sì</label>
                                <input type="radio" className="btn-check" name="options-outlined disabEnt" id="disabEnt-false" autoComplete="off" checked={hd.disabled_entrace == null ? false : !hd.disabled_entrace} disabled={true}/>
                                    <label className="btn btn-outline-primary" htmlFor="disabEnt-false" disabled={true}>No</label>   
                            </div>
                        </div>
                    
                        <div className="col-4">Impianto climatizzazione
                            <div value={hd.air_conditioner}>
                                <input type="radio" className="btn-check" name="options-outlined ac" id="ac-true" autoComplete="off" checked={hd.air_conditioner == null ? false : hd.air_conditioner} disabled={true}/>
                                    <label className="btn btn-outline-primary" htmlFor="ac-true" disabled={true}>Sì</label>
                                <input type="radio" className="btn-check" name="options-outlined ac" id="ac-false" autoComplete="off" checked={hd.air_conditioner == null ? false : hd.air_conditioner} disabled={true}/>
                                    <label className="btn btn-outline-primary" htmlFor="ac-false" disabled={true}>No</label>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <div className="panel" data-component="energetic-rating" data-props-id-categoria="1">
                <div className="panel-heading" id="certificazione-energetica">
                    <h4>Certificazione Energetica</h4>
                </div>

                <div className="panel-body">
                <div className="col-12">
                    <div data-component="ipe-certification">
                        <div className="row">
                            <div data-error-target="classeEnergetica" className="step2-form-error alert alert-danger" hidden={true}></div>
                            <div className="col-4">Classe Energetica
                                <div className="styled-select ">
                                    <select value={JSON.parse(hd.indexAPE).scale} disabled={true}>
                                        <option value="">Scegli</option>
                                        { getRenderingEnergeticClass() }
                                    </select>
                                </div>
                            </div>
                        
                            <div className="col-6">
                                Indice di prestaz. energetica
                                <input value={JSON.parse(hd.indexAPE).value} type="text" className="form-control"  disabled={true}/>
                                kWh/m² anno
                            </div>
                        </div>
                    </div>

                
                </div>
            </div>
            </div>

            <div className="panel" data-component="cadastral-infos">
                <div className="panel-heading" id="datiCatastali"><h4>Dati catastali</h4></div>
                <div className="panel-body">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-6 ">
                                Sezione
                                <input type="text" value={JSON.parse(hd.cadastral_data).sezione} data-component="sezione" className="form-control"  disabled={true}/>
                            </div>
                            <div className="col-6 ">
                                Foglio
                                <input type="text" value={JSON.parse(hd.cadastral_data).foglio} data-component="foglio" className="form-control"  disabled={true}/>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="col-6 ">
                                Particella
                                <input type="text" value={JSON.parse(hd.cadastral_data).particella} data-component="particella" className="form-control"  disabled={true}/>
                            </div>
                            <div className="col-6 ">
                                Subalterno
                                <input type="text" value={JSON.parse(hd.cadastral_data).subalterno} data-component="subalterno" className="form-control"  disabled={true}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-6">
                                Rendita catastale
                                <span className="tip">
                                    <span className="badge label-primary pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Valore riportato in scheda catastale. Inserire il valore in euro. Utilizzare la virgola come separatore per le cifre decimali.">?
                                    </span>
                                </span>
                            
                                <div className="input-group no-tip-sm">
                                    <div className="input-group-addon">€</div>
                                    <input type="text" value={JSON.parse(hd.cadastral_data).rendita_catastale} data-component="rendita_catastale" className="form-control" data-props-precision="2" data-parsley-number-formatted="true"  disabled={true}/>
                                </div>
                            </div>

                            <div className="col-12 col-sm-6">
                                Categoria Catastale
                                <div className="styled-select">
                                    <select data-component="classe_catastale" value={JSON.parse(hd.cadastral_data).categoria} disabled={true}>
                                        {getCadrastalCategory()}
                                    </select>
                                </div>
                            </div>
                    
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="panel" data-component="brokerage">
                <div className="panel-heading"><h4>Dati intermediazione</h4></div>
                <div className="panel-body">
                    <div className="col-12">
                        <div className="row mb-2">
                            <div className="col-4 ">
                                Riferimento annuncio
                                <input type="text" value={hd.ref} className="form-control"  disabled={true}/>
                            </div>
                            <div className="col-4 ">
                                Priorità (1MAX)
                                <div className='input-group'>
                                    <span className="input-group-text">1</span>
                                    <input type="range" value={hd.priority} min="1" max="10" data-component="codice" className="form-control" disabled={true}/>
                                    <span className="input-group-text">10</span>
                                </div>
                            </div>
                            <div className="col-4">
                                Stato
                                <div className="styled-select">
                                <select data-component="stato" value={hd.state} disabled={true}>
                                    <option value="Attivo">Attivo</option>
                                    <option value="Sospeso">Sospeso</option>
                                </select>
                            </div>
                            </div>
                        </div>
                    
                        <div className="row">
                        <div className='col-3'></div>
                            <div className="col-6">
                                <label htmlFor="prop">Proprietario</label>
                                <input list="clients" id="prop" name="customs" value={hd.owners} disabled={true}/>
                                <datalist id="clients">
                                </datalist>
                            </div>
                        </div>
            
                    <div className="row mb-2">
                        <div className="col-6">Tipo Mandato
                            <div className="styled-select">
                                <select data-component="tipoMandato" disabled={true}>
                                    {getRenderingMandatory()}
                                </select>
                            </div>
                        </div>
                        <div className="col-6 ">
                            Scadenza Mandato
                            <div className="input-group date-picker">
                                <input type="date" className="form-control" data-component="gtxScadenzaMandato"  data-parsley-date="true" disabled={true}/>
                            </div>
                        </div>
                    </div>
            
                </div>
            </div>
            </div>
            
            <div data-component="description" data-props-max-length="3000" data-props-max-title-length="60">
                    <div className="panel">
                        <div className="panel-heading" id="descrizione">
                            <h4>Descrizione per i portali web</h4>
                        </div>
                        <div className="panel-body">
                            <div className="col-12">
                            <div className="tab-content" data-role="desc-text-container">
                                <div id="tab-panel-1" data-desc-lang="it" className="tab-pane active">
                                    <input type="text" value={hd.description.slice(0, hd.description.indexOf(';'))} className="form-control mb-3" placeholder="Inserisci il titolo (max 60 caratteri)" data-component="titolo-descrizione-it" maxLength="60"  disabled={true}/>
                                    <textarea value={hd.description.slice(hd.description.indexOf(';')+1)} data-component="descrizione" data-parsley-maxwords="3000" className="form-control" cols="133" rows="8" placeholder="Inserisci la descrizione" disabled={true}></textarea>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
            </div>

            <div className="panel" data-component="location">
                <div className="panel-heading"><h4>Posizione</h4></div>
                <div className="panel-body">
                <div className="col-12">
                        <div className='row'>
                            <div className="col-6 ">
                                Citta'
                                <input type="text" value={(hd.address).slice(0, hd.address.indexOf(';'))} data-component="city" className="form-control" disabled={true}/>
                                Indirizzo
                                <input type="text" value={(hd.address).slice(hd.address.indexOf(';')+1)} data-component="street" className="form-control"  disabled={true}/>
                            </div>
                            <div className='col-6' id='map'>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2854.587087088543!2d11.796796915519481!3d44.3184384791041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132b48361b680f27%3A0x12cd8d8cffa0cb4d!2sL&#39;Agenzia%20LA%20PIAZZA!5e0!3m2!1sit!2sit!4v1645971567734!5m2!1sit!2sit"></iframe>
                            </div>         
                        </div>        
                    </div>
                </div>
            </div>

            <div className="panel" data-component="photo">
                <div className="panel-heading"><h4>Foto</h4></div>
                <div className="panel-body">
                    
                    <div className="col-12"> 
                        <input className="form-control" type="file" id="photos"
                            multiple={true} disabled={true}></input>
                    </div>
                </div>
            </div>

            <div className="panel" data-component="document">
                <div className="panel-heading"><h4>Documentazione</h4></div>
                <div className="panel-body">
                    <div className="col-12">
                        <div className='row mb-3'>
                            <div className="col-4">
                                <label htmlFor="surv" className="form-label">Geometra incaricato</label>
                                <input type="text" value={hd.survayor} className="form-control"  disabled={true}/>
                            </div>
                            <div className="col-4 ">
                                <label htmlFor="APE" className="form-label">Certificazione energetica</label>
                                <input className="form-control" type="file" id="APE" disabled={true}></input>
                            </div>
                            <div className="col-4">
                                <label htmlFor="RTI" className="form-label">Relazione Tecnica Integrata</label>
                                <input className="form-control" type="file" id="RTI" disabled={true}></input>                        
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-4 mb-3 ">
                                <label htmlFor="AAA" className="form-label">Accesso agli atti</label>
                                <input className="form-control" type="file" id="AAA" multiple={true} disabled={true}></input>
                            </div>

                            <div className="col-4 mb-3 ">
                                <label htmlFor="cadastral_plan" className="form-label">Planimetrie Catastali</label>
                                <input className="form-control" type="file" id="cadastral_plan" disabled={true}></input>
                            </div>

                            <div className="col-4 mb-3">
                                <label htmlFor="certificate" className="form-label">Visure catastali</label>
                                <input className="form-control" type="file" id="certificate" disabled={true}></input>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

            <div className='d-flex justify-content-end m-2'>
                <button className='btn rounded-circle customerButton' onClick={()=>{
                    navigate('/agentHome/house-edit/'+getHouseID())
                }}>
                    <i className="fa fa-pencil" aria-hidden="false" title='Modifica immobile'></i>
                </button>
            </div>
        </form>
            
            </div>
            </React.Fragment>
        </> : <Loading/>
    )
}
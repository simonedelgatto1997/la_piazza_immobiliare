import * as React from "react";
import { useNavigate, useLocation } from "react-router-dom"
import { AgentHome } from "../AgentHome";
import { Loading } from "../Loading"

import { getCadrastalCategory } from '../../house/cadrastalCategory';
import { getRenderingCategories } from '../../house/category';
import { getRenderingAgreementTypes } from '../../house/agreementTypes';
import { getRenderingBuildingStates } from '../../house/buildingState';
import { getRenderingFloor } from '../../house/floor';
import { getRenderingEnergeticClass } from '../../house/energeticClass';
import { getRenderingMandatory } from '../../house/mandatoryType';
import { getRenderingContractTypes } from '../../house/contractType'

let fD = new FormData()
export function HouseDetailEdit({ user }){

    const navigate = useNavigate()
    const categoryRef = React.useRef(null)
    const agreementRef = React.useRef(null)
    const propertyRef = React.useRef(null)
    var priceRef = React.useRef(Number)
    var condominiumFeesRef = React.useRef(Number)
    const freeFromRef = React.useRef(null)
    const rentToBuyRef = React.useRef(Boolean)
    const incomePropertyRef = React.useRef(Boolean)
    const squareMeterRef = React.useRef(null)
    const roomsRef = React.useRef(null)
    const bathroomsRef = React.useRef(null)
    const kitchenRef = React.useRef(null)
    const gardenRef = React.useRef(Boolean)
    const garageRef = React.useRef(Boolean)
    var cellarRef = React.useRef(Boolean)
    var balconyRef = React.useRef(Boolean)
    var indipendentEntraceRef = React.useRef(Boolean)
    const airConditionerRef = React.useRef(Boolean)
    const disabledEntraceRef = React.useRef(Boolean)
    const liftRef = React.useRef(Boolean)
    const floorRef = React.useRef(null)
    const floorsNumberRef = React.useRef(null)
    const heatingRef = React.useRef(null)
    const buildingYearRef = React.useRef(null)
    const buildingStateRef = React.useRef(null)
    const sezCadRef = React.useRef(null)
    const fgCadRef = React.useRef(null)
    const pllaCadRef = React.useRef(null)
    const subCadRef = React.useRef(null)
    const rendCadRef = React.useRef(null)
    const catCadRef = React.useRef(null)
    const titleDescRef = React.useRef(null)
    const descriptionRef = React.useRef(null)
    const rifRef = React.useRef(null)
    const cityAddRef = React.useRef(null)
    const streetAddRef = React.useRef(null)
    const valueAPE = React.useRef(null) 
    const scaleAPE = React.useRef(null)
    const ownersRef = React.useRef(null) //_id table
    const stateRef = React.useRef(null)
    const priorityRef = React.useRef(null)
    const survayorRef = React.useRef(null)

    let disabledCondFees = false

    var [myOptions, setMyOptions] = React.useState([])
    
    const getDataFromAPI = async () => {
        myOptions = [];
        let r = await fetch(`/users/customers?jwt=${user.jwt}`, {                                
                            method: 'GET',
                            "Content-type": "application/json"
                        })
                                    
        let h = await r.json()
        if (h.length > 0) {
            h.map(function(v) { 
                myOptions.push(v)
            })
        }

        setMyOptions(myOptions)
    }

    function showCustomer(){
        return <div className="styled-select">
            <select data-component="acquirenti">
                {myOptions.map((e) => 
                    { return <option key={e._id} value={e.name + " " + e.surname}>{e.phone}</option>})
                }
            </select>
        </div>
    }

    React.useEffect( async () => {
        if(user != null){
            getDataFromAPI()

            let id = getHouseID()
            //console.log(`/users/${id}?jwt=${user.jwt}`)
            let res = await fetch(`/houses/${id}?jwt=${user.jwt}`)
            let h = await res.json()
            
            if (h.length != 0) {
                console.log('Find this house. ID: ' + id )
                categoryRef.current.value = h.category
                agreementRef.current.value = h.agreement
                propertyRef.current.value = h.property_type
                priceRef.current.value = h.price
                condominiumFeesRef.current.value = h.condominium_fees
                freeFromRef.current.value = h.free_from
                rentToBuyRef.current.value = h.rent_to_buy
                incomePropertyRef.current.value = h.income_property
                squareMeterRef.current.value = h.square_meter
                roomsRef.current.value = h.rooms
                bathroomsRef.current.value = h.bathrooms
                kitchenRef.current.value = h.kitchen
                gardenRef.current.value = h.garden
                garageRef.current.value = h.garage
                cellarRef.current.value = h.cellar
                balconyRef.current.value = h.balcony
                indipendentEntraceRef.current.value = h.indipendent_entrace
                airConditionerRef.current.value = h.air_conditioner
                disabledEntraceRef.current.value = h.disabled_entrace
                liftRef.current.value = h.lift
                floorRef.current.value = h.floor
                floorsNumberRef.current.value = h.floors_number
                heatingRef.current.value = h.heating
                buildingYearRef.current.value = h.building_year
                buildingStateRef.current.value = h.building_state
                
                sezCadRef.current.value = JSON.parse(h.cadastral_data).sezione
                fgCadRef.current.value = JSON.parse(h.cadastral_data).foglio
                pllaCadRef.current.value = JSON.parse(h.cadastral_data).particella
                subCadRef.current.value = JSON.parse(h.cadastral_data).subalterno
                catCadRef.current.value = JSON.parse(h.cadastral_data).categoria
                rendCadRef.current.value = JSON.parse(h.cadastral_data).rendita_catastale


                titleDescRef.current.value = h.description.slice(0, h.description.indexOf(';'))
                descriptionRef.current.value = h.description.slice(h.description.indexOf(';')+1)
                rifRef.current.value = h.ref
                cityAddRef.current.value = h.address.slice(0, h.address.indexOf(';')) 
                streetAddRef.current.value = h.address.slice(h.address.indexOf(';')+1) 
                
                valueAPE.current.value = JSON.parse(h.indexAPE).value
                scaleAPE.current.value = JSON.parse(h.indexAPE).scale
                
                ownersRef.current.value = h.owners

                stateRef.current.value = h.state
                priorityRef.current.value = h.priority

                survayorRef.current.value = h.survayor
                //fD = new FormData()
            }
        }
    }
    , [user])

    function getHouseID(){
        var n = location.pathname.lastIndexOf('/')
        return location.pathname.slice(n+1)
    }

    function handleHidePrice () { priceRef.current.value = -priceRef.current.value }

    function handleNoCondominiumFees () { 
        if (disabledCondFees == false) {
            condominiumFeesRef.current.value = 0
            disabledCondFees = true
        }
    }

    function setInWaitingAPE(){
        valueAPE.current.value = 0
        scaleAPE.current.value = 'W'
    }

    function setNotCertificableAPE(){
        valueAPE.current.value = -1
        scaleAPE.current.value = 'NC'
    }
    
    let location = useLocation()

    return (
        user != null ? <>
            <AgentHome/>
            <React.Fragment>
            <div className='container text-center'>    
            <form role="form" className="gtx-form" method="post" id="gtx-form-ins-annuncio">
                <div data-error-target="annuncio" className="step2-form-error  gtx-standard-margin" hidden={true}>
                <div className="alert alert-danger"></div>
            </div>

            <div className="panel" data-component="tipologies">
                <div className="panel-heading"><h4>Tipologia</h4></div>
                <div className="panel-body">                  
                    <div className="row">
                        <div className="col-4">Categoria
                        <div className="styled-select" >
                                <select data-component="gruppo" ref={categoryRef}>
                                    {getRenderingCategories()}
                                </select>
                            </div>
                        </div>
                        <div className="col-4"> Contratto
                            <div className="styled-select" >
                                <select data-component="contratto" ref={agreementRef} data-parsley-required="">
                                    {getRenderingAgreementTypes()}
                                </select>
                            </div>
                        </div>
                        <div className="col-4">Tipo proprietà
                            <div className="styled-select">
                                <select data-component="tipoProprieta" ref={propertyRef}>
                                   { getRenderingContractTypes() }
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="panel" data-component="contract-price" data-props-category="1">
                <div className="panel-heading"><h4>Prezzo e costi</h4></div>
                <div className="panel-body">
                    <div className="row">
                        <div className="col-6">Richiesta
                            <div className="input-group mb-3">
                                <span className="input-group-text">€</span>
                                <input type="number" ref={priceRef} className="form-control" id="prezzo"/>
                            </div>
                            <label htmlFor="flag_nascondiPrezzo">
                                <input type="checkbox" id="flag_nascondiPrezzo" defaultValue="false" onChange={handleHidePrice}/>
                                <span className="text-nowrap">
                                    <span className="hidden-full-pezzo-label"> Mostra come </span>
                                    <span>"Prezzo su richiesta"</span>
                                </span>
                            </label>
                        </div>

                        <div className="col-6">Spese condominiali
                            <div className="input-group mb-3">
                                <span className="input-group-text" id="basic-addon3">€/anno</span>
                                <input type="text" ref={condominiumFeesRef} className="form-control" id="speseCond" aria-describedby="basic-addon3" defaultValue="0" disabled={disabledCondFees}/>
                            </div>
                            <label htmlFor="speseCondominialiNulle">
                                <input type="checkbox" id="speseCondominialiNulle" data-component="speseCondominialiNulle" onChange={handleNoCondominiumFees}/>
                                <span className="text-nowrap">
                                    <span className="hidden-full-spese-label">Nessuna spesa condominiale</span>
                                </span>
                            </label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-4 ">Libero                
                            <div data-component="libero">
                                <input name="libero" ref={freeFromRef} id="libero" type="text" defaultValue="Alla stipula"/>
                            </div>
                        </div>
                        {console.log(rentToBuyRef.current.value)}
                        <div className="col-4">Affitto con riscatto [{rentToBuyRef.current.value == null ? '' : rentToBuyRef.current.value == true ? 'Sì' : 'No'}]
                            <div ref={rentToBuyRef}>
                                <input type="radio" className="btn-check" name="options-outlined rentToBuy" id="rentToBuy-true" autoComplete="off" onClick={()=>{rentToBuyRef.current.value=true}}/>
                                    <label className="btn btn-outline-primary" htmlFor="rentToBuy-true">Sì</label>
                                <input type="radio" className="btn-check" name="options-outlined rentToBuy" id="rentToBuy-false" autoComplete="off" onClick={()=>{rentToBuyRef.current.value=false}}/>
                                    <label className="btn btn-outline-primary" htmlFor="rentToBuy-false">No</label>   
                            </div>
                        </div>
                        <div className="col-4">Immobile a reddito: [{incomePropertyRef.current.value == true ? 'Sì' : 'No'}]
                           <div ref={incomePropertyRef}>
                                <input type="radio" className="btn-check" name="options-outlined incomeProp" id="incomeProp-true" autoComplete="off" onClick={()=>incomePropertyRef.current.value=true}/>
                                    <label className="btn btn-outline-primary" htmlFor="incomeProp-true">Sì</label>
                                <input type="radio" className="btn-check" name="options-outlined incomeProp" id="incomeProp-false" autoComplete="off" onClick={()=>incomePropertyRef.current.value=false}/>
                                    <label className="btn btn-outline-primary" htmlFor="incomeProp-false">No</label>   
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="panel" data-component="surface-details">
                <div className="panel-heading" id="superficie">
                    <h4>Superficie</h4>
                </div>
                <div className="panel-body">
                    <div className='row'>
                        <div className='col-12'>
                            <input type="number" ref={squareMeterRef} step="0.1" defaultValue=""/>
                            <span className="input-group-addon">   m<sup>2</sup></span>
                        </div>
                    </div>
                </div>
            </div>

            <div className="panel" data-component="composition-residential">
                <div className="panel-heading" id="composizione">
                    <h4>Composizione</h4>
                </div>
                
                <div className="panel-body">
                    <div data-error-target="composizione" className="step2-form-error alert alert-danger" hidden={true}></div>
                        <div className="col-12">
                            <div className="row mb-3">
                                <div className='col-2'></div>
                                <div className='col-4'>Numero stanze
                                    <div className="input-group">
                                        <input type="number" ref={roomsRef} className="form-control" defaultValue="1"/>
                                    </div>
                                </div>
                                <div className="col-4">Bagni
                                    <div className="input-group">
                                        <input type="number" ref={bathroomsRef} className="form-control" defaultValue="1"/>
                                    </div>
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-4 ">Cucina
                                    <div className="styled-select">
                                        <select data-component="cucina" ref={kitchenRef}>
                                            <option value="">Scegli</option>
                                            <option value="Abitabile">Abitabile</option>
                                            <option value="Angolo cottura">Angolo cottura</option>
                                            <option value="Cucinotto">Cucinotto</option>
                                        </select>
                                    </div>
                                </div>                  
                                <div className="col-4 ">Giardino
                                    <div className="styled-select">
                                        <select data-component="idGiardino" ref={gardenRef}>
                                            <option value="false">Scegli</option>
                                            <option value="true">Privato</option>
                                            <option value="false">Comune</option>
                                            <option value="false">Nessuno</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-4 "> Garage
                                    <div className="styled-select">
                                        <select data-component="boxauto" ref={garageRef}>
                                            <option value="false">Scegli</option>
                                            <option value="true">Singolo</option>
                                            <option value="true">Doppio</option>
                                            <option value="false">Nessuno</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="row mb-1">
                                <div className="col-4 "> Balcone / terrazzo [{balconyRef.current.value== true ? 'Sì' : 'No'}]
                                    <div ref={balconyRef}>
                                        <input type="radio" className="btn-check" name="options-outlined balcony" id="balcony-true" autoComplete="off" onClick={()=>balconyRef.current.value=true}/>
                                            <label className="btn btn-outline-primary" htmlFor="balcony-true">Sì</label>
                                        <input type="radio" className="btn-check" name="options-outlined balcony" id="balcony-false" autoComplete="off" onClick={()=>balconyRef.current.value=false}/>
                                            <label className="btn btn-outline-primary" htmlFor="balcony-false">No</label> 
                                    </div>
                                </div>
                                <div className="col-4"> Cantina [{cellarRef.current.value== true ? 'Sì' : 'No'}]
                                    <div ref={cellarRef}>
                                        <input type="radio" className="btn-check" name="options-outlined cellar" id="cellar-true" autoComplete="off" onClick={()=>cellarRef.current.value=true}/>
                                            <label className="btn btn-outline-primary" htmlFor="cellar-true">Sì</label>
                                        <input type="radio" className="btn-check" name="options-outlined cellar" id="cellar-false" autoComplete="off" onClick={()=>cellarRef.current.value=false}/>
                                            <label className="btn btn-outline-primary" htmlFor="cellar-false">No</label>   
                                    </div>
                                </div>
                                <div className="col-4 ">Ingresso indipendente [{indipendentEntraceRef.current.value== true ? 'Sì' : 'No'}]
                                    <div ref={indipendentEntraceRef}>
                                        <input type="radio" className="btn-check" name="options-outlined ingrIndip" id="ingrIndip-true" autoComplete="off" onClick={()=>indipendentEntraceRef.current.value=true}/>
                                            <label className="btn btn-outline-primary" htmlFor="ingrIndip-true">Sì</label>
                                        <input type="radio" className="btn-check" name="options-outlined ingrIndip" id="ingrIndip-false" autoComplete="off" onClick={()=>indipendentEntraceRef.current.value=false}/>
                                            <label className="btn btn-outline-primary" htmlFor="ingrIndip-false">No</label>   
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

            <div className="panel" data-component="feautures-residential" data-props-ad-id="">
                <div className="panel-heading">
                    <h4>Caratteristiche</h4>
                </div>
            <div className="panel-body">
                <div className="col-12">
                    <div className="row mb-2">
                        <div className='col-2'></div>
                        <div className="col-4 ">Anno costruzione
                            <input type="text" ref={buildingYearRef} data-component="gtxAnnoCostruzione" className="form-control" data-parsley-type="integer" data-parsley-min="1000" data-parsley-max="2100" data-parsley-min-message="Questo valore deve essere maggiore di 999." defaultValue=""/>
                        </div>  
                        <div className="col-4">Stato
                            <div className="styled-select">
                                <select data-component="stato" ref={buildingStateRef}>
                                    {getRenderingBuildingStates()}
                                </select>
                            </div>
                        </div>                     
                    </div>
                    <div className="row mb-2">
                        <div className="col-4 ">Piano
                            <div className="styled-select">
                                <select data-component="piano" ref={floorRef}>
                                    {getRenderingFloor()}
                                </select>
                            </div>
                        </div>
                        <div className="col-4 ">Su _ piani
                            <input type="number" ref={floorsNumberRef} className="form-control" defaultValue="1"/>
                        </div> 
                        <div className="col-4">Riscaldamento
                            <div className="styled-select">
                                <select data-component="riscaldamento" ref={heatingRef}>
                                    <option value="">Scegli</option>
                                    <option value="Autonomo a pavimento">Autonomo a pavimento</option>
                                    <option value="Autonomo a radiatori">Autonomo a radiatori</option>
                                    <option value="Centralizzato a radiatori">Centralizzato a radiatori</option>
                                    <option value="Centralizzato ad aria">Centralizzato ad aria</option>
                                    <option value="Assente">Assente</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-4">Ascensore [{liftRef.current.value== true ? 'Sì' : 'No'}]
                            <div ref={liftRef}>
                                <input type="radio" className="btn-check" name="options-outlined lift" id="lift-true" autoComplete="off" onClick={()=>liftRef.current.value=true}/>
                                    <label className="btn btn-outline-primary" htmlFor="lift-true">Sì</label>
                                <input type="radio" className="btn-check" name="options-outlined lift" id="lift-false" autoComplete="off" onClick={()=>liftRef.current.value=false}/>
                                    <label className="btn btn-outline-primary" htmlFor="lift-false">No</label>   
                            </div>
                        </div>
                    
                        <div className="col-4">Accesso per disabili [{disabledEntraceRef.current.value== true ? 'Sì' : 'No'}]
                            <div ref={disabledEntraceRef}>
                                <input type="radio" className="btn-check" name="options-outlined disabEnt" id="disabEnt-true" autoComplete="off" onClick={()=>disabledEntraceRef.current.value=true}/>
                                    <label className="btn btn-outline-primary" htmlFor="disabEnt-true">Sì</label>
                                <input type="radio" className="btn-check" name="options-outlined disabEnt" id="disabEnt-false" autoComplete="off" onClick={()=>disabledEntraceRef.current.value=false}/>
                                    <label className="btn btn-outline-primary" htmlFor="disabEnt-false">No</label>   
                            </div>
                        </div>
                    
                        <div className="col-4">Impianto climatizzazione [{airConditionerRef.current.value== true ? 'Sì' : 'No'}]
                            <div ref={airConditionerRef}>
                                <input type="radio" className="btn-check" name="options-outlined ac" id="ac-true" autoComplete="off" onClick={()=>airConditionerRef.current.value=true}/>
                                    <label className="btn btn-outline-primary" htmlFor="ac-true">Sì</label>
                                <input type="radio" className="btn-check" name="options-outlined ac" id="ac-false" autoComplete="off" onClick={()=>airConditionerRef.current.value=false}/>
                                    <label className="btn btn-outline-primary" htmlFor="ac-false">No</label>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <div className="panel" data-component="energetic-rating" data-props-id-categoria="1">
                <div className="panel-heading" id="certificazione-energetica">
                    <h4>Certificazione Energetica</h4>
                </div>

                <div className="panel-body">
                <div className="col-12">
                    <div data-component="ipe-certification">

                        <div className="row">
                            <div data-error-target="classeEnergetica" className="step2-form-error alert alert-danger" hidden={true}></div>
                            <div className="col-4">Classe Energetica
                                <div className="styled-select ">
                                    <select ref={scaleAPE} data-component="classe_energetica" data-parsley-required="true">
                                        <option defaultValue="">Scegli</option>
                                        { getRenderingEnergeticClass() }
                                    </select>
                                </div>
                            </div>
                        
                            <div className="col-6">
                                Indice di prestaz. energetica
                                <input ref={valueAPE} type="text" className="form-control" defaultValue=""/>
                                kWh/m² anno
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-12"><hr/></div>
                    
                    <div data-component="ipe-free">
                        <div className="row mb-2">
                            <div className="col-12">
                                <div className="styled-checkbox">
                                    <label htmlFor="checkCertificato" className="text-nowrap">
                                        <input type="checkbox" onChange={setInWaitingAPE}/>
                                        <span>Indice di prestazione energetica in attesa di certificazione</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="styled-checkbox">
                                    <label htmlFor="checkEsente" className="text-nowrap">
                                        <input type="checkbox" onChange={setNotCertificableAPE}/>
                                        <span>Immobile esente da certificazione energetica</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <div className="panel" data-component="cadastral-infos">
                <div className="panel-heading" id="datiCatastali"><h4>Dati catastali</h4></div>
                <div className="panel-body">
                    <div className="col-12">
                        <div className="row">
                            <div className="col-6 ">
                                Sezione
                                <input type="text" ref={sezCadRef} data-component="sezione" className="form-control" defaultValue=""/>
                            </div>
                            <div className="col-6 ">
                                Foglio
                                <input type="text" ref={fgCadRef} data-component="foglio" className="form-control" defaultValue=""/>
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="col-6 ">
                                Particella
                                <input type="text" ref={pllaCadRef} data-component="particella" className="form-control" defaultValue=""/>
                            </div>
                            <div className="col-6 ">
                                Subalterno
                                <input type="text" ref={subCadRef} data-component="subalterno" className="form-control" defaultValue=""/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-6">
                                Rendita catastale
                                <span className="tip">
                                    <span className="badge label-primary pointer" data-toggle="tooltip" data-placement="top" title="" data-original-title="Valore riportato in scheda catastale. Inserire il valore in euro. Utilizzare la virgola come separatore per le cifre decimali.">?
                                    </span>
                                </span>
                            
                                <div className="input-group no-tip-sm">
                                    <div className="input-group-addon">€</div>
                                    <input type="text" ref={rendCadRef} data-component="rendita_catastale" className="form-control" data-props-precision="2" data-parsley-number-formatted="true" defaultValue=""/>
                                </div>
                            </div>

                            <div className="col-12 col-sm-6">
                                Categoria Catastale
                                <div className="styled-select">
                                    <select data-component="classe_catastale" ref={catCadRef}>
                                        {getCadrastalCategory()}
                                    </select>
                                </div>
                            </div>
                    
                        </div>
                    </div>
                </div>
            </div>
            
            <div className="panel" data-component="brokerage">
                <div className="panel-heading"><h4>Dati intermediazione</h4></div>
                <div className="panel-body">
                    <div className="col-12">
                        <div className="row mb-2">
                            <div className="col-4 ">
                                Riferimento annuncio
                                <input type="text" ref={rifRef} className="form-control" defaultValue=""/>
                            </div>
                            <div className="col-4 ">
                                Priorità (1MAX)
                                <div className='input-group'>
                                    <span className="input-group-text">1</span>
                                    <input type="range" ref={priorityRef} min="1" max="10" data-component="codice" className="form-control" defaultValue="10"/>
                                    <span className="input-group-text">10</span>
                                </div>
                            </div>
                            <div className="col-4">
                                Stato
                                <div className="styled-select">
                                <select data-component="stato" ref={stateRef}>
                                    <option defaultValue="Attivo">Attivo</option>
                                    <option defaultValue="Sospeso">Sospeso</option>
                                </select>
                            </div>
                            </div>
                        </div>
                    
                    <div className="row">
                    <div className='col-3'></div>
                        <div className="col-6">
                            <label htmlFor="prop">Proprietario</label>
                            <input list="clients" id="prop" name="customs" ref={ownersRef}/>
                            <datalist id="clients">
                                {showCustomer()}
                            </datalist>
                        </div>
                    </div>
            
                    <div className="row mb-2">
                        <div className="col-6">Tipo Mandato
                            <div className="styled-select">
                                <select data-component="tipoMandato">
                                    {getRenderingMandatory()}
                                </select>
                            </div>
                        </div>
                        <div className="col-6 ">
                            Scadenza Mandato
                            <div className="input-group date-picker">
                                <input type="date" className="form-control" data-component="gtxScadenzaMandato" defaultValue="" data-parsley-date="true"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
            <div data-component="description" data-props-max-length="3000" data-props-max-title-length="60">
                    <div className="panel">
                        <div className="panel-heading" id="descrizione">
                            <h4>Descrizione per i portali web</h4>
                        </div>
                        <div className="panel-body">
                            <div className="col-12">
                            <div className="tab-content" data-role="desc-text-container">
                                <div id="tab-panel-1" data-desc-lang="it" className="tab-pane active">
                                    <input type="text" ref={titleDescRef} className="form-control mb-3" placeholder="Inserisci il titolo (max 60 caratteri)" data-component="titolo-descrizione-it" maxLength="60" defaultValue=""/>
                                    <textarea ref={descriptionRef} data-component="descrizione" data-parsley-maxwords="3000" className="form-control" cols="133" rows="8" placeholder="Inserisci la descrizione"></textarea>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
            </div>

            <div className="panel" data-component="location">
                <div className="panel-heading"><h4>Posizione</h4></div>
                <div className="panel-body">
                <div className="col-12">
                        <div className='row'>
                            <div className="col-6 ">
                                Citta'
                                <input type="text" ref={cityAddRef} data-component="city" className="form-control" />
                                Indirizzo
                                <input type="text" ref={streetAddRef} data-component="street" className="form-control" defaultValue=""/>
                            </div>
                            <div className='col-6' id='map'>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2854.587087088543!2d11.796796915519481!3d44.3184384791041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132b48361b680f27%3A0x12cd8d8cffa0cb4d!2sL&#39;Agenzia%20LA%20PIAZZA!5e0!3m2!1sit!2sit!4v1645971567734!5m2!1sit!2sit"></iframe>
                            </div>         
                        </div>        
                    </div>
                </div>
            </div>

            <div className="panel" data-component="photo">
                <div className="panel-heading"><h4>Foto</h4></div>
                <div className="panel-body">
                    
                    <div className="col-12"> 
                        <input className="form-control" type="file" id="photos"
                            onChange={(e) => {
                                for (let i = 0; i < e.target.files.length; i++) {
                                     let f = e.target.files[i]
                                     console.log('append file')
                                     fD.append('photos', f)
                                 }
                            }} multiple={true}></input>
                    </div>
                </div>
            </div>

            <div className="panel" data-component="document">
                <div className="panel-heading"><h4>Documentazione</h4></div>
                <div className="panel-body">
                    <div className="col-12">
                        <div className='row mb-3'>
                            <div className="col-4">
                                <label htmlFor="surv" className="form-label">Geometra incaricato</label>
                                <input type="text" ref={survayorRef} className="form-control" defaultValue=""/>
                            </div>
                            <div className="col-4 ">
                                <label htmlFor="APE" className="form-label">Certificazione energetica</label>
                                <input className="form-control" type="file" id="APE"
                                    onChange={(e) => {
                                        for (let i = 0; i < e.target.files.length; i++) {
                                        let f = e.target.files[i]
                                        console.log('append file')
                                        fD.append('APE', f)
                                        }
                                }} multiple={true}></input>
                            </div>
                            <div className="col-4">
                                <label htmlFor="RTI" className="form-label">Relazione Tecnica Integrata</label>
                                <input className="form-control" type="file" id="RTI"
                                    onChange={(e) => {
                                        for (let i = 0; i < e.target.files.length; i++) {
                                        let f = e.target.files[i]
                                        console.log('append file')
                                        fD.append('RTI', f)
                                        }
                                }} multiple={true}></input>                        
                            </div>
                        </div>

                        <div className='row'>
                            <div className="col-4 mb-3 ">
                                <label htmlFor="AAA" className="form-label">Accesso agli atti</label>
                                <input className="form-control" type="file" id="AAA"
                                    onChange={(e) => {
                                        for (let i = 0; i < e.target.files.length; i++) {
                                        let f = e.target.files[i]
                                        console.log('append file')
                                        fD.append('AAA', f)
                                        }
                                }} multiple={true}></input>
                            </div>

                            <div className="col-4 mb-3 ">
                                <label htmlFor="cadastral_plan" className="form-label">Planimetrie Catastali</label>
                                <input className="form-control" type="file" id="cadastral_plan"
                                    onChange={(e) => {
                                        for (let i = 0; i < e.target.files.length; i++) {
                                        let f = e.target.files[i]
                                        console.log('append file ' + f)
                                        fD.append('cadastral_plan', f)
                                        }
                                }} multiple={true}></input>
                            </div>

                            <div className="col-4 mb-3">
                                <label htmlFor="certificate" className="form-label">Visure catastali</label>
                                <input className="form-control" type="file" id="certificate"
                                    onChange={(e) => {
                                        for (let i = 0; i < e.target.files.length; i++) {
                                        let f = e.target.files[i]
                                        console.log('append file')
                                        fD.append('certificate', f)
                                        }
                                }} multiple={true}></input>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

            <div className='d-flex justify-content-end m-2'>
                    <button className='btn rounded-circle customerButton' onClick={ async (e) => {
                        e.preventDefault()
                        fD.append('category', categoryRef.current.value)
                        fD.append('agreement', agreementRef.current.value)
                        fD.append('property_type', propertyRef.current.value)
                        fD.append('price', priceRef.current.value)
                        fD.append('condominium_fees', condominiumFeesRef.current.value)
                        fD.append('free_from', freeFromRef.current.value)
                        fD.append('rent_to_buy', rentToBuyRef.current.value)
                        fD.append('income_property', incomePropertyRef.current.value)
                        fD.append('square_meter', squareMeterRef.current.value)
                        fD.append('rooms', roomsRef.current.value)
                        fD.append('bathrooms', bathroomsRef.current.value)
                        fD.append('kitchen', kitchenRef.current.value)
                        fD.append('garden', gardenRef.current.value)
                        fD.append('garage', garageRef.current.value)
                        fD.append('cellar', cellarRef.current.value)
                        fD.append('balcony', balconyRef.current.value)
                        fD.append('indipendent_entrace', indipendentEntraceRef.current.value)
                        fD.append('air_conditioner', airConditionerRef.current.value)
                        fD.append('disabled_entrace', disabledEntraceRef.current.value)
                        fD.append('lift', liftRef.current.value)
                        fD.append('floor', floorRef.current.value)
                        fD.append('floors_number', floorsNumberRef.current.value)
                        fD.append('heating', heatingRef.current.value)
                        fD.append('building_year', buildingYearRef.current.value)
                        fD.append('building_state', buildingStateRef.current.value)
                        fD.append('cadastral_data', JSON.stringify({
                            sezione: sezCadRef.current.value,
                            foglio: fgCadRef.current.value,
                            particella: pllaCadRef.current.value,
                            subalterno: subCadRef.current.value,
                            rendita_catastale: rendCadRef.current.value,
                            categoria: catCadRef.current.value
                        }))
                        fD.append('description', titleDescRef.current.value + ";" + descriptionRef.current.value)
                        fD.append('ref', rifRef.current.value)
                        fD.append('address', cityAddRef.current.value + ";" + streetAddRef.current.value)
                        fD.append('indexAPE', JSON.stringify({
                            scale: scaleAPE.current.value,
                            value: valueAPE.current.value
                        })) 
                        fD.append('state', stateRef.current.value)
                        fD.append('priority', priorityRef.current.value)
                        fD.append('survayor', survayorRef.current.value)
                        fD.append('mock', false)
                        fD.append('_id', getHouseID())
                        fD.append('owners', ownersRef.current.value)
                    //photos: [{filename: 'String', download_link: 'String'}],

                        for (var v of fD.values()){
                            console.log(v)
                        }
                        try{
                            const res = await fetch(`/houses/${getHouseID()}?jwt=${user.jwt}`, {
                                method: 'PUT',
                                body: fD,
                                "Content-type": "multipart/form-data"
                            })
                            if(res.ok) {
                                console.log('fetch done in modify house')
                                navigate('/agentHome/house/'+getHouseID())
                            } else {
                                //TODO notifico errore
                            }
                        } catch(err) {
                            console.log(err)
                        }
                    }}>
                    <i className="fa fa-floppy-o" aria-hidden="false" title='Salva immobile'></i>
                    </button>
            </div>
        </form>
            
        </div>
            </React.Fragment>
        </> : <Loading/>
    )
}
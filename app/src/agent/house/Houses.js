import React from 'react';
import { AgentHome } from '../AgentHome';
import "./Houses.css";
import { Loading } from '../Loading';
import { navigate, useNavigate } from 'react-router-dom';

export function Houses({hs, l, user}) {
    const navigate = useNavigate()
    function showH(){
        return hs.map((val, key) => {
            return (
            <tr key={key}>
            <td><div>{val.ref}</div></td>
            <td><div>
                { 
                    <div key={val.photos[0].id} >
                        <img style={{ width: '100%', height: 'auto', maxHeight: '120px'}} src={val.photos[0].download_link} alt={val.photos[0].filename} ></img> 
                    </div>
                }
            </div></td>
            <td><div>{val.address}</div></td>
            <td><div>{val.price}</div></td>
            <td>
                <div className='subnav'>
                    <a className="header_toggle">
                        <i id="header-toggle" className="fa fa-bars text-black"></i>
                    </a>
                    <div className='subnav-content'>
                        <a onClick={() => navigate('/agentHome/house-edit/'+val.id)}>Modifica</a>                                
                        <br></br>
                        <a onClick={() => navigate('/agentHome/house/'+val.id)}>Visualizza</a>
                        <br></br>
                        <a onClick={async () => {
                            let res = await fetch(`/houses/${val.id}?jwt=${user.jwt}`, {
                                method: 'DELETE',
                                "Content-type": "application/json"
                                })
                                navigate('/agentHome/showHouses/')
                            }}>Elimina</a>
                    </div>
                </div>
            </td>     
            </tr>)
        })
    }

    function getHouseTable(){
        return l ? 
        <Loading/>: hs.lenght == 0 ? <div className='text-center'>Non sono presenti immobili</div> : <> 
        <table id='tab'>
                <thead>
                    <tr>
                    <th>Rif</th>
                    <th>Foto</th>
                    <th>Luogo</th>
                    <th>Prezzo</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    {showH()}
                </tbody>
            </table>
        </>
    }

    return(
        <>
            <AgentHome/>
            <div className='container'>
                {getHouseTable()}
            </div>
        </>
    );
}



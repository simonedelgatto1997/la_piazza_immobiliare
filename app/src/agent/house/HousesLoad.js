import React from 'react'
import { Houses } from './Houses'

export function HousesLoad({ user }) {
    const [houses, setHouses] = React.useState([])
    const [loading, setLoading] = React.useState(true)

    React.useEffect(async () => {
        let res = await fetch('/houses?filters={}')
        let h = await res.json()
        let hou = []
        console.log('loading house')
        if (h.length > 0) {
            h.map(function(v) { 
                hou.push({
                    ref: v.ref,
                    photos: v.photos,
                    address: v.address,
                    price: v.price,
                    id: v._id
                })
            })
            setHouses(hou)
        }
        setLoading(false)
        console.log('. . . loaded')
    }, [])


    return (
        <React.Fragment>
            <Houses hs={houses} l={loading} user={user}></Houses>
        </React.Fragment>
    )
}
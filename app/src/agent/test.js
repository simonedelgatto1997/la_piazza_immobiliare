import React from "react";
import { Authify, useUser } from "../auth";

export function AgentComponent({ user }) {
    return (
        <div> AGENT {user.username} AUTENTICATO</div>
    )
}

export function Agent() {
    const [logoutButton, setLogoutButton] = React.useState(null)

    React.useEffect(() => {
        setLogoutButton(document.getElementById('logout-button'))
    }, [])
    return (
        <div>
            <div id='logout-button'></div>
            {logoutButton && <Authify
                logoutButtonEl={logoutButton}
                AUTH_API={'/auth/'} ROLES={['agent']} AUTH_DECODE_API={'/auth/decode/'}>
                <AgentComponent/>
            </Authify>}
        </div>
    )
}
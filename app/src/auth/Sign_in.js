import React from "react";
import { AuthifyNoLogout, useUser } from "./index";
import { Link, Navigate } from 'react-router-dom'
import Footer from "../main_elements/Footer";
import Header from "../main_elements/Header";

function SignIn() {
  return (
    <React.Fragment>
      <Header />
      <div className="container-fluid mt-3">
        <div className="row text-center">
          <div className="col-3"></div>
          <div className="col-6">
            <AuthifyNoLogout
              AUTH_API={'/auth/'} ROLES={['agent', 'customer']} AUTH_DECODE_API={'/auth/decode/'}>
              <React.Fragment > 
                <Navigate to='/'></Navigate>
              </React.Fragment >
            </AuthifyNoLogout >
          </div>
          <p>Non hai un account? <Link className="a" to="/registrati">Registrati</Link></p>
          <div className="col-3"></div>
        </div>

      </div>
      <Footer></Footer>
    </React.Fragment>
  )
}

export { SignIn }
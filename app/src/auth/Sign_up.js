import * as React from 'react'
import Footer from '../main_elements/Footer'
import Header from '../main_elements/Header'
import { useNavigate } from 'react-router-dom'
import { AgentHome } from '../agent/AgentHome'

export function SignUp( {user}) {
    const navigate = useNavigate()
    const emaiRef = React.useRef(null)
    const phoneRef = React.useRef(null)
    const passRef = React.useRef(null)
    const nameRef = React.useRef(null)
    const surnameRef = React.useRef(null)
    const usernameRef = React.useRef(null)

    React.useEffect(async () => {

    }, [])

    function show() {
        if (user == null) {
            return <Header></Header>
        } else if (user.role == 'agent') {
            return <AgentHome></AgentHome>
        }
    }


    return (
        <>
            {show()}
            <form>
                <div className="container col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div className="form-group">
                        <label >Email address</label>
                        <input type="email" ref={emaiRef} className="form-control" aria-describedby="emailHelp" placeholder="Enter email">
                        </input>
                        <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div>
                        <label>Cellulare</label>
                        <input type="phone" ref={phoneRef} className="form-control" aria-describedby="phoneHelp" placeholder="Inserisci cellulare"/>
                    </div>
                    <div className="form-group">
                        <label>Username</label>
                        <input type="text" ref={usernameRef} className="form-control" aria-describedby="emailHelp" placeholder="Username">
                        </input>
                    </div>
                    <div className="form-group">
                        <label >Password</label>
                        <input type="password" ref={passRef} className="form-control" placeholder="Password">
                        </input>
                    </div>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" ref={nameRef} className="form-control" placeholder="Name">
                        </input>
                        <label>Surname</label>
                        <input type="text" ref={surnameRef} className="form-control" placeholder="Surname">
                        </input>
                    </div>
                    <div className='text-center'>
                    <button onClick={async (e) => {
                        console.log('BOTTONE PREMUTO')
                        e.preventDefault()

                        fetch('/users', {
                            body: JSON.stringify({
                                email: emaiRef.current.value,
                                password: passRef.current.value,
                                name: nameRef.current.value,
                                surname: surnameRef.current.value,
                                username: usernameRef.current.value,
                                phone: phoneRef.current.value,
                                role: 'customer'
                            }),
                            method: 'POST',
                            headers: { 'Content-Type': 'application/json' }
                        }).then(res => {
                            console.log('fatto fetch')
                            if(user == null){
                                navigate('/')
                            } else if (user.role == 'agent'){
                                navigate('/agentHome/showClients')
                            }
                        }).catch(err => {
                            console.log(err)
                        })

                    }}> Registrati </button>
                    </div>
                </div>
            </form>
            <Footer></Footer></>
    );
}


import React from 'react';
import {useUser} from './hook.js';

export const AuthForm = function (props) {
    const {Methods} = useUser(props.ROLES, props.AUTH_API, props.AUTH_DECODE_API);
    const [login, setLogin] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [loading, setLoading] = React.useState(false);
    const [error, setError] = React.useState(false);

    const onChange = function (key) {
        return function (e) {
            e.persist();
            const set = {login: setLogin, password: setPassword}[key];
            set(e.target.value);
        };
    };

    const onClick = async function (e) {
        e.preventDefault();
        setLoading(true);
        try {
            await Methods.login(login, password);
        } catch (e) {
            setError(true);
            setLoading(false);
        }
    };

    const roles = props.ROLES.map(function (role, i) {
        if (i === props.ROLES.length - 1)
            return (
                <span key={role}>
                    <em>{role}</em>
                </span>
            );
        return (
            <span key={role}>
                <em>{role}</em> o{' '}
            </span>
        );
    });

    return (
        <form className='m-3' onSubmit={onClick}>
            <fieldset disabled={loading}>
                <div className="form-floating mb-3">
                    <input
                        autoFocus
                        className="form-control form-control-lg"
                        placeholder="Username"
                        value={login}
                        onChange={onChange('login')}
                    />
                    <label>Username</label>
                    <div className="form-text">Devi autenticarti come {roles}</div>
                </div>
                <div className="form-floating mb-3">
                    <input
                        type="password"
                        className="form-control form-control-lg"
                        placeholder="Password"
                        value={password}
                        onChange={onChange('password')}
                    />
                    <label>Password</label>
                </div>
                {error && (
                    <div className="alert alert-danger" role="alert">
                        Errore di autenticazione
                    </div>
                )}
                <button type="submit" className="btn btn-lg btn-primary">
                    <i className="bi bi-box-arrow-in-right"></i> Login
                </button>
            </fieldset>
        </form>
    );
};

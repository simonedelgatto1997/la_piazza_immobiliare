import useSWR from 'swr';

import Store from './store';
import Model from './model';

export const useUser = function (ROLES, AUTH_API, AUTH_DECODE_API) {
    const { data, mutate } = useSWR(
        'user',
        async function () {
            const t = Store.getValue();
            if (t !== null) {
                try {
                    const res = await Model(ROLES, AUTH_API, AUTH_DECODE_API).decode(t['jwt']);
                    if (!ROLES.includes(res.role)) throw new Error(`The role ${res.role} is not authorized!`);
                    return t;
                } catch (e) {
                    return null;
                }
            } else return null;
        },
        { dedupingInterval: 60000 }
    );

    return {
        user: data,
        Methods: {
            login: async function (login, password) {
                const user = await Model(ROLES, AUTH_API, AUTH_DECODE_API).login(login,password)
                Store.setValue(user);
                mutate(user);
            },
            logout: function () {
                Store.removeValue();
                mutate();
                // window.location = window.location.href.split('?')[0];
            },
        },
    };
};

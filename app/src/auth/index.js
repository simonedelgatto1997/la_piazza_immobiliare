import React from 'react';
import ReactDOM from 'react-dom';

import { AuthForm } from './auth_form';
import { useUser } from './hook';
import AuthStore from './store';

const LogoutButton = function ({ el, ROLES, AUTH_API, AUTH_DECODE_API }) {
    const { user, Methods } = useUser(ROLES, AUTH_API, AUTH_DECODE_API);

    let res = null;
    if (user !== null) {
        const onClick = function (e) {
            e.preventDefault();
            Methods.logout();
        };

        res = (
            <React.Fragment>
                <span className="navbar-text" style={{ marginRight: '10px' }}>
                    {user.name} <sub>{user.role}</sub>
                </span>
                <button className="btn btn-outline-danger my-2 my-sm-0" onClick={onClick}>
                    <i className="bi bi-box-arrow-in-left"></i> Logout
                </button>
            </React.Fragment>
        );
    }

    return ReactDOM.createPortal(res, el);
};

const Authify = function ({ logoutButtonEl, children, ROLES, AUTH_API, AUTH_DECODE_API }) {
    const { user } = useUser(ROLES, AUTH_API, AUTH_DECODE_API);

    if (user === undefined)
        return (
            <div className="spinner-grow" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        );

    const mainComponent =
        user === null ? (
            <AuthForm ROLES={ROLES} AUTH_API={AUTH_API} AUTH_DECODE_API={AUTH_DECODE_API} />
        ) : (
            React.Children.map(children, function (child) {
                return React.cloneElement(child, { user: user });
            })
        );
    return (
        <React.Fragment>
            <LogoutButton el={logoutButtonEl} />
            {mainComponent}
        </React.Fragment>
    );
};

const AuthifyNoLogout = function ({ children, ROLES, AUTH_API, AUTH_DECODE_API }) {
    const { user } = useUser(ROLES, AUTH_API, AUTH_DECODE_API);

    if (user === undefined)
        return (
            <div className="spinner-grow" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        );

    const mainComponent =
        user === null ? (
            <AuthForm ROLES={ROLES} AUTH_API={AUTH_API} AUTH_DECODE_API={AUTH_DECODE_API} />
        ) : (
            React.Children.map(children, function (child) {
                return React.cloneElement(child, { user: user });
            })
        );
    return (
        <React.Fragment>
            {mainComponent}
        </React.Fragment>
    );
};

const LogoutButtonPopup = function ({ ROLES, AUTH_API, AUTH_DECODE_API }) {
    const { user, Methods } = useUser(ROLES, AUTH_API, AUTH_DECODE_API);

    const onClick = function (e) {
        if (user !== null) {
            e.preventDefault();
            Methods.logout();
        }
    };

    return (
        <button style={{ bottom: '2%', right: '2%', position: "fixed", zIndex: 100 }}
            onClick={onClick} type="button" className="btn bg-secondary">
            📦
        </button>
    )
};

export  {
    LogoutButtonPopup,   
    AuthifyNoLogout,
    Authify,
    LogoutButton,
    useUser, 
    AuthStore
}




const Model = function (ROLES, AUTH_API, AUTH_DECODE_API) {
    return {
        login: function (login, password) {
            const authFunc = ROLES.reduce(function (acc, role) {
                acc[role] = `${AUTH_API}${role}`;
                return acc;
            }, {});

            const acc = new Promise(function (resolve, reject) {
                reject('Start First Call');
            });
            return ROLES.reduce(function (acc, role) {
                return acc.catch(function () {
                    return fetch(`${authFunc[role]}?login=${login}&password=${password}`).then(function (response) {
                        if (response.ok) return response.json();
                        throw new Error('Errore di autenticazione');
                    });
                });
            }, acc);
        },
        decode: async function (jwt) {
            const res = await fetch(`${AUTH_DECODE_API}?jwt=${jwt}`);
            if (!res.ok) throw new Error('Errore di autenticazione');
            return res.json();
        },
    };
};

export default Model;

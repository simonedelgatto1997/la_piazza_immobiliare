import React from "react";
import { Link } from "react-router-dom";
import './Customer.css'

// export function CustomerProfile({ user }) {
//     React.useEffect(() => {
//         console.log('utente cambiato')
//         console.log(user)
//     },[user])
//     return user ? <Link className="a" to="/customer-edit">EDIT</Link> :
//         <div className="row justify-content-center g-2 mt-5">
//             <div className="spinner-grow" role="status">
//                 <span className="visually-hidden">Loading...</span>
//             </div>
//         </div>
// }


import { useNavigate } from "react-router-dom"
import { Authify, AuthStore, useUser } from "../auth";
import { HeaderMin } from "../main_elements/HeaderMin";
import { NavigationFooter } from "../elements/NavigationFooter";

let fD = undefined
export function CustomerProfile({ user }) {

    const { user2, Methods } = useUser(['agent', 'customer'], '/auth/', '/auth/decode/')

    const navigate = useNavigate()
    const emaiRef = React.useRef(null)
    const passRef = React.useRef(null)
    const nameRef = React.useRef(null)
    const surnameRef = React.useRef(null)
    const usernameRef = React.useRef(null)
    const cfRef = React.useRef(null)
    React.useEffect(() => {
        if (user != null) {

            cfRef.current.value = user.CF
            emaiRef.current.value = user.email
            passRef.current.value = user.password
            nameRef.current.value = user.name
            surnameRef.current.value = user.surname
            usernameRef.current.value = user.username
            fD = new FormData()
        }
    }, [user])
    React.useEffect(() => {

        if (user != null) {

            cfRef.current.value = user.CF
            emaiRef.current.value = user.email
            passRef.current.value = user.password
            nameRef.current.value = user.name
            surnameRef.current.value = user.surname
            usernameRef.current.value = user.username
            fD = new FormData()
        }
    }, [])
    console.log('CUSTOMER PROFILE')
    console.log(user, user2)
    return (
        user != null ? <React.Fragment>
            <HeaderMin></HeaderMin>
            <div className="row justify-content-center g-2 mt-3">
                <div className="form-floating col-10 col-md-5 m-2">
                    <input type="text" ref={nameRef} className="form-control" placeholder={user.name} disabled={true}>
                    </input>
                    <label>Name</label>
                </div>
                <div className="form-floating col-10 col-md-5 m-2">
                    <input type="text" ref={surnameRef} className="form-control" placeholder={user.surname} disabled={true}>
                    </input>
                    <label>Surname</label>
                </div>
                <div className="form-floating col-10 col-md-5 m-2 ">
                    <input type="email" ref={emaiRef} className="form-control form-control-lg" aria-describedby="emailHelp" placeholder={user.email} disabled={true}>
                    </input>
                    <label >Email address</label>
                    {/* <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small> */}
                </div>
                <div className="form-floating col-10 col-md-5 m-2">
                    <input type="text" ref={usernameRef} className="form-control" aria-describedby="emailHelp" placeholder={user.username} disabled={true}>
                    </input>
                    <label >Username</label>
                </div>
                <div className="form-floating col-10 col-md-5 m-2">
                    <input type="password" ref={passRef} className="form-control" placeholder={user.password} disabled={true}>
                    </input>
                    <label >Password</label>
                </div>
                <div className="form-floating col-10 col-md-5 m-2">
                    <input type="text" ref={cfRef} className="form-control" placeholder="CF" disabled={true}>
                    </input>
                    <label >Codice Fiscale</label>
                </div>


            </div>
            <div className='d-flex justify-content-end m-2'>
                <button className='btn rounded-circle customerButton' onClick={()=>{
                    navigate('/customer-edit')
                }}>
                    <i className="bi bi-pen-fill"></i>
                </button>
                <button className='btn rounded-circle customerButton' onClick={()=>{
                    Methods.logout()
                    navigate('/auth')
                }}>
                    <i className="bi bi-door-open-fill"></i>
                </button>
            </div>
            <NavigationFooter current='customer'></NavigationFooter>
        </React.Fragment > : <div className="row justify-content-center g-2 mt-5"><div className="spinner-grow" role="status">
            <span className="visually-hidden">Loading...</span>
        </div></div>
    )
}
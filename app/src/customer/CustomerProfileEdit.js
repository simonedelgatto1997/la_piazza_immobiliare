import React from "react";
import { useNavigate } from "react-router-dom"
import { Authify, AuthStore,useUser } from "../auth";
import { HeaderMin } from "../main_elements/HeaderMin";

let fD = undefined
export function CustomerProfileEdit({ user }) {

    const navigate = useNavigate()
    const emaiRef = React.useRef(null)
    const passRef = React.useRef(null)
    const nameRef = React.useRef(null)
    const surnameRef = React.useRef(null)
    const usernameRef = React.useRef(null)
    const cfRef = React.useRef(null)
    React.useEffect(() => {
        if (user != null) {

            cfRef.current.value = user.CF
            emaiRef.current.value = user.email
            passRef.current.value = user.password
            nameRef.current.value = user.name
            surnameRef.current.value = user.surname
            usernameRef.current.value = user.username
            fD = new FormData()
        }
    }, [user])
    React.useEffect(() => {

        if (user!= null) {

            cfRef.current.value = user.CF
            emaiRef.current.value = user.email
            passRef.current.value = user.password
            nameRef.current.value = user.name
            surnameRef.current.value = user.surname
            usernameRef.current.value = user.username
            fD = new FormData()
        }
    }, [])
    return (
        user != null ? <React.Fragment>
            <HeaderMin></HeaderMin>
            <div className="row justify-content-center g-2 mt-3">
                <i className="bi rounded-circle bi-person-circle"></i>
                <div className="form-floating col-10 col-md-5 m-2">
                    <input type="text" ref={nameRef} className="form-control" placeholder={user.name}>
                    </input>
                    <label>Name</label>
                </div>
                <div className="form-floating col-10 col-md-5 m-2">
                    <input type="text" ref={surnameRef} className="form-control" placeholder={user.surname}>
                    </input>
                    <label>Surname</label>
                </div>
                <div className="form-floating col-10 col-md-5 m-2 ">
                    <input type="email" ref={emaiRef} className="form-control form-control-lg" aria-describedby="emailHelp" placeholder={user.email}>
                    </input>
                    <label >Email address</label>
                    {/* <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small> */}
                </div>
                <div className="form-floating col-10 col-md-5 m-2">
                    <input type="text" ref={usernameRef} className="form-control" aria-describedby="emailHelp" placeholder={user.username}>
                    </input>
                    <label >Username</label>
                </div>
                <div className="form-floating col-10 col-md-5 m-2">
                    <input type="password" ref={passRef} className="form-control" placeholder={user.password}>
                    </input>
                    <label >Password</label>
                </div>
                <div className="form-floating col-10 col-md-5 m-2">
                    <input type="text" ref={cfRef} className="form-control" placeholder = "CF">
                    </input>
                    <label >Codice Fiscale</label>
                </div>
                <div className="col-10 col-md-5 m-2">
                    <label htmlFor="CI" className="form-label">Carta d'identità</label>
                    <input className="form-control" type="file" id="CI"
                        onChange={(e) => {
                            for (let i = 0; i < e.target.files.length; i++) {
                                let f = e.target.files[i]
                                console.log('append file')
                                fD.append('CI', f)
                            }

                        }} multiple={true}></input>
                </div>
                <div className="col-10 col-md-5 m-2">
                    <label htmlFor="SC" className="form-label">Stato civile</label>
                    <input className="form-control " type="file" id="SC"
                        onChange={(e) => {
                            for (let i = 0; i < e.target.files.length; i++) {
                                let f = e.target.files[i]
                                fD.append('SC', f)
                            }

                        }} multiple={true}></input>
                </div>


                <i style={{ bottom: '2%', right: '2%', position: "fixed", height: '50px', width: '50px', zIndex: 100, backgroundColor: 'rgb(40, 166, 160)' }} className="btn bi bi-journal-check rounded-circle m-1" onClick={async (e) => {
                    e.preventDefault()
                    fD.append('email', emaiRef.current.value)
                    fD.append('password', passRef.current.value)
                    fD.append('name', nameRef.current.value)
                    fD.append('surname', surnameRef.current.value)
                    fD.append('username', usernameRef.current.value)
                    fD.append('_id', user._id)
                    fD.append('CF', cfRef.current.value)
                    try {
                        const res = await fetch(`/users/${user._id}?jwt=${user.jwt}`, {
                            body: fD,
                            method: 'PUT',
                            "Content-type": "multipart/form-data"
                        })
                        if (res.ok) {
                            let nU = await res.json()
                            nU.jwt = user.jwt
                            AuthStore.setValue(nU)
                            navigate('/customer')
                        }
                    } catch (err) {
                        console.log(err)
                    }
                    //dovrei creare qualcosa per dire che c'è stato un errore
                }}></i>
            </div>
        </React.Fragment > : <div className="row justify-content-center g-2 mt-5"><div className="spinner-grow" role="status">
            <span className="visually-hidden">Loading...</span>
        </div></div>
    )
}
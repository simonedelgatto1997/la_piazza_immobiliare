import React from "react"
let curr = null 
function FeedBackBar({ curr, popFeedBack }) {
    React.useEffect(() => {
        if(curr == null){

            setTimeout(()=>{
                popFeedBack()
            }, 5000)
        }
    },[curr])
    return <div id='FeedBackBar' >
        <div className='d-flex justify-content-evenly p-2'>
            {curr != null && <span className="badge rounded-pill bg-secondary">{curr}</span>}
        </div>
    </div>
}

const FeedBackStore = (() => {
    let feedBack = []
    return {
        get: function () {
            return feedBack
        },
        add: function(fb){
            feedBack = [...feedBack, fb]
        },

        pop: function () {
            return feedBack.pop()
        }
    }
})()

let c = 0
export function FeedBack({ children }) {
    const [curr, setCurr] = React.useState(null)

    function addFeedBack(fb) {
        FeedBackStore.add(fb)
        if(curr ==  null)
            popFeedBack()
    }

    function popFeedBack() {
        setCurr(FeedBackStore.pop())
    }

    const elements = React.Children.map(children, function (child) {
        return React.cloneElement(child, { addFeedBack: addFeedBack});
    })

    elements.push(<FeedBackBar popFeedBack={popFeedBack} curr={curr}/>)

    return <React.Fragment>
        {elements}
    </React.Fragment>
}
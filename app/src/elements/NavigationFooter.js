import { useNavigate } from 'react-router-dom'
import './element.css'

export function NavigationFooter({ current, user }) {
    const navigate = useNavigate()

    function navigateToLatestSearch() {
        navigate('/search')
    }
    function navigateToSavedSearches() {
        navigate('/savedSearches')
    }
    function navigateToCustomer() {
        navigate('/customer')
    }
    function navigateToFavourites() {
        navigate('/favourites')
    }
    return <div id='NavigationFooter' >
        <div className='d-flex justify-content-evenly p-2'>
            <div className={`btn rounded-circle  ${current == 'search' ? 'footerIconContainerSelected' : 'footerIconContainer'}`}
                onClick={navigateToLatestSearch}>
                <i className="bi bi-search"></i>
            </div>
            <div className={`btn rounded-circle  ${current == 'savedSearch' ? 'footerIconContainerSelected' : 'footerIconContainer'}`}
                onClick={navigateToSavedSearches}>
                <i className="bi bi-bell "></i>
            </div>
            <div className={`btn rounded-circle  ${current == 'favourites' ? 'footerIconContainerSelected' : 'footerIconContainer'}`}
                onClick={navigateToFavourites}>
                <i className="bi bi-heart-fill "></i>
            </div>
            <div className={`btn rounded-circle  ${current == 'customer' ? 'footerIconContainerSelected' : 'footerIconContainer'}`}
                onClick={navigateToCustomer}>
                <i className="bi bi-person "></i>
            </div>
        </div>
    </div>
}
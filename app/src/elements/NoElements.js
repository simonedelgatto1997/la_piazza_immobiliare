import React from 'react'

export const NoElementsFound = function ({ children, user }) {
    let childrenFiltered = children.filter(c => !(Array.isArray(c) && c.length == 0))
    const childrenToShow = childrenFiltered.length > 1 ? childrenFiltered.slice(0, childrenFiltered.length - 1) : null
    const mainComponent =
        childrenFiltered.length > 1 ? (
            React.Children.map(childrenToShow, function (child) {
                return React.cloneElement(child, { user: user });
            })
        ) : (
            React.cloneElement(childrenFiltered[0], { user: user })
        );
    return (
        <React.Fragment>
            {mainComponent}
        </React.Fragment>
    );
};


export const NoLogin = function ({ children, user }) {
    console.log('No Login',user)
    const childrenToShow = children.length > 1 ? children.slice(0, children.length - 1) : null
    const mainComponent =
        user != null ? (
            React.Children.map(childrenToShow, function (child) {
                return React.cloneElement(child, { user: user });
            })
        ) : (
            React.cloneElement(children[children.length - 1], { user: user })
        );
    return (
        <React.Fragment>
            {mainComponent}
        </React.Fragment>
    );
};

export function Loading({children, loading, props}){
    const toShow = ! loading ? (
        React.Children.map(children, function (child) {
            return React.cloneElement(child, {...props});
        })) : <div className="in_the_middle"><div className="spinner-grow" role="status">
        <span className="sr-only">Loading...</span>
    </div></div>
    
    return <React.Fragment>
        {toShow}
    </React.Fragment>
}



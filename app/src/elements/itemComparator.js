import { Search } from "../search/search"
import * as OH from 'object-hash' 
function getSavedItems(itemType) {
    return () => {  
        const res = JSON.parse(window.localStorage.getItem(itemType))
        return res == null ? [] : res 
    }
}

function saveItem(itemType, parseF) {
    return async (item, alreadyParsed) => {
        //console.log('saveItem',itemType, getSavedItems(itemType))
        let p = item
        delete p.diff
        if(alreadyParsed == undefined || !alreadyParsed){
            p = await parseF(item)
        }
        console.log('p', p)
        let toSave = getSavedItems(itemType)().filter(i => {
            console.log(i.id, p.id)
            return i.id != p.id
        })
        console.log('to save',toSave)
        toSave.push(p)
        window.localStorage.setItem(itemType, JSON.stringify(toSave))
    }
}

function compare(itemType, parseF, comparatorFunction) {
    return async (items) => {
        const sI = getSavedItems(itemType)()
        for (let index = 0; index < items.length; index++) {
            const n = (await parseF(items[index]))
            const old = sI.filter(s => s.id == n.id)[0]
            //console.log('new', n, 'old', old)
            if (old == null) {
                await saveItem(itemType, parseF)(n, true)
                items[index].diff = null
            } else {
                items[index].diff = comparatorFunction(old, n)
            }
        }
        return items
    }
}


export const SavedSearchComparator = (() => {
    const parseF = async (ss) => {
        const id = OH.MD5(JSON.stringify(ss))
        return {
            id:id,
            len: (await Search.searchOneTime(ss)).length
        }
    }
    const comparatorF = (old, n) => {
        console.log('compare', old, n)
        return n.len - old.len
    }
    const TYPE = 'SavedSearch'
    return {
        save: async (item) => {

            console.log('call save', item)
            return await saveItem(TYPE, parseF)(item)
        },
        compare: async (items) => {
            return await compare(TYPE, parseF, comparatorF)(items)
        }
    }
})()

export const HouseComparator = (() => {
    const parseF = async (h) => {
        const id = h._id
        delete h.diff
        return {
            id:id,
            house: h
        }
    }
    const comparatorF = (old, n) => {
        return deepEqual(old, n)
    }
    const TYPE = 'House'
    return {
        //quando la casa viene salvata nei preferiti e quando viene acceduta la sua cards dai preferiti
        save: async (item) => {

            console.log('call save', item)
            return await saveItem(TYPE, parseF)(item)
        },
        compare: async (items) => {
            return await compare(TYPE, parseF, comparatorF)(items)
        }
    }
})()



function deepEqual(object1, object2) {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);
    if (keys1.length !== keys2.length) {
      return false;
    }
    for (const key of keys1) {
      const val1 = object1[key];
      const val2 = object2[key];
      const areObjects = isObject(val1) && isObject(val2);
      if (
        areObjects && !deepEqual(val1, val2) ||
        !areObjects && val1 !== val2
      ) {
        return false;
      }
    }
    return true;
  }
  function isObject(object) {
    return object != null && typeof object === 'object';
  }
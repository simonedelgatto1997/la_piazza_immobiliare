import * as React from 'react'

const fD = new FormData()
export function FileUploader({user}) {

    return (
        <div >
            {user.username}
            <label htmlFor="formFile" className="form-label">Default file input example</label>
            <input className="form-control" type="file" id="formFile"
                onChange={(e) => {
                    fD.append('user', user)
                    for (let i = 0; i < e.target.files.length; i++) {
                        let f = e.target.files[i]
                        fD.append('documentation', f)
                    }

                }} multiple={true}></input>

            <label htmlFor="formFile" className="form-label">Default file input example</label>
            <input className="form-control" type="file" id="formFile"
                onChange={(e) => {
                    fD.delete('second')
                    fD.append('second', e.target.files[0])
                    console.log(fD.entries())
                }}></input>

            <button className="btn btn-success mb-3" onClick={() => {
                //`/users/${user._id}/documents?jwt=${user.jwt}`
                fetch(`/users/${user._id}jwt=${user.jwt}`, {
                    method: 'POST',
                    body: fD,
                    "Content-type": "multipart/form-data"
                })
            }}>Load documents</button>
        </div>
    )
}
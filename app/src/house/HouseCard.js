import React from 'react'
import { getBuildingStates, isExcellent, isGood, isHabitable, isNew } from './house'
import { useNavigate } from 'react-router-dom'
import { addUserFavourite, checkIsAlreadyAfavourite, deleteUserFavourite } from './favourites'
import { AuthStore } from '../auth'
import { addUserInterest, checkIsAlreadyAInterest, deleteUserInterest } from './interests'
import { HouseComparator } from '../elements/itemComparator'
import { CustomerWebSocket } from '../websocket/customerWebSocket'


function Photos({ house }) {
    function getId(h) {
        return h.address.split(' ')
            .map(str => str.replace(/[^a-zA-Z ]/g, ""))
            .reduce((prev, current) => prev + current, '')
    }
    function getInterval() {
        return 24 * 60 * 60 * 1000
    }
    function getPhotos() {
        return house.photos
            .map(p => p.download_link)
            .map((dl, i) => {
                const c = i == 0 ? "carousel-item active" : "carousel-item"
                return <div key={i} className={c} data-bs-interval={getInterval()}>
                    <img style={{ width: '100%', height: '240px', objectFit: 'cover' }} src={dl} alt="house image" ></img>
                </div>
            })
    }
    const id = getId(house)

    return (
        <div id={id} className="carousel slide" data-bs-ride="carousel">
            <div className="carousel-inner">
                {getPhotos()}
            </div>
            <button className="carousel-control-prev" type="button" data-bs-target={`#${id}`} data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
            </button>
            <button className="carousel-control-next" type="button" data-bs-target={`#${id}`} data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
            </button>
        </div>
    )
}


export function HouseCard({ house, user, showDetails }) {

    const navigate = useNavigate()

    function getTitle() {
        return house.title != null ? house.title : `${house.ref} - ${house.category} - ${house.address}`
    }

    function getPrice() {
        return house.agreement == 'AFFITTO' ? `€ ${house.price}/mese` : `€ ${house.price}`
    }

    function getStateIcon() {
        const newIcon = <div>Nuovo </div>
        const excellentIcon = <div>Ottimo <i className="bi bi-check-all"></i></div>
        const goodIcon = <div>Buono <i className="bi bi-check"></i></div>
        const renovateIcon = <div>Da ristrutturare <i className="bi bi-hammer"></i></div>
        if (isHabitable(house)) {
            return isNew(house) ? newIcon : isExcellent(house) ? excellentIcon :
                isGood(house) ? goodIcon : renovateIcon
        }

        // return getBuildingStates()
        // .filter(bs => bs.value == house.building_state)
        // .map(bs => <div>{bs.value} {bs.icon}</div>)
    }

    function getRoomsIcon() {
        if (isHabitable(house))
            return <div><i className="bi bi-border-left"></i>{house.rooms}</div>
    }

    function checkIfIsLogin(cb) {
        if (user == null) {
            navigate('/auth')
        } else {
            cb(house, user)
        }
    }



    function setAsFavourite() {
        if (!checkIsAlreadyAfavourite(user, house)) {
            checkIfIsLogin(async (house, user) => {
                let u = await addUserFavourite(house._id, user)
                u.jwt = user.jwt
                AuthStore.setValue(u)
            })
        } else {

            checkIfIsLogin(async (house, user) => {
                let u = await deleteUserFavourite(house._id, user)
                u.jwt = user.jwt
                AuthStore.setValue(u)
            })
        }
    }

    function setAsInterest() {
        if (!checkIsAlreadyAInterest(user, house)) {
            checkIfIsLogin(async (house, user) => {
                let u = await addUserInterest(house._id, user)
                HouseComparator.save(house)
                u.jwt = user.jwt
                AuthStore.setValue(u)
                CustomerWebSocket.expressInterest(user._id, house)
            })
        } else {
            checkIfIsLogin(async (house, user) => {
                let u = await deleteUserInterest(house._id, user)
                u.jwt = user.jwt
                AuthStore.setValue(u)
            })
        }
    }



    return (
        <div className='container-fluid mt-3 houseCard'>
            <hr ></hr>
            <div className='row mt-2'>
                <Photos house={house}></Photos>
            </div>
            <h5 className='houseCardTitle m-2' onClick={showDetails}>
                {getTitle()}    {house.diff != null && !house.diff ? <i className="bi bi-exclamation-circle .text-secondary"></i> : ''}
            </h5>
            <h5 className='houseCardPrice m-2'>{getPrice()}</h5>
            <div id='houseCardCharacteristics'>
                <div><i className="bi bi-rulers"></i> {house['square_meter'] == null ? 100 : house['square_meter']}m²</div>
                {getRoomsIcon()}
                {getStateIcon()}
            </div>
            <div className='d-flex justify-content-end mt-2'>
                <button className='btn rounded-circle actionButton' onClick={setAsFavourite}>
                    {checkIsAlreadyAfavourite(user, house) ?
                        <i className="bi bi-heart-fill"></i> : <i className="bi bi-heart"></i>}
                </button>
                <button className='btn rounded-circle actionButton' onClick={setAsInterest}>
                    {checkIsAlreadyAInterest(user, house) ? <i className="bi bi-envelope-fill"></i> : <i className="bi bi-envelope"></i>}
                </button>
            </div>

        </div>
    )
}
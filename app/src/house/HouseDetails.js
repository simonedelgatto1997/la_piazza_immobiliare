import React from "react";
import { HouseCard } from "./HouseCard";

export function HouseDetails({ house, user, stopShowHouse }) {

    function BackButton() {

        return <button style={{
            top: '150px',
            left: '0%',
            position: 'fixed',
            zIndex: 100
        }} class="btn rounded-circle btn-secondary" onClick={() => {
            stopShowHouse()
        }}>
            <i className="bi bi-arrow-left-circle"></i>
        </button>
    }

    function printBooleanElement(mapping) {
        function getMapping(k) {
            return mapping.filter(m => m.key == k)[0]
        }
        let mapKeys = mapping.map(e => e.key)
        Object.keys(house).filter(k => mapKeys.includes(k)).forEach(e => {
            console.log(e)
            console.log(house[e])
        })
        return <div className="row">

            {Object.keys(house).filter(k => mapKeys.includes(k)).filter(k => house[k])
                .map(k =>
                    <div className="col-6 col-md-5">
                        <div className={'btn mt-2 greenButtonFilterSelected'}>
                            {getMapping(k).title}
                        </div>
                    </div>)}
        </div>
    }

    function printElement(mapping) {
        function getMapping(k) {
            return mapping.filter(m => m.key == k)[0]
        }
        let mapKeys = mapping.map(e => e.key)
        Object.keys(house).filter(k => mapKeys.includes(k)).forEach(e => {
            console.log(e)
            console.log(house[e])
        })
        return Object.keys(house).filter(k => mapKeys.includes(k))
            .map(k => <div className="row">
                <div className="col-5 col-md-5">
                    {getMapping(k).title}
                </div>
                <div className="col-5 col-md-5">
                    {getMapping(k).parse == null ? house[k] : getMapping(k).parse(house[k])}
                </div>
            </div>)
    }

    function getCharacteristic() {
        return printElement([
            { key: 'agreement', title: 'CONTRATTO' },
            { key: 'category', title: 'CATEGORIA' },
            { key: 'locals', title: 'LOCALI' },
            { key: 'locals', title: 'LOCALI' },
            { key: 'property_type', title: 'TIPO PROPRIETA\'' },
            { key: 'bathrooms', title: 'BAGNI' },
            { key: 'kitchen', title: 'CUCINA' },
            { key: 'building_year', title: 'ANNO DI COSTRUZIONE' },

        ])
    }

    function getBooleanCharacteristic() {
        return printBooleanElement([
            { key: 'garden', title: 'Giardino' },
            { key: 'balcony', title: 'Balcone' },
            { key: 'garage', title: 'Garage' },
            { key: 'cellar', title: 'Cantina' },
            { key: 'air_conditioner', title: 'Climatizzatore' },
            { key: 'indipendent_entrace', title: 'Ingresso Indipendente' },
        ])
    }

    console.log('classe energetica',house.indexAPE)

    return <React.Fragment>
        <BackButton />
        <HouseCard house={house} user={user}></HouseCard>
        <div className='container-fluid mt-3 houseCard'>
            <h6>Descrizione</h6>
            <hr />
            <p>{house.description}</p>
            <h6>Caratteristiche</h6>
            {getCharacteristic()}
            {
                house.indexAPE != null &&
                <div className="row">
                    <div className="col-5 col-md-5">
                        CLASSE ENERGETICA
                    </div>
                    <div className="col-5 col-md-5">
                        {JSON.parse(house.indexAPE).scale}
                    </div>
                </div>
            }
            {getBooleanCharacteristic()}
            <hr />
            <div className="mb-5"></div>
        </div>

    </React.Fragment>
}
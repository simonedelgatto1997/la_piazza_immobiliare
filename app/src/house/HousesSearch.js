import { HouseCard } from "./HouseCard";
import './House.css'
import { HouseDetails } from "./HouseDetails";
import React from "react";

export function HousesSearch({ houses, loading, user }) {
    const [currHouse, setCurrHouse] = React.useState(null)
    function showHouses(){
        return houses.map((h, index) => <HouseCard key={index} 
        house={h} user={user} showDetails={()=>{setCurrHouse(h)}}
        ></HouseCard>)
    }
    function stopShowHouse(){
        setCurrHouse(null)
    }
    return (
        loading ? <div className="in_the_middle"><div className="spinner-grow" role="status">
            <span className="sr-only">Loading...</span>
        </div></div> : houses.length == 0 ? <div className="d-flex flex-column in_the_middle2">
            
            <span><i className="bi bi-house"></i> Nessuna casa trovata</span>
        </div> : currHouse == null ?
            <div id='HousesSearch' className="container">
                {showHouses()}
            </div> : <HouseDetails house={currHouse} user={user} stopShowHouse={stopShowHouse}></HouseDetails>
    )
}
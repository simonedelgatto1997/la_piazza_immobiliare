import React from "react";

function Images({ houses }) {
  return <React.Fragment>
    {houses.map((e, i) => {
      let w = window.innerWidth >= 600 ? '100%' : '100%'
      console.log(window.innerWidth, w)
      if (i == counter) {
        return <div class="carousel-item active" style={{ width: w, height: '240px' }}>
          <img src={e.photos[0].download_link} style={{ width: '100%', height: '240px', objectFit: 'cover' }} alt="..." />
        </div>
      } else {
        return <div class="carousel-item" style={{ width: w, height: '240px' }}>
          <img src={e.photos[0].download_link} style={{ width: '100%', height: '240px', objectFit: 'cover' }} alt="..." />
        </div>
      }
    })}
  </React.Fragment>
}
let counter = 0
export function PriorityHouses() {
  const [houses, setHouses] = React.useState([])
  const [title, setTitle] = React.useState('')
  React.useEffect(async () => {
    let res = await (await fetch('/houses/priority?n=5')).json()
    console.log('res', res)
    setHouses(res)
  }, [])

  React.useEffect(() => {
    if (houses.length > 0) {
      counter = 0
      console.log('set event')
      setTitle(houses[0].title != null ? houses[0].title : `${houses[0].category} - ${houses[0].address}`)
      var myCarousel = document.getElementById('priorityHouses')
      myCarousel.addEventListener('slide.bs.carousel', function () {
        console.log('events')
        counter = counter == houses.length - 1 ? 0 : counter + 1
        setTitle(houses[counter].title != null ? houses[counter].title : `${houses[counter].category} - ${houses[counter].address}`)
      })
    }
  }, [houses])

  return <React.Fragment>
    <div className="m-2">
      <div class="d-flex justify-content-center">
        <h3 style={{ color: 'rgb(40, 166, 160)' }}>In evidenza</h3>
      </div>
      {houses.length > 0 ? <div>
        <div class="d-flex justify-content-center">
          <div id="priorityHouses" className="carousel slide" data-bs-ride="carousel" data-bs-interval="5000">
            <div className="carousel-inner">
              <Images houses={houses}></Images>
            </div>
          </div>
        </div>
        <div class="d-flex justify-content-center  m-2">
          <h5 className='houseCardTitle m-2'>
            {title}
          </h5>
        </div>
      </div> : <div></div>}
    </div>
  </React.Fragment>
}
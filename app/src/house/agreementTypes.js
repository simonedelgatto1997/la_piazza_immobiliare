export function defaultAgreement(){
    return {name: 'Vendita'}
}

export function getAgreementTypes(){
    return [{name: 'Vendita'}, {name: 'Affitto'} ]
}

export function getRenderingAgreementTypes(){
    return [
            {name: 'Vendita'}, 
            {name: 'Affitto'} 
    ]   .map((e) => {
            e = <option key={e.name} value={e.name}>{e.name}</option>
            return e
        })
}
//is mandatory to import SavedSearches.css
export function getBuildingStates(){
    return  [
        {
            value: null,
            icon: ''
        },
        {
            value: 'Buono',
            icon: <i className="bi bi-check"></i>
        },
        {
            value: 'Ottimo',
            icon: <i className="bi bi-check-all"></i>
        },
        {
            value: 'Da ristrutturare',
            icon: <i className="bi bi-hammer"></i>
        },
        {
            value: 'Nuovo',
            icon: ''
        },
    ].map((bs) => {
        bs.classicView = <div className="simpleText">{bs.value} {bs.icon}</div>
        return bs
    })
}

export function getRenderingBuildingStates(){
    return  [
        {value: 'Nuovo'},
        {value: 'Ottimo'},
        {value: 'Buono'},
        {value: 'Da ristrutturare'},
    ].map((e) => {
        e = <option key={e.value} value={e.value}>{e.value}</option>
        return e
    })
}
export function getCadrastalCategory(){
    return  [
        {value: 'Scegli'},
        {value: 'A/1',},
        {value: 'A/2',},
        {value: 'A/3',},
        {value: 'A/4',},
        {value: 'A/5',},
        {value: 'A/6',},
        {value: 'A/7',},
        {value: 'A/8',},
        {value: 'A/9',},
        {value: 'A/10',},
        {value: 'A/11',},

        {value: 'B/1',},
        {value: 'B/2',},
        {value: 'B/3',},
        {value: 'B/4',},
        {value: 'B/5',},
        {value: 'B/6',},
        {value: 'B/7',},
        {value: 'B/8',},
        
        {value: 'C/1',},
        {value: 'C/2',},
        {value: 'C/3',},
        {value: 'C/4',},
        {value: 'C/5',},
        {value: 'C/6',},
        {value: 'C/7',},
        
        {value: 'D/1',},
        {value: 'D/2',},
        {value: 'D/3',},
        {value: 'D/4',},
        {value: 'D/5',},
        {value: 'D/6',},
        {value: 'D/7',},
        {value: 'D/8',},
        {value: 'D/9',},
        {value: 'D/10',},
        {value: 'D/11',},
        {value: 'D/12',},

        {value: 'E/1',},
        {value: 'E/2',},
        {value: 'E/3',},
        {value: 'E/4',},
        {value: 'E/5',},
        {value: 'E/6',},
        {value: 'E/7',},
        {value: 'E/8',},
        {value: 'E/9',},

        {value: 'F/1',},
        {value: 'F/2',},
        {value: 'F/3',},
        {value: 'F/4',},
        {value: 'F/5',},
        {value: 'F/6',},

        {value: 'T',},
    ].map((e) => {
        e = <option key={e.value} value={e.value}>{e.value}</option>
        return e
    })
}

export function getAllCategories(){
    return ['Residenziale', 'Garage o Posto auto', 'Ufficio o Negozio', 'Capannone','Terreno agricolo', 'Lotto edificabile']
}

export function getRenderingCategories(){
    return  [
        {value: 'Residenziale',},
        {value: 'Garage o Posto auto',},
        {value: 'Ufficio o Negozio',},
        {value: 'Capannone',},
        {value: 'Terreno agricolo',},
        {value: 'Lotto edificabile',},
    ].map((bs) => {
        bs = <option key={bs.value} value={bs.value}>{bs.value}</option>
        return bs
    })
    /*return getAllCategories.map(e => {
        e = <option key={e} value={e}>{e}</option>
        return e
    });*/
}

export function getRenderingContractTypes(){
    return  [
        {value: 'Intera proprietà'},
        {value: 'Nuda proprietà'},
        {value: 'Usufrutto'},
    ].map((e) => {
        e = <option key={e.value} value={e.value}>{e.value}</option>
        return e
    })
}
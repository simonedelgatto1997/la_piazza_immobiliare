export function getRenderingEnergeticClass(){
    return  [
        {value: 'A4',
        max:'26'},
        {value: 'A3',
        max:'40'},
        {value: 'A2',
        max:'53'},
        {value: 'A1',
        max:'67'},
        {value: 'B',
        max:'80'},
        {value: 'C',
        max:'100'},
        {value: 'D',
        max:'134'},
        {value: 'E',
        max:'174'},
        {value: 'F',
        max:'234'},
        {value: 'G',
        max:'1000'},
    ].map((e) => {
        e = <option key={e.max} value={e.value}>{e.value}</option>
        return e
    })
}
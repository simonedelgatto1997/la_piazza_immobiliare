import { Search } from "../search/search"
import { HouseCard } from "./HouseCard"
import React from 'react'
import { Loading, NoElementsFound } from "../elements/NoElements"
import { NavigationFooter } from "../elements/NavigationFooter"
import { HouseComparator } from "../elements/itemComparator"
import { HouseDetails } from "./HouseDetails"

export function isAFavourites(user, houseId) {
    return user.favourites.map(f => f._id).includes(houseId)
}

export async function addUserFavourite(houseId, user) {
    return await updateUserFavourites(houseId, false, user)
}

export async function deleteUserFavourite(houseId, user) {
    return await updateUserFavourites(houseId, true, user)
}

export function checkIsAlreadyAfavourite(user, house) {
    return user != null && user.favourites.map(f => f._id).includes(house._id)
}

async function updateUserFavourites(houseId, toDelete, user) {
    let fd = new FormData()
    fd.append('favourites', JSON.stringify({ _id: houseId, toDelete: toDelete }))
    let res = await fetch(`/users/${user._id}?jwt=${user.jwt}`, {
        method: 'PUT',
        body: fd,
        "Content-type": "multipart/form-data"
    })
    return await res.json()
}

function NoFavourites() {
    return <h5 id='NoElement'>Nessun immobile salvato</h5>

}

export function Favourites({ user }) {
    const [sf, setSF] = React.useState([])
    const [currHouse, setCurrHouse] = React.useState(null)
    const [l, setL] = React.useState(true)

    function stopShowHouse() {
        setCurrHouse(null)
    }

    React.useEffect(async () => {
        // console.log('UTENETE PRIMA VOLTA', user)      
        const res = await Search.searchOneTime({ ids: user.favourites.map(e => e._id) })
        setSF(res)
        setL(false)
    }, [])

    React.useEffect(async () => {
        // console.log('UTENETE AGGIORNATO', user)
        const res = await Search.searchOneTime({ ids: user.favourites.map(e => e._id) })
        setSF((await HouseComparator.compare(res)))
    }, [user])

    function createFavouriteCards() {
        console.log('create card', sf.length)
        console.log(user)
        return sf.map((f, index) => {
            return <HouseCard house={f} user={user} key={index} showDetails={() => {
                HouseComparator.save(f)
                setCurrHouse(f)
            }}></HouseCard>
        })

    }


    return <React.Fragment>
        <Loading loading={l}>
            <NoElementsFound user={user}>
                {currHouse == null ? createFavouriteCards() : <HouseDetails house={currHouse}
                    user={user} stopShowHouse={stopShowHouse} />}
                <NoFavourites></NoFavourites>
            </NoElementsFound>
        </Loading>
        <NavigationFooter current='favourites'></NavigationFooter>
    </React.Fragment>
}
export function getRenderingFloor(){
    return  [
        {value: 'Terra',
        key:'PT'},
        {value: 'Rialzato',
        key:'PR'},
        {value: 'Primo',
        key:'1P'},
        {value: 'Secondo',
        key:'2P'},
        {value: 'Terzo',
        key:'3P'},
        {value: '> Quarto',
        key:'>4P'},
        {value: 'Interrato',
        key:'-1P'},
        {value: 'Seminterrato',
        key:'-P'},
    ].map((e) => {
        e = <option key={e.key} value={e.value}>{e.value}</option>
        return e
    })
}
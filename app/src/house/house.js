export function isHabitable(h){
    return ['Residenziale', 'Ufficio o Negozio'].includes(h.category)
}

export function isToRenovate(h){

    return h.building_state == 'Da ristrutturare'
}

export function isNew(h){

    return h.building_state == 'Nuovo'
}

export function isExcellent(h){

    return h.building_state == 'Ottimo'
}

export function isGood(h){

    return h.building_state == 'Buono'
}

export function getBuildingStates(){
    return  [
        {
            value: 'Buono',
            icon: <i className="bi bi-check"></i>
        },
        {
            value: 'Ottimo',
            icon: <i className="bi bi-check-all"></i>
        },
        {
            value: 'Da ristrutturare',
            icon: <i className="bi bi-hammer"></i>
        },
        {
            value: 'Nuovo',
            icon: <div style={{width:'15px', height:'15px'}}>  </div>
        },
    ]
}
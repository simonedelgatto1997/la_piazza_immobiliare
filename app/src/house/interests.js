export function isAInterests(user, houseId) {
    return user.interests.map(f => f._id).includes(houseId)
}

export async function addUserInterest(houseId, user) {
    return await updateUserInterests(houseId, false, user)
}

export async function deleteUserInterest(houseId, user) {
    return await updateUserInterests(houseId, true, user)
}

export function checkIsAlreadyAInterest(user, house){
    return user != null && user.interests.map(f => f._id).includes(house._id)
}

async function updateUserInterests(houseId, toDelete, user) {
    let fd = new FormData()
    fd.append('interests', JSON.stringify({ _id: houseId, toDelete: toDelete }))
    let res = await fetch(`/users/${user._id}?jwt=${user.jwt}`, {
        method: 'PUT',
        body: fd,
        "Content-type": "multipart/form-data"
    })
    return await res.json()
}
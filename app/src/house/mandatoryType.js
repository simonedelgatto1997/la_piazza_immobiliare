export function getRenderingMandatory(){
    return  [
        {value: 'Esclusiva',
        key:'1'},
        {value: 'Co-esclusiva',
        key:'4'},
        {value: 'Non esclusiva',
        key:'7'},
        {value: 'Verbale',
        key:'9'},
        {value: 'Collaborazione',
        key:'10'},
    ].map((e) => {
        e = <option key={e.key} value={e.value}>{e.value}</option>
        return e
    })
}
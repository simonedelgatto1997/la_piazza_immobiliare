import './Header.css';
import * as React from 'react'
import {useNavigate} from "react-router-dom"

function Footer() {
	const navigate = useNavigate()
    return (
        <footer className="bd-footer py-2 mt-3 bg-light">
  			<div className="container py-2">
    			<div className="row">
      				<div className="col-lg-5 mb-3 text-center">
        				<a className="d-inline-flex align-items-center mb-2 link-dark text-decoration-none" href="/" aria-label="Bootstrap">
          					<img src='/lplogo.png' width="40" height="50" className="d-block me-2" viewBox="0 0 118 94" role="img"/>
          					<span className="fs-3">La Piazza <h5>di Valgimigli Antonella</h5></span>
        				</a>
						<ul className="list-unstyled small text-muted">
							<li className="mb-2"><a href="https://www.google.com/maps/place/L'Agenzia+LA+PIAZZA/@44.3184385,11.7989856,15z/data=!4m5!3m4!1s0x0:0x12cd8d8cffa0cb4d!8m2!3d44.3184531!4d11.7989508">Via Garavini,15</a> 48014 - Castel Bolognese (RA)</li>
							<li className="mb-2">P.IVA: 02454890399</li>
							<li className="mb-2">Iscrizione Albo Agenzie immobiliari n.1878</li>
						</ul>
      				</div>
      				<div className="col-6 col-lg-3 mb-3 text-center">
    					<h5>Contattaci</h5>
						<ul className="list-unstyled">
							<li className="mb-2"><a href="tel:+390546656506">0546 656506</a></li>
							<li className="mb-2"><a href="tel:+393357071755">335 7071755</a></li>
							<li className="mb-2"><a href="tel:+393665611895">366 5611895</a></li>
							<li className="mb-2"><a href="mailto:info@lapiazzaimmobiliare.it">info@lapiazzaimmobiliare.it</a></li>
							<li className="mb-2"><a href="mailto:valgimigliantonella@arubapec.it">valgimigliantonella@arubapec.it</a></li>
						</ul>
					</div>
					<div className="col-6 col-lg-2 mb-3 text-center">
		    			<h5>Links</h5>
						<ul className="list-unstyled">
							<li className="mb-2"><a href="/">Home</a></li>
							<li className="mb-2"><a onClick={() => navigate("/agenzia")}>Agenzia</a></li>
							<li className="mb-2"><a onClick={() => navigate("/mutui")}>Mutui</a></li>
							<li className="mb-2"><a onClick={() => navigate("/auth")}>Log in</a></li>

						</ul>
					</div>
					<div className="col-6 col-lg-2 mb-3 text-center">
						<h5>Social</h5>
						<ul className="list-unstyled">
						<li className="mb-2"><a href="https://www.facebook.com/www.lapiazzaimmobiliare.it">
							<i className="fa fa-facebook-official" aria-hidden="true"></i>
							<span>  Facebook</span>
						</a></li>
						<li className="mb-2"><a href="https://www.instagram.com/lapiazzaimmobiliare/">
						<i className="fa fa-instagram" aria-hidden="true"></i>
							<span>  Instagram</span>
						</a></li>
						</ul>
					</div>
				</div>
			</div>
		</footer>
    );
}

export default Footer;
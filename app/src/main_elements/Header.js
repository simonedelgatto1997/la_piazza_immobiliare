import React, { Component } from "react";
import { Link } from "react-router-dom";
import { useUser } from "../auth/hook";
import './Header.css'


function Header({ user }) {

  function setResponsive() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
    console.log('click');
  }

    return (
      <><header className="App-header">
        <a className="navbar-brand" href="/ " title="Logo">
          <img src='/logoLap.png' className="logoImg" />
        </a>
        </header>
        <nav className="topnav navbar-expand-sm bg-light navbar-light">
        <div className="container-fluid">
        <div className="topnav" id="myTopnav">
          <Link className="a" to="/">Home</Link>
          <Link className="a" to="/agenzia">Agenzia</Link>
          <Link className="a" to="/mutui">Mutui</Link>
          {user == null ?
              <Link className="a" to="/auth">Log in</Link> : <div></div>
          }
          {user != null && user.role == 'customer' && 
            <Link className="a" to='/customer'>
              <i className="bi bi-person-circle"></i>
            </Link>
          }
          {user != null && user.role == 'agent' && 
            <Link className="a" to="/agentHome/showHouses">
              Home page Agente {user.name}
            </Link>
          }
          <a className="icon" onClick={setResponsive}>
            <i className="fa fa-bars"></i>
          </a>
        </div>
        </div>
      </nav>
      </>
    );

}

export default Header;

/* replace line 31 - in realtà meglio rinderizzare dopo login agente
  {user != null && user.role == 'agent' &&
    <Link className="a" to="/agentHome">Prova</Link>
  }
*/
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { useUser } from "../auth/hook";
import './Header.css'

export function HeaderMin() {


  function setResponsive() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
    console.log('click');
  }

  return (
    <><header className="App-header">
      <a className="navbar-brand" href="/ " title="Logo">
        <img src='./logoLap.png' className="logoImg" />
      </a>
    </header>
    </>
  );

}
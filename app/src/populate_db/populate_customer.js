function getRandomInt(max) { return Math.floor(Math.random() * max); }
function getWeightedRandomInt(w, max) { return Math.floor(Math.random() * max * w); }
function getSomeValue(values, w) {
    return values[getWeightedRandomInt(w, values.length)]
}

function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

async function start() {
    let vip = require('./vip.json')
    vip = vip.map(v => v.name.split(" "))
    const u = []
    for (let i = 0; i < 30; i++) {
        const v = getSomeValue(vip, 1)
        const b = {
            email: v[0] + v[1] + '@lapiazzaimmobiliare.it',
            password: makeid(16),
            name: v[0],
            surname: v[1],
            username: v[0] + '_' + v[1],
            role: 'customer',
            mock: true
        }

        let res = await fetch('/users', {
            body: JSON.stringify(b),
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
        })
        
        u.push(res)
    }

    console.log(u.map(x => x._id))
}

export function PopulateCustomerDb() {
    return (
        <div>
            <button className='btn btn-danger m-4' onClick={start}>CUSTOMERS</button>
        </div>
    );
}
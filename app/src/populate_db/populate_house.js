import React from 'react';

const municipality_req = 'https://comuni-ita.herokuapp.com/api/comuni/provincia/'
const descriptions = require('./descriptions.json')
const photos = require('./photos.json')
let customers = []

function getRandomInt(max) { return Math.floor(Math.random() * max); }
function getWeightedRandomInt(w, max) { return Math.floor(Math.random() * max * w); }
function getSomeValue(values, w){
    return values[getWeightedRandomInt(w, values.length)]
}

function fetchMunicipality() {
    let fc_municipality
    let bo_municipality
    let ra_municipality
    let municipality = []
    return new Promise((resolve, reject) => {
        fetch(municipality_req + 'forlì-cesena').then((res) => {
            console.log('fetch fc municipality')
            res.json().then(res => {
                fc_municipality = res
                fetch(municipality_req + 'bologna').then((res) => {
                    console.log('fetch bo municipality')
                    res.json().then(res => {
                        bo_municipality = res
                        fetch(municipality_req + 'ravenna').then((res) => {
                            console.log('fetch ra municipality')
                            res.json().then(res => {
                                ra_municipality = res

                                fc_municipality = fc_municipality.map(c => {
                                    c.provincia = 'Forlì-Cesena'
                                    c.regione = 'Emilia-Romagna'
                                    return c
                                })

                                bo_municipality = bo_municipality.map(c => {
                                    c.provincia = 'Bologna'
                                    c.regione = 'Emilia-Romagna'
                                    return c
                                })

                                ra_municipality = ra_municipality.map(c => {
                                    c.provincia = 'Ravenna'
                                    c.regione = 'Emilia-Romagna'
                                    return c
                                })

                                municipality.push(...fc_municipality)
                                municipality.push(...bo_municipality)
                                municipality.push(...ra_municipality)
                                resolve(municipality)
                            })
                        })
                    })
                })
            })
        })
    })
}


function getAddress(municipality, vip) {
    const MAX_HOUSE_NUMBER = 1024
    const WEIGHT_FOR_STREET = 0.5
    const streetType = ['Via', 'Piazza', 'Vicolo']
    let st = streetType[getWeightedRandomInt(WEIGHT_FOR_STREET, streetType.length)]
    let m = municipality[getRandomInt(municipality.length)]
    let v = vip[getRandomInt(vip.length)]
    let hn = getRandomInt(MAX_HOUSE_NUMBER)
    return `${st} ${v} ${hn}, ${m.nome}, ${m.provincia}, ${m.regione}`
}

function getCategory(){
    return getSomeValue(['Residenziale', 'Garage o Posto auto', 'Ufficio o Negozio', 'Capannone','Terreno agricolo', 'lotto edificabile'], 0.2)
}

function getAgreement() {
    return getSomeValue(['Vendita', 'Affitto'], 1)
}

function getPropertyType(){
    return getSomeValue(['Intera', 'Usufrutto', 'Nuda Proprietà'], 0.5)
}

function getPrice(agreement, mq) {
    if(agreement == 'Vendita'){
        return (getRandomInt(300) + 1500) * mq
    } else {
        return (getRandomInt(12) + 5) * mq
    }
}

function getCodominiumFees(){
    return getRandomInt(170)
}

function getFreeFrom() {
    if(getRandomInt(2) == 0){
        return 'now'
    } else {
        let m = getSomeValue(['Gennaio', "Febbreio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],1)
        let d = getRandomInt(28)
        return `disponibile dal ${d} ${m}`
    }
}

function getRentToBuy(agreement){
    if(agreement == 'VENDITA'){
        return false
    } else {
        return getRandomInt(10) == 0
    }
}

function getIncomeProperty(params) {
    return getRandomInt(10) == 0
}

function getRooms(){
    return getRandomInt(25) + 1 
}

function getSquareMeter(rooms){
    let mq = 0
    for (let i = 0; i < rooms; i++) {
        mq += getRandomInt(20)
    }
    return mq
}


function getBathRooms(mq) {
    return Number.parseInt(mq/100) + 1
}

function getKitchen(){
    return getSomeValue(['Cucina abitabile', 'Angolo ottura', 'Cucinotto'], 0.5)
}

function getGarden(mq){
    if(mq < 80){
        return false
    } else {
        return getRandomInt(2) == 0 
    }
}

function getGarage(mq){
    if(mq < 20){
        return false
    } else {
        return getRandomInt(2) == 0 
    }
}

function getCellar(mq){
    if(mq < 50){
        return false
    } else {
        return getRandomInt(2) == 0 
    }
}

function getBalcony(rooms){
    return getRandomInt(rooms * 0.5)
}

function getIndipendentEntrace(mq){
    return mq > 80
}

function getAirConditioner(){
    return getRandomInt(2) >= 1
}

function getDisabledEntrace() {
    return getRandomInt(5) == 0
}

function getFloorNumber(mq){
    if(mq >= 40){
        return getRandomInt(3) + 1
    } else {
        return 1
    }
}

function getLift(floors){
    return floors == 0? false : getRandomInt(3) >= 1
}

function getFloor(floors){
    return floors > 1 ? 0 : getRandomInt(5) 
}

function getBuildingYear() {
    return getRandomInt(121) + 1900
}

function getBuildingState(building_year){
    return building_year >= 2006 ? getSomeValue(['Ottimo', 'Buono', 'Nuovo', 'In costruzione'], 1) : getSomeValue(['Ottimo', 'Buono', 'Da ristrutturare'],1)
}

function getRef(){
    const alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    return getSomeValue(alphabet,1) + getRandomInt(15000)
}

function getRandomPhoto(){
    console.log('----- PHOTO --------')
    console.log(getSomeValue(photos,1))
    return JSON.stringify(getSomeValue(photos,1))
}

function setPhotos(fd){
    for (let i = 0; i < getRandomInt(12) + 9; i++) {
        fd.append('photos', getRandomPhoto())
    }
}

function getState(){
    return getSomeValue(['Attivo','Sospeso'],0.4)
}


async function start() {
    let municipality
    let vip = require('./vip.json')
    vip = vip.map(v => v.name)

    customers.push(...(await(
        await fetch('/users/customers?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmI0YmIyMDVlOGI1MmFiYThiYWI5ZWEiLCJuYW1lIjoiU2hhaWxlbmUiLCJzdXJuYW1lIjoiV29vZGxleSIsImVtYWlsIjoiU2hhaWxlbmVXb29kbGV5QGxhcGlhenphaW1tb2JpbGlhcmUuaXQiLCJ1c2VybmFtZSI6IlNoYWlsZW5lX1dvb2RsZXkiLCJwYXNzd29yZCI6InZISHJNeTBrVlA4TjY5UmYiLCJyb2xlIjoiYWdlbnQiLCJDSSI6W10sIlNDIjpbXSwiZmF2b3VyaXRlcyI6W10sImludGVyZXN0cyI6W10sInNlYXJjaGVzIjpbXSwibW9jayI6dHJ1ZSwiX192IjowLCJpYXQiOjE2NTYwMTE4MTgsImV4cCI6MTY1NjA5ODIxOH0.M2dnvYhM74CHn6_HFyFNl7WnMAdb8zVxG900ePMWtBQ')).json()))
    customers = customers.map(c => c._id)
    // customers = ['simo']
    fetchMunicipality().then(async (mun) => {
        municipality = mun
        for (let i = 0; i < 50; i++) {
            const fd = new FormData()
            
            fd.append('mock', true)
            
            console.log(getAddress(municipality, vip))
            fd.append('address', getAddress(municipality, vip))
            
            console.log(getCategory())
            fd.append('category', getCategory())
            
            const agreement = getAgreement()
            console.log(agreement)
            fd.append('agreement', agreement)
            
            const rooms = getRooms()
            console.log('Stanze', rooms)
            fd.append('rooms', rooms)

            const mq = getSquareMeter(rooms)
            console.log('MQ', mq)
            fd.append('mq', mq)

            console.log('Proprietà', getPropertyType())
            fd.append('property_type', getPropertyType())
            
            const price = getPrice(agreement, mq)
            console.log('prezzo', price)
            fd.append('price', price)
            
            console.log('tasse condominiali', getCodominiumFees())
            fd.append('condominium_fees', getCodominiumFees())
            
            console.log(getFreeFrom())
            fd.append('free_from', getFreeFrom())
            
            console.log('affitto con riscatto', getRentToBuy(agreement))
            fd.append('rent_to_buy', getRentToBuy(agreement))
            
            console.log('Immobile a reddito',getIncomeProperty())
            fd.append('income_property', getIncomeProperty())
            
            console.log('Bagni', getBathRooms(mq))
            fd.append('bathrooms',getBathRooms(mq))

            console.log('Cucina', getKitchen())
            fd.append('kitchen', getKitchen())

            console.log('Giardino', getGarden(mq))
            fd.append('garden', getGarden(mq))

            console.log('garage', getGarage(mq))
            fd.append('garage', getCellar(mq))

            console.log('cantina', getCellar(mq))
            fd.append('cellar', getCellar(mq))

            console.log('balcone', getBalcony(rooms))
            fd.append('balcony',  true)

            console.log('ingresso indipendente', getIndipendentEntrace(mq))
            fd.append('indipendent_entrace', getIndipendentEntrace(mq))

            console.log('aria conditionata', getAirConditioner())
            fd.append('air_conditioner', getAirConditioner())

            console.log('ingresso disabili', getDisabledEntrace())
            fd.append('disabled_entrace', getDisabledEntrace())

            const floors = getFloorNumber(mq)
            console.log('numero di piani', floors)
            fd.append('floors_number', floors)

            console.log('ascensore', getLift(floors))
            fd.append('lift', getLift(floors))

            console.log('piano', getFloor(floors))
            fd.append('floor', getFloor(floors))

            console.log('riscaldamento',true)
            fd.append('heating', true)

            console.log('anno di costruzione', getBuildingYear())
            fd.append('building_year', getBuildingYear())

            console.log('stato di costruzione', getBuildingState())
            fd.append('building_state', getBuildingState())

            fd.append('cadastral_data', JSON.stringify({
                sezione: 'xxxx',
                foglio: getRandomInt(15000),
                particella: getRandomInt(15000),
                subalterno: getSomeValue(vip,1),
                rendita_catastale: getRandomInt(200000),
                categoria: 'xxxx'
            }))

            fd.append('description', getSomeValue(descriptions, 1))

            console.log(getRef())
            fd.append('ref', getRef())

            setPhotos(fd)

            fd.append('state', getState())

            fd.append('priority', getRandomInt(50))

            fd.append('RTI', JSON.stringify({
                "filename": "RTI.txt",
                "download_link": "https://lapiazzaimmobiliare.s3.eu-west-1.amazonaws.com/RTI.txt"
            }))
            fd.append('APE', JSON.stringify({
                "filename": "APE.txt",
                "download_link": "https://lapiazzaimmobiliare.s3.eu-west-1.amazonaws.com/APE.txt"
            }))
            fd.append('cerificate', JSON.stringify({
                "filename": "certificate.txt",
                "download_link": "https://lapiazzaimmobiliare.s3.eu-west-1.amazonaws.com/certificate.txt"
            }))
            fd.append('cadastral_plan', JSON.stringify({
                "filename": "cadastral_plan.txt",
                "download_link": "https://lapiazzaimmobiliare.s3.eu-west-1.amazonaws.com/cadastral_plan.jpg"
            }))
            fd.append('AAA', JSON.stringify({
                "filename": "AAA.txt",
                "download_link": "https://lapiazzaimmobiliare.s3.eu-west-1.amazonaws.com/AAA.txt"
            }))
          
            
            console.log('PROPRIETARIO', getSomeValue(customers,1))
            fd.append('owners', [getSomeValue(customers,1)])
            
            console.log(fd)
            let post = await fetch('/houses/populate', {
                method: 'POST',
                body: fd
            })
            console.log(post.ok)
            

            console.log('-----------')

        }
        console.log('____ POPULATED! ____')
    })
}


export function PopulateDb() {
    return (
        <div>
            <button className='btn btn-primary' onClick={start}>HOUSES</button>
        </div>
    );
}

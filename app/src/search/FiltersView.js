import React from 'react'
import { getAgreementTypes } from '../house/agreementTypes'
import { getAllCategories } from '../house/category'
import { getBuildingStates } from '../house/house'
import { Search } from './search'
import { useNavigate } from 'react-router-dom'

function AgreementFilter() {
    const [agreement, setAgreement] = React.useState(getAgreementTypes()
        .map(at => {
            return { name: at.name, selected: Search.getAgreement() == at.name }
        }))


    async function higlightAgreement(name) {
        Search.setFilters({ 'agreement': name })
        setAgreement((prev) => {
            let curr = getAgreementTypes()
                .map(at => {
                    return { name: at.name, selected: Search.getAgreement() == at.name }
                })
            return curr
        })
        await Search.search()
    }

    function createAgreementButtons() {
        let w = Number.parseInt(12 / agreement.length)
        w = w < 2 ? 2 : w
        w = w > 10 ? 5 : w
        return agreement.map((a, index) => {
            const classToSet = a.selected ? 'selectedAgreementFilters' : 'notSelectedAgreementFilters'
            return (
                <div key={index} className={`col-${w} mt-3`} onClick={() => {
                    higlightAgreement(a.name)
                }}>
                    <div className={classToSet}>
                        {a.name}
                    </div>
                    <div className={`${classToSet} pl-1 pr-2`}></div>
                </div>
            )
        })
    }

    return (
        <div className='container'>
            <div className="row justify-content-center">
                {createAgreementButtons()}
            </div>
        </div>
    )

}

function SelectFilterComponent({ values, filter, icons }) {
    let currSelected = Search.getCurrentFilters()[filter]
    currSelected = currSelected != null ? currSelected : 'Tutto'
    //mettici una lista di valori e poi prendi indice e metti icona giusta


    function select(e) {
        let v = e.target.value
        if (v != null) {
            let f = v == 'Tutto' ? null : v
            let filters = {}
            filters[filter] = f
            Search.search(filters)
        }
    }

    function getToUse() {
        let toUse = [{ value: 'Tutto', icon: valuesUsingIcon() ? '   ' : null }]
        let first = values.filter(c => c.value == currSelected)
        console.log('first', first)
        return [...first, ...toUse, ...values.filter(c => c.value != currSelected)]
    }

    function valuesUsingIcon() {
        return values[0].icon != null
    }

    function createOptions() {

        return getToUse().map(v => v.value).map((c, idx) => {
            if (c == currSelected) {
                return <option key={idx} value={c} defaultValue>{c}</option>
            } else {
                return <option key={idx} value={c}>{c}</option>

            }
        })
    }
    return (

        <div className='container'>
            <div className="input-group">
                <select onChange={select} className="form-select form-select selectFilterOption" aria-label=".form-select-lg example">
                    {createOptions()}
                </select>
            </div>
        </div>
    )
}

function SelectCategory() {
    return <div className='mt-2'>
        <SelectFilterComponent values={getAllCategories()
            .map(c => { return { value: c, icon: null } })} filter={'category'}></SelectFilterComponent>
    </div>
}

function SelectBuildingState() {
    return <div className='mt-2'>
        <SelectFilterComponent values={getBuildingStates()} filter={'building_state'}></SelectFilterComponent>
    </div>

}


function FromToComponent({ um, filter, label }) {
    const [fromValue, setFromValue] = React.useState(null)
    const [toValue, setToValue] = React.useState(null)
    const from = React.useRef(null)
    const to = React.useRef(null)

    React.useEffect(() => {
        let toUse = Search.getCurrentFilters()[filter]
        if (toUse != null) {
            from.current.value = toUse.from
            to.current.value = toUse.to
        }
    }, [])

    function checkIsNotValid(v) {
        return v == null || (v != 0 && v == '') || isNaN(v)
    }
    function changeFrom() {
        let curr = Number.parseInt(from.current.value)
        curr = checkIsNotValid(curr) ? null : curr
        let params = {}
        params[filter] = {
            from: curr,
            to: toValue
        }
        Search.search(params)
        setFromValue(curr)
    }

    function changeTo() {
        let curr = Number.parseInt(to.current.value)

        curr = checkIsNotValid(curr) ? null : curr
        let params = {}
        params[filter] = {
            from: fromValue,
            to: curr
        }
        Search.search(params)
        setToValue(curr)
    }

    return (
        <div className='container'>
            <div className='row  justify-content-center'>
                <div className='col-3 fromToText2'>{label} {um}</div>
                <div className='col-4 fromToText'>
                    <input onChange={changeFrom} ref={from} type="number"
                        className="form-control priceInput" id="daPrice" min={0} placeholder={`da`}></input>
                </div>
                <div className='col-4 fromToText'>
                    <input onChange={changeTo} ref={to} type="number"
                        className="form-control priceInput" id="daPrice" min={fromValue} placeholder={`a`}></input>
                </div>
            </div>
        </div>
    )
}


function SelectRooms() {
    return <div className='mt-2'>
        <FromToComponent um={<i className="bi bi-border-left"></i>} filter={'rooms'} label={'Locali'}></FromToComponent>
    </div>
}

function SelectPrice() {
    return <div className='mt-2'>
        <FromToComponent um={'€'} filter={'price'} label={'Prezzo'}></FromToComponent>
    </div>
}

function BooleanFilter({ filter }) {
    const [selected, setSelected] = React.useState(false)

    React.useEffect(async () => {
        let fromSearch = Search.getCurrentFilters()[filter.value]
        console.log(filter.value, fromSearch != null && fromSearch)
        setSelected(fromSearch != null && fromSearch)
    }, [])

    async function selectFilter() {
        let filters = {}
        filters[filter.value] = !selected
        await Search.search(filters)
        setSelected((prev) => {
            return !prev
        })
    }

    console.log('selected?', selected)
    let classToUse = selected ? 'greenButtonFilterSelected' : 'greenButtonFilter'
    return <button type='button' className={'btn mt-2 ' + classToUse} onClick={selectFilter}>
        {filter.view}
    </button>
}

function GarageSelector() {
    return <BooleanFilter filter={{
        view: 'Garage',
        value: 'garage'
    }}></BooleanFilter>
}

function GardenSelector() {
    return <BooleanFilter filter={{
        view: 'Giardino',
        value: 'garden'
    }}></BooleanFilter>
}

function IndipendentEntranceSelector() {
    return <BooleanFilter filter={{
        view: 'Ingresso indipendente',
        value: 'id'
    }}></BooleanFilter>
}


function CharacteristicsSelector({ children }) {

    let buttons = React.Children.map(children, function (c) {
        return React.cloneElement(<div className='col-6 col-md-3'>{c}</div>);
    })

    return <div className='container'>
        <div className='row'>
            {buttons}
        </div>
    </div>
}

let cbCounterButton = undefined
function CounterButton() {
    const [announcements, setAnnouncements] = React.useState(0)
    const navigate = useNavigate()

    React.useEffect(async () => {
        cbCounterButton = Search.on(handleSearchEvents)
        await Search.search()
        // return () => { console.log('mi smonto'); subscribed = false }
    }, [])

    React.useEffect(() => {
        cbCounterButton.complete()
        cbCounterButton = Search.on(handleSearchEvents)
    }, [announcements])

    function handleSearchEvents(e) {
        if (e.event == 'CurrentSearch') {
            setAnnouncements(e.houses.length)
        }
    }

    function comeBackToSearch() {
        navigate('/search')
    }

    return (
        <div id='CounterButtonFooter'>
            <button id='CounterButton' type='button' className='btn' onClick={comeBackToSearch}>
                Vedi {announcements} annunci
            </button>
        </div>
    )
}

export function FiltersView() {
    return (
        <React.Fragment>
            <AgreementFilter></AgreementFilter>
            <SelectCategory></SelectCategory>
            <SelectPrice></SelectPrice>
            <SelectRooms></SelectRooms>
            <SelectBuildingState></SelectBuildingState>
            <CharacteristicsSelector>
                <GarageSelector></GarageSelector>
                <GardenSelector></GardenSelector>
                <IndipendentEntranceSelector></IndipendentEntranceSelector>
            </CharacteristicsSelector>
            <CounterButton></CounterButton>
        </React.Fragment>
    )
}
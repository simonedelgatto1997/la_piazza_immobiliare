import React from "react";
import './LocationSearchingBar.css'
import { useNavigate } from "react-router-dom";
import { Search } from './search'


export function LocationSearchingBar({ user, type, currentAddress, setHouse, setLoading }) {
    const searchingBarRef = React.useRef(null)
    const [searching, setSearching] = React.useState(false)
    const navigate = useNavigate()
    function search() {
        console.log(searchingBarRef.current.value)
        Search.setFilters({ address: searchingBarRef.current.value })
        if(type == 'searchview'){
            setLoading(true)
            Search.search().then(res => {
                setHouse(res)
                setLoading(false)
            })
        } else {
            navigate('/search')
        }
    }
    switch (type) {
        case 'searchview':
            return (<LocationSearchingBarSearchView
                searchingBarRef={searchingBarRef}
                searching={searching}
                search={search}
                currentAddress={currentAddress}
            />)


        default:
            return (<LocationSearchingBarClassic
                searchingBarRef={searchingBarRef}
                searching={searching}
                search={search}
            />)

    }
}

function LocationSearchingBarSearchView({ searchingBarRef, searching, search, currentAddress }) {
    const navigate = useNavigate()
    return (
        <div id="LocationSearchingBarSearchView" className="row p-1 m-0">
            <div className="col-2 justify-content-center" onClick={() => {
                navigate('/')
            }}>
                <img style={{width:'100%',maxWidth:'50px'}} src="./lplogo.png" alt="logo" className="rounded-circle"></img>
            </div>
            <div className="col-7">
                <input type="text" style={{height:'100%'}} ref={searchingBarRef} className="form-control"
                    placeholder={currentAddress != null ? currentAddress : 'es Via Castel Latino 115, Forlì, Forlì-Cesena, Emilia-Romagna'}>
                </input>
            </div>
            <div className='col-3'>
                <button id='buttonSearch' style={{ width: '100%', height:'100%'}}
                    onClick={search} className="btn" disabled={searching} >
                    <i className="bi bi-search"></i>
                    {searching ? <div className="spinner-grow text-success" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </div> : <div></div>}
                </button>
            </div>
        </div>
    )
}


function LocationSearchingBarClassic({ searchingBarRef, searching, search }) {
    return (
        <div id="LocationSearchingBar" className="justify-content-center row p-3">
            <div className="col-md-6 col-10">
                <input type="text" ref={searchingBarRef} className="form-control" placeholder='es Via Castel Latino 115, Forlì, Forlì-Cesena, Emilia-Romagna'></input>
            </div>
            <div className='col-md-1 col-2'>
                <button id='buttonSearch' style={{ width: '100%', }}
                    onClick={search} className="btn" disabled={searching} >
                    <i className="bi bi-search"></i>
                    {searching ? <div className="spinner-grow text-success" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </div> : <div></div>}
                </button>
            </div>
        </div>
    )
}
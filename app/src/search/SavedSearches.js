import React from 'react'
import { Loading, NoElementsFound } from '../elements/NoElements'
import { getBuildingStates } from '../house/buildingState'
import { useNavigate } from 'react-router-dom'

import './SavedSearches.css'
import { Search } from './search'
import { NavigationFooter } from '../elements/NavigationFooter'
import { SavedSearchComparator } from '../elements/itemComparator'

function SavedSearchCard({ search }) {
    const navigate = useNavigate()
    function getTitle() {
        let t = search.category != null ? `${search.category} - ` : ''
        t = search.address != null ? t + search.address : t
        t = t == '' ? 'TUTTO' : t
        return t
    }
    function getFromTo(element, symbol, description) {
        const p = element
        if (p != null) {
            let s = p.from != null ? `da ${p.from} ${symbol} ` : ''
            s = p.to != null ? s + `fino a ${p.to} ${symbol} ${description}` : s
            return <p className='simpleText m-2'>{s}</p>
        } else {
            return <div></div>
        }
    }

    function getPrice() {
        return getFromTo(search.price, '€', '')
    }

    function getRooms() {
        return getFromTo(search.rooms, '', 'Locali')
    }

    function getDiff() {
        if (search.diff != null) {
            return <div className='d-flex justify-content-end mt-2'>
                <span className="badge rounded-pill bg-secondary">
                    {search.diff >= 0 ? '+' + search.diff : '-' + search.diff}
                    <span className="visually-hidden">unread messages</span>
                </span>
            </div>
        } else {
            return <div></div>
        }
    }

    function getBuildingStateSearch() {
        const bs = search.building_state
        if (bs != null) {
            return getBuildingStates().filter((bs_get) => bs_get.value == bs)[0].classicView
        } else {
            return <div></div>
        }
    }

    async function goToSearch() {
        console.log(search)
        await SavedSearchComparator.save(search)
        console.log('ss', search)
        Search.setFiltersNotUpdate(search)
        navigate('/search')
    }

    return <div className='savedSearchCard' >
        <div className='container-fluid' onClick={goToSearch}>
            <hr></hr>
            <h5 className='greenText m-2'>{getTitle()}</h5>
            {getPrice()}
            {getRooms()}
            <div className='m-2'>
                {getBuildingStateSearch()}
            </div>
            {getDiff()}
        </div>
    </div>


}

function NoSavedSearch() {
    return <h5 id='NoElement'>Nessuna ricerca salvata</h5>

}

export function SavedSearches({ user }) {
    const [ss, setSS] = React.useState([])
    const [l, setL] = React.useState(true)

    React.useEffect(async () => {
        const res = (await (await fetch(`/users/${user._id}?jwt=${user.jwt}`)).json()).searches
        setSS(res)
        const res2 = (await SavedSearchComparator.compare(res))
        console.log(res2)
        setSS([...res2])
        setL(false)
    }, [])

    function createSavedSearchCards() {
        return ss.map((s, index) => {
            return <div>
                <SavedSearchCard key={index} search={s}></SavedSearchCard>

            </div>
        })

    }


    return <React.Fragment>
        <Loading loading={l}>
            <NoElementsFound>
                {createSavedSearchCards()}
                <NoSavedSearch></NoSavedSearch>
            </NoElementsFound>
        </Loading>
        <NavigationFooter current='savedSearch'></NavigationFooter>
    </React.Fragment>
}
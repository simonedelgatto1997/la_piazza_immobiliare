import React from 'react'
import { LocationSearchingBar } from './LocationSearchingBar'
import { Search } from './search'
import { useNavigate } from 'react-router-dom'
import './SearchView.css'
import { HousesSearch } from '../house/HousesSearch'
import { NavigationFooter } from '../elements/NavigationFooter'
export function SearchView({ user, addFeedBack }) {
    const navigate = useNavigate()
    const [houses, setHouses] = React.useState([])
    const [loading, setLoading] = React.useState(true)

    React.useEffect(async () => {
        console.log('reload')
        //chiedere la ricerca corrente e mettersi in ascolto ?non vogloi cambiare la searchin bar ma arricchira
        let h = await Search.search()
        if (h.length > 0) {
            setHouses(h)
        }
        setLoading(false)
    }, [])


    function goToFilters(params) {
        navigate('/filters')
    }

    async function checkIfIsLogin(cb) {
        if (user == null) {
            navigate('/auth')
        } else {
            await cb(user)
        }
    }


    async function saveCurrentSearch() {
        await checkIfIsLogin(async () => {
            await Search.save(user)
            addFeedBack('Ricerca salvata')
        })
    }


    //SearchViewHeader
    return (
        <React.Fragment>
            <div className='sticky-top'>
                <LocationSearchingBar user={user} type='searchview'
                    currentAddress={Search.getCurrentFilters().address}
                    setHouse={setHouses}
                    setLoading={setLoading}
                    >
                </LocationSearchingBar>
                <div id='SearchUtilsButtons' className='row p-1 m-0'>

                    <div className='col-6 col-lg-1 p-1'>
                        <button onClick={goToFilters} style={{ width: '100%' }} className='btn btn-outline underSearchBarButton'>
                            <i className="bi bi-sliders"></i>Filtra
                        </button>
                    </div>
                    <div className='col-6 col-lg-1 p-1'>
                        <button style={{ width: '100%' }} onClick={saveCurrentSearch} className='btn btn-outline underSearchBarButton'>
                            <i className="bi bi-bell"></i> Salva
                        </button>
                    </div>
                </div>
            </div>

            <HousesSearch houses={houses} loading={loading} user={user}></HousesSearch>
            <NavigationFooter current='search'></NavigationFooter>
        </React.Fragment>

    )
}
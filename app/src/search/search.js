import { filter, Subject } from 'rxjs';
import { defaultAgreement } from '../house/agreementTypes';

export const Search = (() => {
    const emitter = new Subject()
    let filters = { agreement: defaultAgreement().name }
    let currentHomes = []
    let addressIsChange = true

    function emitNewHouseFounded(house) {
        emitter.next({ event: 'NewHouseFound', house: house })
    }

    function emitCurrentSearch(houses) {
        emitter.next({ event: 'CurrentSearch', houses: houses })
    }

    function filterHouse(h) {
        // console.log('--- FILTRO CASA ---')
        const res = Object.keys(filters)
            .map(k => {
                // console.log(k, filters[k], h[k])
                if (k != 'address'){
                    if(k == 'price' || k == 'rooms'){
                        // console.log(h.price, filters[k].from, filters[k].to, (filters[k].from == null || h[k] >= filters[k].from), (filters[k].to == null || h[k] <= filters[k].to))
                        return (filters[k].from == null || h[k] >= filters[k].from) && (filters[k].to == null || h[k] <= filters[k].to)
                    }
                    return filters[k] == h[k]
                } else {
                    return true
                }

            }).reduce((prev, curr) => prev && curr, true)
        // console.log('RISULTATO FINALE', res)
        // console.log('------------')
        return res
    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    return {
        on: function (cb) {
            try {
                return emitter.subscribe(cb)
            } catch (error) {
                console.log(error)
            }
        },

        unsubscribe: function(cb){
            emitter.unsubscribe(cb)
        },

        getEmitter: function(){
            return emitter
        },

        setFiltersNotUpdate: function(f){
            filters = f
        },

        setFilters: function (f) {
            if (f != null) {
                if (f.address != null) {
                    addressIsChange = true
                    // console.log('-- cambia indirizzo --', f.address)
                }
                Object.keys(f).forEach(k => {
                    if(f[k] != null && f[k] != false){
                        filters[k] = f[k]
                    } else {
                        delete filters[k]
                    }
                })
            } 
        },

        searchOneTime: async function (filtersOneTime) {
            let res = await fetch(`/houses?filters=${JSON.stringify(filtersOneTime)}`)
            return await res.json()
        },

        search: async function (filtersToSet) {
            this.setFilters(filtersToSet)
            console.log('FILTERS', filters)
            // console.log(addressIsChange)
            if (addressIsChange) {
                console.log('indirizzo cambiato')
                addressIsChange = false
                // console.log('-- fetch indirizzo cambiato filters --')
                let res = await fetch(`/houses?filters=${JSON.stringify({address : filters.address})}`)
                currentHomes = await res.json()
                // console.log('current homes', currentHomes)
            }
            // console.log('--- CURRENT HOUSE ---')
            // console.log(currentHomes)
            // console.log('--------')
            emitCurrentSearch(currentHomes.filter(ch =>
                filterHouse(ch)
            ))
            return currentHomes.filter(ch => filterHouse(ch))
        },

        getCurrentFilters: function () {
            return JSON.parse(JSON.stringify(filters))
        },

        getAgreement() {
            return filters.agreement
        },

        async save(user) {
            console.log('SALVO')
            let fd = new FormData()
            fd.append('searches',JSON.stringify([filters]))
            let res = await fetch(`/users/${user._id}?jwt=${user.jwt}`, {
                method:'PUT',
                body: fd,
                "Content-type": "multipart/form-data"
            })
            await res.json()
        },

    }
})()
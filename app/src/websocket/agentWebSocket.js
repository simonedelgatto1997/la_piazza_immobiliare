import { io } from "socket.io-client";

export const AgentWebSocket = function () {
    let socket = undefined
    let onCustomerExpressInterestCB = undefined

    function joinAgentsRoom() {
        socket.emit('joinAgentsRoom')
    }
    function addEventsHandler() {
        socket.on("customerExpressInterest", (data) => {
            onCustomerExpressInterestCB(data)
        })
    }

    function onCustomerExpressInterest(f) {
        onCustomerExpressInterestCB = f
    }
    return {
        connect() {
            const s = io();
            socket = s
            s.on("connect", () => {
            });
            joinAgentsRoom()
            addEventsHandler()
        },
        onCustomerExpressInterest
    }
}()
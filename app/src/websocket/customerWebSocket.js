import { io } from "socket.io-client";

export const CustomerWebSocket = function () {
    let socket = undefined
    function joinCustomerRoom() {
        socket.emit('joinCustomersRoom')
    }
    return {
        connect() {
            const s = io();
            socket = s
            s.on("connect", () => {
                // console.log('client')
                // console.log(s.id);
            });
            joinCustomerRoom()
        },
        expressInterest: function (userId, property) {
            socket.emit("customerExpressInterest",{
                userId: userId,
                property: property
            })
        }
    }
}()
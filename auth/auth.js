var jsonwebtoken = require('jsonwebtoken');
var fs = require('fs')
var path = require('path')
var users = require('../model/users')

const expiration = 60 * 60 * 24

function getRoles() {
    return {
        customer: { level: 0 },
        agent :{ level: 1},
        admin :{ level: 2},
    }
}

function roleIsBiggerOrEqualThan(r, r1) {
    console.log(getRoles()[r])
    console.log(getRoles()[r1])
    console.log(getRoles()[r] >= getRoles()[r1])
    return getRoles()[r] >= getRoles()[r1]
}

async function auth(username, password, role) {
    var u = await users.getUser(username, password, role)
    if (u !== null) {
        var jwt = jsonwebtoken.sign(u, fs.readFileSync(path.join('./', 'jwt')), { expiresIn: expiration });
        u.jwt = jwt
        u.expiration = expiration
        return u
    } else {
        throw {
            name: 'Unauthorized',
            message: `No user with username:${username}, password:${password}, role:${role}`
        }
    }
}

function checkToken(jwt) {
    let res = jsonwebtoken.verify(jwt, fs.readFileSync(path.join('./', 'jwt')))
    return res
}

function checkTokenWithRoles(ROLES) {
    return (jwt) => {
        var contents = checkToken(jwt)
        if (!ROLES.includes(contents.role)) {
            throw {
                name: 'Unauthorized',
                message: `Is mandatory to have role ${ROLES}. Role ${contents.role} is not allowed`
            }
        }
        return contents
    }
}

function checkTokenHaveAtLeastThisLevel(ROLE) {
    return (jwt) => {
        var contents = checkToken(jwt)
        if (!roleIsBiggerOrEqualThan(contents.role, ROLE)) {
            throw {
                name: 'Unauthorized',
                message: `Is mandatory to have role equal or biger than ${ROLE}. Role ${contents.role} is not allowed`
            }
        }
        return contents
    }
}

function getCheckTokenHaveAtLeastThisLevelMiddleware(ROLE) {
    return (req, res, next) => {
        try {
            var user = checkTokenHaveAtLeastThisLevel(ROLE)(req.query.jwt)
            // if (req.params.id !== undefined && req.params.id !== user._id) {
            //     res.status('401')
            //     res.send(`User mismatch. JWT for ${user._id}, request for ${req.params.id}`)
            // }
            req.auth_middleware = user 
            next()
        } catch (error) {
            res.status('401')
            res.send(error.message)
        }

    }
}

function getCheckTokenWithRolesMiddleware(ROLES) {
    return (req, res, next) => {
        try {
            var user = checkTokenWithRoles(ROLES)(req.query.jwt)
            // if (req.params.id !== undefined && req.params.id !== user._id) {
            //     res.status('401')
            //     res.send(`User mismatch. JWT for ${user._id}, request for ${req.params.id}`)
            // }
            next()
        } catch (error) {
            res.status('401')
            res.send(error.message)
        }

    }
}

function getCheckTokenCustomerMiddleware() {
    return getCheckTokenWithRolesMiddleware(['customer'])
}

module.exports = {
    auth,
    checkToken,
    checkTokenWithRoles,
    getCheckTokenWithRolesMiddleware,
    getCheckTokenCustomerMiddleware,
    getCheckTokenHaveAtLeastThisLevelMiddleware
}





const dbConnector = require('../utils/db_connector.js')
const Exception = require('../utils/exception')

async function createNewDeal(deal) {
    var Deal = dbConnector.getDealModel()
    if(deal.documentation != null){
        Object.keys(deal['documentation']).forEach(doc_key => {
            console.log('doc key', doc_key)
            deal[doc_key] = deal.documentation[doc_key]
        })
    }
    await Deal.create(deal, function (err, small) {
        if (err) return Exception.throwCannotCreateDealException(err, deal)
    })
    return deal
}

async function getDeal(){
    try{
        var Deal = dbConnector.getDealModel()
        var d = await Deal.find({}).exec()
        console.log("D " + d)
        return d
    } catch (error) {
        console.log(error)
        return null
    }
}

async function getDealById(id){
    try{
        var Deal = dbConnector.getDealModel()
        var d = await Deal.findById(id).exec()
        console.log("D " + d)
        return d.toObject()
    } catch (error) {
        console.log(error)
        return null
    }
}

async function updateDeal(data){
    try {
        var Deal = dbConnector.getDealModel()
        var d = await Deal.findOne({
            _id: data._id
        }).exec()

        if (d != null) {
            Object.keys(data).forEach((key) => {
                if (key == 'documentation') {
                    Object.keys(data['documentation']).forEach(doc_key => {
                        console.log('doc key', doc_key)
                        d[doc_key] = data.documentation[doc_key]
                        console.log('u doc key', h[doc_key])
                    })
                } else {
                    d[key] = data[key]
                }
            })
            await d.save()
            return d.toObject()
        } else {
            //Exception.throw({ _id: data._id })
        }
    } catch (error) {
        console.log(error)
        Exception.throwErrorSearchingUserOnDB(error)
    }
}

async function deleteDeal(id) {
    try{
        var Deal = dbConnector.getDealModel()
        console.log('ID ' + id)
        var res = await Deal.findByIdAndDelete(id).clone();
        return res
        //await Deal.deleteById(id)
        /*var d = await Deal.find({}).exec()
        var dD = await Deal.findById(id).exec()
        await Deal.delete(dD)
        console.log("D " + d)
        return d*/
    } catch (error) {
        console.log(error)
        return null
    }
}

module.exports = {
    createNewDeal, 
    getDealById,
    getDeal,
    deleteDeal,
    updateDeal
}
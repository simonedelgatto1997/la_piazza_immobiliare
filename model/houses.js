const dbConnector = require('../utils/db_connector.js')
const Exception = require('../utils/exception')

async function createNewHouse(house) {
    var House = dbConnector.getHouseModel()
    if (house.documentation != null) {
        Object.keys(house['documentation']).forEach(doc_key => {
            console.log('doc key', doc_key)
            house[doc_key] = house.documentation[doc_key]
        })
    }

    await House.create(house, function (err, small) {
        if (err) return Exception.throwCannotCreateHouseException(err, house)
    })
    return house
}

async function getHouses(filters) {

    var House = dbConnector.getHouseModel()
    if (filters.address != null) {
        filters.address = new RegExp(filters.address)
    }

    if (filters.ids != null){
        filters._id = {$in: filters.ids}
        delete filters.ids
    }
    console.log('get house filters')
    console.log(filters)
    let r = await House.find(filters).exec()
    return r

}

async function getAllHouses(){
    try {
        var Houses = dbConnector.getHouseModel()
        var h = await Houses.find.exec()
        console.log("Find in houses model" + h)
        return h.toObject()
    } catch (error) {
        console.log(error)
        return null
    }
}

async function getHouseById(id){
    try {
        var Houses = dbConnector.getHouseModel()
        var h = await Houses.findById(id).exec()
        return h
    } catch (error) {
        console.log(error)
        return null
    }
}

async function updateHouse(data){
    try {
        var Houses = dbConnector.getHouseModel()
        var h = await Houses.findOne({
            _id: data._id
        }).exec()

        if (h != null) {
            Object.keys(data).forEach((key) => {
                if (key == 'documentation') {
                    Object.keys(data['documentation']).forEach(doc_key => {
                        console.log('doc key', doc_key)
                        h[doc_key] = data.documentation[doc_key]
                        console.log('u doc key', h[doc_key])
                    })
                } else {
                    h[key] = data[key]
                }
            })
            console.log(h)
            await h.save()
            return h.toObject()
        } else {
            //Exception.throw({ _id: data._id })
        }
    } catch (error) {
        console.log(error)
        Exception.throwErrorSearchingUserOnDB(error)
    }
}

async function deleteHouse(id) {
    try{
        var House = dbConnector.getHouseModel()
        var res = await House.findByIdAndDelete(id).clone();
        return res
    } catch (error) {
        console.log(error)
        return null
    }
}

async function getPriorityHouses(n){
    var House = dbConnector.getHouseModel()
    let r = await House.find().sort({priority: -1}).limit(n).exec()
    return r
}

module.exports = {
    createNewHouse, 
    getHouses, 
    getAllHouses,
    getHouseById,
    updateHouse,
    deleteHouse,
    getPriorityHouses
}
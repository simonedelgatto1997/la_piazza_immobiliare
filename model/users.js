const dbConnector = require('../utils/db_connector.js')
const Exception = require('../utils/exception')

//manca un eccezione che dice che utente non c'è
async function getUser(username, password, role) {
    try {
        var Users = dbConnector.getUserModel()
        var u = await Users.findOne({
            username: username,
            password: password,
            role: role
        }).exec()
        return u.toObject()
    } catch (error) {
        console.log(error)
        return null
    }
}

async function getUserByName(name) {
    //var us = [];
    try {
        console.log("By NAME . . . . .  .. ")
        var Users = dbConnector.getUserModel()
        var u = await Users.find({
            name: name
        }).exec()
        /*var s = await Users.find({
            surname: name
        }).exec()
        s != null ? us.add(s) : null
        u != null ? us.add(s) : null*/
        return u
    } catch (error) {
        console.log(error)
        return null
    }
}

async function getUserById(id) {
    try {
        console.log("By ID * * * * * ** * * * * *  ")
        var Users = dbConnector.getUserModel()
        var u = await Users.findById(id).exec()
        return u.toObject()
    } catch (error) {
        console.log(error)
        return null
    }
}

//manca una cannot create user exception
async function createNewUser(user) {
    var User = dbConnector.getUserModel()
    await User.create(user, function (err, small) {
        if (err) return handleError(err);
        console.log('saved')
    })
    return user
}

function isEqual(key){
    if(['favourites', 'interests'].includes(key)){
        return(e1,e2) => {
            return e1._id == e2._id
        }
    }
}

// crea un file di eccezioni
async function updateUser(data) {
    try {
        var Users = dbConnector.getUserModel()
        var u = await Users.findOne({
            _id: data._id
        }).exec()
        if (u != null) {
            Object.keys(data).forEach((key) => {
                if (key == 'documentation') {
                    Object.keys(data['documentation']).forEach(doc_key => {
                        console.log('doc key', doc_key)
                        u[doc_key] = data.documentation[doc_key]
                        console.log('u doc key', u[doc_key])
                    })
                } else if (['searches'].includes(key)) {
                    let s = JSON.parse(data[key])
                    u[key] = [...u[key], ...s]
                } else if (['favourites', 'interests'].includes(key)) {
                    let s = JSON.parse(data[key])
                    if(s.toDelete){
                        u[key] = u[key].filter(d => !isEqual(key)(d, s))
                    } else {
                        delete s.toDelete
                        console.log(u[key], s, !u[key].includes(s))
                        if(!u[key].includes(s)){
                            u[key] = [...u[key], s]
                        }
                    }
                } else {
                    u[key] = data[key]
                }
            })
            console.log(u)
            await u.save()
            return u.toObject()
        } else {
            Exception.throwUserNotFoundException({ _id: data._id })
        }
    } catch (error) {
        console.log(error)
        Exception.throwErrorSearchingUserOnDB(error)
    }
}

async function getCustomers() {
    try {
        var Users = dbConnector.getUserModel()
        var u = await Users.find({role: 'customer'}).exec()
        console.log('/customers ' + u)
        return u
    } catch (error) {
        console.log(error)
        return null
    }
}

//TODO
async function getFavourites(idUser) {
    try {
        var Users = dbConnector.getUserModel()
        var u = await Users.findById(idUser).exec() 
        console.log("User " + JSON.stringify(u))
        console.log("User fav " + JSON.stringify(u.favourites))
        return u.favourites
    } catch (error) {
        console.log(error)
        return null
    }
}

async function getInterests(){
    try {
        var Users = dbConnector.getUserModel()
        var u = await Users.find().exec()
        console.log("UUUS " + u)
        var e = []
        u.map(function(v) { 
            e.push(JSON.stringify({ id: v._id, 
                                    name: v.name, 
                                    surname: v.surname, 
                                    interests: v.interests }));
            //e.push("interests", v.interests);
        })
        return e //u.interests.toObject()
    } catch (error) {
        console.log(error)
        return null
    }
}

module.exports = {
    getUser,
    createNewUser,
    updateUser,
    getCustomers,
    getUserById,
    getFavourites,
    getInterests,
    getUserByName
}
var express = require('express');
var router = express.Router();
var auth = require('../auth/auth');

/* Login with token*/
router.get('/decode', function (req, res, next) {
    var user = auth.checkToken(req.query.jwt)
    res.json(user)
});

/* Login with username and password*/
router.get('/:role', async function (req, res, next) {
    try {
        var username = req.query.login
        var password = req.query.password
        var role = req.params.role
        var user = await auth.auth(username, password, role)
        res.json(user)
    } catch (error) {
        res.status(401)
        res.send(error.message)
    }

});

module.exports = router
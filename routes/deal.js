var express = require('express');
var multer = require('multer')

var router = express.Router();
const Deal = require('../model/deal');
const {getUploadMiddlewareS3} = require('../utils/documentation')
var { getCheckTokenWithRolesMiddleware } = require('../auth/auth')

router.post('/', 
    getCheckTokenWithRolesMiddleware(['admin', 'agent']),
    getUploadMiddlewareS3(['proposal']),
    async function (req, res, next) {
        console.log(req.body)
        try {
            var h = await Deal.createNewDeal(req.body)
            res.status(201)
            res.send(JSON.stringify(h))
        } catch (error) {
            console.log(error)
            res.status(500)
            res.send(error)
        }
    }
);

router.get('/',
    getCheckTokenWithRolesMiddleware(['agent', 'admin']),
    async function (req, res, next) {
        try {
            delete req.query.jwt
            var d = await Deal.getDeal()
            res.status(201)
            res.json(d)
        } catch (error) {
            console.log(error)
            res.status(500)
            res.send(error)
        }
    }
);

router.get('/:id',
    getCheckTokenWithRolesMiddleware(['agent', 'admin']),
    async function (req, res, next) {
        try {
            delete req.query.jwt
            var d = await Deal.getDealById(req.params.id)
            res.status(201)
            res.json(d)
        } catch (error) {
            console.log(error)
            res.status(500)
            res.send(error)
        }
    }
);


const multerS3 = require("multer-s3");
const aws = require("aws-sdk");
aws.config.update({
    apiVersion: '2012-10-17',
    accessKeyId: 'AKIAQDEFZMGTIVOAGVWS',
    secretAccessKey: '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',
    region: "eu-west-1",
});
const s3 = new aws.S3()

const upload = multer({
    storage: multerS3({
        acl: "public-read",
        s3,
        bucket: "lapiazzaimmobiliare",
        onError: function (err, next) {
            console.log('error', err);
            next(err);
        },
        metadata: function (req, file, cb) {
            cb(null, { fieldName: "TESTING_METADATA" });
        },
        key: function (req, file, cb) {
            const filename = req.params.id + '_' + file.originalname
            if (req.body.documentation == undefined) {
                req.body.documentation = {}
            }
            if (req.body.documentation[file.fieldname] == undefined) {
                req.body.documentation[file.fieldname] = []
            }
            req.body.documentation[file.fieldname].push({ filename: file.originalname, download_link: 'https://lapiazzaimmobiliare.s3.eu-west-1.amazonaws.com/' + 'filename' })
            cb(null, req.params.id + '_' + file.originalname);
        },
    }),
});

const middleware = upload.fields([{ name: 'proposal', maxCount: 20 }])


router.put('/:id',
    getCheckTokenWithRolesMiddleware(['agent', 'admin']),
    middleware,
    async function(req, res, next){
        try {
            req.body._id = req.params.id
            console.log('req.body request', req.body)
            const d = await Deal.updateDeal(req.body)
            res.status(201)
            res.json(d)
        } catch (error) {
            console.log(error)
            res.status(500)
            res.send(error)
        }
    }
)

router.delete('/:id',
    getCheckTokenWithRolesMiddleware(['agent', 'admin']),
    middleware,
    async function(req, res, next){
        try {
            console.log('Deal delete')
            const d = await Deal.deleteDeal(req.params.id)
            res.status(201)
            res.json(d)
            console.log('done')
        } catch (error) {
            console.log(error)
            res.status(500)
            res.send(error)
        }
    }
)

module.exports = router
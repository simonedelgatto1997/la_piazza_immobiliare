var express = require('express');
var multer = require('multer')

var router = express.Router();
const Houses = require('../model/houses')
const {getUploadMiddlewareS3, castFileToObject} = require('../utils/documentation')
var { getCheckTokenWithRolesMiddleware } = require('../auth/auth')

router.get('/', async function (req, res, next) {
    try {
        delete req.query.jwt
        var h = await Houses.getHouses(JSON.parse(req.query.filters))
        res.status(201)
        res.json(h)
    } catch (error) {
        console.log(error)
        res.status(500)
        res.send(error)
    }
})

router.get('/priority', async function (req, res, next) {
    try {
        let n = req.query.n
        var h = await Houses.getPriorityHouses(n)
        console.log(h)
        res.status(200)
        res.json(h)
    } catch (error) {
        console.log(error)
        res.status(500)
        res.send(error)
    }
})

router.get('/:id',
    getCheckTokenWithRolesMiddleware(['customer', 'agent', 'admin']),
    async function (req, res, next) {
        let h = await Houses.getHouseById(req.params.id)
        res.json(h)
});


/* CREATE NEW HOUSE */
router.post('/', 
    getCheckTokenWithRolesMiddleware(['admin', 'agent']),
    getUploadMiddlewareS3(['photos', 'RTI', 'APE', 'certificate', 'cadastral_plan', 'AAA']),
    async function (req, res, next) {
        console.log('body', req.body)
        console.log('POST in houses routes')
        req.body.photos = req.body.documentation.photos
        try {
            var h = await Houses.createNewHouse(req.body)
            console.log('--------- houses added --------')
            res.status(201)
            res.send(JSON.stringify(h))
        } catch (error) {
            console.log("Error " + error)
            res.status(500)
            res.send(error)
        }
});


router.post('/populate', 
// getCheckTokenWithRolesMiddleware(['admin', 'agent']),
getUploadMiddlewareS3([]),
castFileToObject(['RTI', 'APE', 'certificate', 'cadastral_plan', 'AAA']),
async function (req, res, next) {
    console.log('Faccio la callback')
    req.body.photos = req.body.photos.map(p => {
        return JSON.parse(p)
    })
    console.log('body', req.body)
    try {
        var h = await Houses.createNewHouse(req.body)
        res.status(201)
        res.send(JSON.stringify(h))
    } catch (error) {
        res.status(500)
        res.send(error)
    }
});

const multerS3 = require("multer-s3");
const aws = require("aws-sdk");
aws.config.update({
    apiVersion: '2012-10-17',
    accessKeyId: 'AKIAQDEFZMGTIVOAGVWS',
    secretAccessKey: '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',
    region: "eu-west-1",
});
const s3 = new aws.S3()

const upload = multer({
    storage: multerS3({
        acl: "public-read",
        s3,
        bucket: "lapiazzaimmobiliare",
        onError: function (err, next) {
            console.log('error', err);
            next(err);
        },
        metadata: function (req, file, cb) {
            cb(null, { fieldName: "TESTING_METADATA" });
        },
        key: function (req, file, cb) {
            const filename = req.params.id + '_' + file.originalname
            if (req.body.documentation == undefined) {
                req.body.documentation = {}
            }
            if (req.body.documentation[file.fieldname] == undefined) {
                req.body.documentation[file.fieldname] = []
            }
            req.body.documentation[file.fieldname].push({ filename: file.originalname, download_link: 'https://lapiazzaimmobiliare.s3.eu-west-1.amazonaws.com/' + 'filename' })
            cb(null, req.params.id + '_' + file.originalname);
        },
    }),
});

const middleware = upload.fields([{ name: 'RTI', maxCount: 20 }, 
                                { name: 'APE', maxCount: 20 },
                                { name: 'AAA', maxCount: 20 },
                                { name: 'cadastral_plan', maxCount: 20 },
                                { name: 'certificate', maxCount: 20 }])

router.put('/:id',
    getCheckTokenWithRolesMiddleware(['agent', 'admin']),
    middleware,
    async function(req, res, next){
        try {
            req.body._id = req.params.id
            console.log('req.body request', req.body)
            const u = await Houses.updateHouse(req.body)
            res.status(201)
            res.json(u)
        } catch (error) {
            console.log(error)
            res.status(500)
            res.send(error)
        }
    }
)

router.delete('/:id',
    getCheckTokenWithRolesMiddleware(['agent', 'admin']),
    middleware,
    async function(req, res, next){
        try {
            console.log('House delete')
            const h = await Houses.deleteHouse(req.params.id)
            res.status(201)
            res.json(h)
            console.log('done')
        } catch (error) {
            console.log(error)
            res.status(500)
            res.send(error)
        }
    }
)

module.exports = router
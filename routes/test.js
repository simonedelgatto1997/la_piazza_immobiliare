var express = require('express');
var router = express.Router();
var multer = require('multer')
var fs = require('fs')
var modelTest = require('../model/test')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const userId = req.body.userId
        console.log('middleware', userId)
        const dir = `./uploads/${userId}`
        fs.exists(dir, exist => {
            if (!exist) {
                return fs.mkdir(dir, () => {cb(null, dir)})
            }
            return cb(null, dir)
        })
    },
    filename: (req, file, cb) => {
        const { userId } = req.body
        cb(null, file.originalname)
    }
})

const upload = multer({storage})


/* CREATE A NEW USER */
const cpUpload = upload.fields([{ name: 'documentation', maxCount: 20 }, { name: 'userId', maxCount: 1 }])
router.post('/', cpUpload, function (req, res, next) {
    console.log('RICHIESTA CARICAMENTO FILE userId')
    console.log(req.body.userId)
});



module.exports = router;
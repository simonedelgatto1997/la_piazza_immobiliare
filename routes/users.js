var express = require('express');
var multer = require('multer')
var fs = require('fs')
var path = require('path')
var router = express.Router();
var Users = require('../model/users')
var { getUploadMiddleware } = require('../utils/documentation')
var { getCheckTokenWithRolesMiddleware, getCheckTokenHaveAtLeastThisLevelMiddleware } = require('../auth/auth')


/* GET customer listing. */
router.get('/customers',
    getCheckTokenWithRolesMiddleware(['agent', 'admin']),
    async function (req, res, next) {
        let u = await Users.getCustomers()
        //console.log("customer/id " + u)
        res.json(u)
    }
);

router.get('/favourites/:id',
    getCheckTokenWithRolesMiddleware(['agent', 'admin']),
    async function (req, res, next) {
        if (req.auth_middleware._id == req.params.id) {
            let u = await Users.getFavourites(req.params.id)
            res.json(u)
        } else {
            res.status('401')
            res.send(`User mismatch. JWT for ${user._id}, request for ${req.params.id}`)
        }
    });

router.get('/interests',
    getCheckTokenWithRolesMiddleware(['agent', 'admin']),
    async function (req, res, next) {
        try {
            let u = await Users.getInterests()
            console.log(u)
            res.json(u)
        } catch (error) {
            res.status('401')
            console.log(error)
            res.send(error)
        }
    });

router.get('/:id',
    getCheckTokenWithRolesMiddleware(['customer', 'agent', 'admin']),
    //getCheckTokenHaveAtLeastThisLevelMiddleware('customer'),
    async function (req, res, next) {
        console.log("Routes by ID* * * * *")

        console.log('--- AUTH MIDDLEWARE ---- ', req.auth_middleware)
        if (req.auth_middleware != 'customer' || req.auth_middleware._id == req.params.id) {
            let u = await Users.getUserById(req.params.id)
            if(req.auth_middleware == 'customer'){
                delete u.note
            }
            res.json(u)
        } else {
            res.status('401')
            res.send(`User mismatch. JWT for ${req.auth_middleware._id}, request for ${req.params.id}`)
        }
    });

router.get('/byName/:name',
    getCheckTokenWithRolesMiddleware(['agent', 'admin']),
    async function (req, res, next) {
        console.log("Routes by NAME . . .")
        console.log('--- AUTH MIDDLEWARE ---- ', req.auth_middleware)    
        let u = await Users.getUserByName(req.params.name)
        res.json(u)
});


/* CREATE A NEW USER */
router.post('/', async function (req, res, next) {
    console.log('creo nuovo utente')

    var u = await Users.createNewUser(req.body)
    console.log('--- user created ---')
    console.log(u)
    res.status(201)

    res.send(JSON.stringify(req.body))
});

/* ADD USER DOCUMENTATION */
// router.post('/:id/documents',
//     getCheckTokenWithRolesMiddleware(['customer', 'agent', 'admin']),
//     getUploadMiddleware(),
//     function (req, res, next) {
//         console.log(req.body)
//         res.sendStatus(201)
//     }
// )
const multerS3 = require("multer-s3");
const aws = require("aws-sdk");
aws.config.update({
    apiVersion: '2012-10-17',
    accessKeyId: 'AKIAQDEFZMGTIVOAGVWS',
    secretAccessKey: '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',
    region: "eu-west-1",
});
const s3 = new aws.S3()

const upload = multer({
    storage: multerS3({
        acl: "public-read",
        s3,
        bucket: "lapiazzaimmobiliare",
        onError: function (err, next) {
            console.log('error', err);
            next(err);
        },
        metadata: function (req, file, cb) {
            cb(null, { fieldName: "TESTING_METADATA" });
        },
        key: function (req, file, cb) {
            const filename = req.params.id + '_' + file.originalname
            if (req.body.documentation == undefined) {
                req.body.documentation = {}
            }
            if (req.body.documentation[file.fieldname] == undefined) {
                req.body.documentation[file.fieldname] = []
            }
            req.body.documentation[file.fieldname].push({ filename: file.originalname, download_link: 'https://lapiazzaimmobiliare.s3.eu-west-1.amazonaws.com/' + 'filename' })
            cb(null, req.params.id + '_' + file.originalname);
        },
    }),
});

const middleware = upload.fields([{ name: 'CI', maxCount: 20 }, 
{ name: 'SC', maxCount: 20 }])

router.put('/:id',
    getCheckTokenWithRolesMiddleware(['customer', 'agent', 'admin']),
    middleware,
    async function (req, res, next) {
        try {
            req.body._id = req.params.id
            console.log('req.body request', req.body)
            const u = await Users.updateUser(req.body)
            res.status(201)
            res.json(u)
        } catch (error) {
            console.log("Error in users in routes")
            console.log(error)
            res.status(500)
            res.send(error)
        }
    }
)

module.exports = router;
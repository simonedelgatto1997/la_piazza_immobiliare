const { download } = require('express/lib/response');
var mongoose = require('mongoose');
//mongodb://ec2-34-255-29-144.eu-west-1.compute.amazonaws.com:27017/?readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false

function getConnectionException(error) {
    return {
        name: "Connection",
        error: "Can't connect with db. log --> " + error
    }
}

function DBConnection() {
    var connection = undefined
    return {
        initialize: async function () {
            try {
                await mongoose.connect('mongodb://simosophi:pdtwd090222@ec2-54-229-161-118.eu-west-1.compute.amazonaws.com:27017/la_piazza_immobiliare?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false')
                const User = mongoose.Schema({
                    name: 'String',
                    surname: 'String',
                    email: 'String',
                    phone: 'String',
                    username: 'String',
                    password: 'String',
                    role: 'String',
                    CF: 'String',
                    note: 'String',
                    CI: 'Array',
                    SC: 'Array',
                    favourites: [{
                    type: 'Object'
                    }],
                    interests: [{
                        type: 'Object'
                    }],
                    searches: [{
                        type: 'Object'
                    }],
                    mock: 'Boolean'
                });
                mongoose.model('User', User);
                const House = mongoose.Schema({
                    //for file {filename download_link}
                    category: 'String', //Resid, Capann, Gar
                    agreement: 'String', // V o A
                    property_type: 'String', // Nuda, 
                    price: 'Number',
                    condominium_fees: 'Number',
                    free_from: 'String',
                    rent_to_buy: 'Boolean',
                    income_property: 'Boolean',
                    square_meter: 'Number',
                    rooms: 'Number',
                    bathrooms:'Number',
                    kitchen : 'String',
                    garden: 'Boolean',
                    garage: 'Boolean',
                    cellar:'Boolean',
                    balcony: 'Boolean',
                    indipendent_entrace: 'Boolean',
                    air_conditioner: 'Boolean',
                    disabled_entrace: 'Boolean',
                    lift: 'Boolean',
                    floor: 'String',
                    floors_number: 'Number',
                    heating: 'String',
                    building_year:'Number',
                    building_state: 'String',
                    cadastral_data: 'Object',//
                    description: 'String',
                    ref: 'String',
                    address :'String',
                    indexAPE: 'Object',
                    photos: [{filename: 'String', download_link: 'String'}],
                    owners: 'Array', //id dei propietari
                    state: 'String',
                    priority: 'Number',
                    survayor: 'String', //geometra
                    RTI: 'Object',
                    APE: 'Object', // file
                    certificate: 'Object', //visura
                    cadastral_plan: 'Object',
                    AAA: 'Object',
                    mock: 'Boolean'
                })
                mongoose.model('House', House);
                const Deal = mongoose.Schema({
                    proposal: 'Object', //file
                    notary: 'String',
                    first_agreement:'Object', //{location: date}
                    stipulation: 'Object',
                    household_utilities: 'Array', //{customer_code company check state}
                    house: 'String',
                    state: 'String', //Attesa mutuo, Accettata, Conclusa, Rifiutata TODO valutare meglio
                    customers: 'array' //id degli acquirenti
                })
                mongoose.model('Deal', Deal);
                mongoose.connection.on('error', err => {
                    console.log('--- DB CONNECTION ERROR ---')
                    console.log(err)
                });
                connection = mongoose
            } catch (error) {
                console.log(error)
                throw getConnectionException(error.message)
            }
        },

        getConnection: function () {
            return connection
        },

        getUserModel: function () {
            return connection.model('User')
        },

        getHouseModel: function () {
            return connection.model('House')
        }, 

        getDealModel: function () {
            return connection.model('Deal')
        },
    }
}


module.exports = DBConnection();


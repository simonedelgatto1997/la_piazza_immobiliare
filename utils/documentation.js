var multer = require('multer')
var fs = require('fs')
var path = require('path')
const multerS3 = require("multer-s3");
const aws = require("aws-sdk");


function getUploadMiddleware() {
    const storage = multer.diskStorage({
        destination: (req, file, cb) => {
            const dir = './' + 'uploads/' + req.params.id
            console.log('cartella', dir)
            console.log(!fs.existsSync(dir))
            if (!fs.existsSync(dir)) {
                console.log('mi tocca creare roba')
                return fs.mkdir(dir, () => { cb(null, dir) })
            }
            return cb(null, dir)
        },
        filename: (req, file, cb) => {
            cb(null, file.originalname)
        }
    })

    const upload = multer({ storage })
    return upload.fields([{ name: 'documentation', maxCount: 20 }])
}

function getUploadMiddlewareS3(fields) {

    aws.config.update({
        apiVersion: '2012-10-17',
        accessKeyId: 'AKIAQDEFZMGTIVOAGVWS',
        secretAccessKey: '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',
        region: "eu-west-1",
    });
    const s3 = new aws.S3()

    const upload = multer({
        storage: multerS3({
            acl: "public-read",
            s3,
            bucket: "lapiazzaimmobiliare",
            onError: function (err, next) {
                console.log('error', err);
                next(err);
            },
            metadata: function (req, file, cb) {
                cb(null, { fieldName: "TESTING_METADATA" });
            },
            key: function (req, file, cb) {
                const prefix = req.params.id != null ? req.params.id : Date.now()
                const filename = prefix + '_' + file.originalname
                if (req.body.documentation == undefined) {
                    req.body.documentation = {}
                }
                if (req.body.documentation[file.fieldname] == undefined) {
                    req.body.documentation[file.fieldname] = []
                }

                console.log('salvo documento')
                req.body.documentation[file.fieldname].push({ filename: file.originalname, download_link: 'https://lapiazzaimmobiliare.s3.eu-west-1.amazonaws.com/' + filename })
                cb(null, filename);
            },
        }),
    });

    return upload.fields(fields.map(f => { return { name: f, maxCount: 20 } }))

}

function castFileToObject(documentations) {

    const parsingFileInJson = (j, re) => {
        try {

            if (j != null) {
                Object.keys(j).forEach(k => {
                    if (re.exec(k) != null) {
                        j[k] = JSON.parse(j[k])
                    }
                })
            }
        } catch (error) {
            console.log(error)
        }
    }
    return (req, res, next) => {
        try {
            documentations = documentations.map((d) => {
                return new RegExp(d)
            })
            
            documentations.forEach(re => {
                parsingFileInJson(req.body, re)
            });
            
            documentations.forEach(re => {
                parsingFileInJson(req.body.documentation, re)
            });
            next()
        } catch (error) {
            console.log(error)    
        }
    }
}



module.exports = {
    getUploadMiddleware,
    getUploadMiddlewareS3,
    castFileToObject

}
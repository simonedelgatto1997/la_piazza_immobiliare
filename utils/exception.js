function throwCustomException(name, message){
    throw {
        name: name,
        message: message
    }
}

function throwUserNotFoundException(data){
    throwCustomException('UserNotFoundException', 'Can not found user with this data'+ JSON.stringify(data))
}

function throwErrorSearchingUserOnDB(error){
    throwCustomException('ErrorSearchingUserOnDB',error)
}

function throwCannotCreateHouseException(error, home){
    throwCustomException('CannotCreateHomeException', `Error: ${error}, data: ${JSON.stringify(home)}`)
}

function throwCannotCreateDealException(error, deal){
    throwCustomException('CannotCreateDealException', `Error: ${error}, data: ${JSON.stringify(deal)}`)
}


module.exports = {
    throwUserNotFoundException,
    throwErrorSearchingUserOnDB,
    throwCannotCreateHouseException
}
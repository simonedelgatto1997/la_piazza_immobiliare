/**
 * File to test mongodb to delete 
 */

var dbConnector = require('./db_connector')

dbConnector.getConnection().then((connection) => {
  
    const schema = connection.Schema({ name: 'string'});
    const test = connection.model('User', schema);
    
    test.create({ name: 'ago' }, function (err, small) {
        if (err) return handleError(err);
        test.find({}).then(res => {
            console.log('from db')
            console.log(res)
            res.forEach(r => {
                console.log(r)
            });
            console.log('end')
        }).catch(err => {
            console.log(err)
        })
    });
})

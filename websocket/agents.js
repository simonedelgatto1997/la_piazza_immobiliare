function Agents(io) {
    var Customers = undefined
    return {
        addCustomers: function (C) {
            Customers = C
        },
        customerExpressInterest(data){
            console.log('ricevuto messaggio di interessamento')
            io.to('agentsRoom').emit('customerExpressInterest', data)
        },
        initialize: function () {
            io.on('connection', (socket) => {
                socket.on('joinAgentsRoom', ()=>{
                    console.log('an agents connected');
                    
                    socket.join('agentsRoom')
                    
                    socket.on('customerExpressInterest', (data) => {
                        console.log("in agent prova")
                        Agents.customerExpressInterest(data)
                    })
                })
            });

        }
    }

}

module.exports = {
    Agents
}
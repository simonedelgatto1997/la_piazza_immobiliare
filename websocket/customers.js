function Customers(io) {
    var Agents = undefined
    return {
        addAgents: function (A) {
            Agents = A
        },
        initialize: function () {
            io.on('connection', (socket) => {
                socket.on('joinCustomersRoom', ()=>{
                    console.log('CUSTOMER CONNECTED');
                    
                    socket.join('customersRoom')
                    
                    socket.on('customerExpressInterest', (data) => {
                        console.log('AGENTS')
                        console.log(Agents)
                        Agents.customerExpressInterest(data)
                    })
                })
            });
        },
    }

}

module.exports = {
    Customers
}